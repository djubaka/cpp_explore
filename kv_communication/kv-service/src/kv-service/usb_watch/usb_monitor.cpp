#include "usb_monitor.hpp"
#include <fcntl.h>
#include <unistd.h>
#include <cstring>
#include "udev/device_enumeration.hpp"

namespace service {

usb_monitor::usb_monitor() : udev_mon_{udev_.create_monitor()} {
  udev_mon_.observe_by_subsystem("block", "partition");
  configure_self_pipe();
}

usb_monitor::~usb_monitor() {
  close(pfd_[1]);
  close(pfd_[0]);
}

void usb_monitor::configure_self_pipe() {
  if (pipe(pfd_) == -1) {
    return set_error_code(errno);
  }

  configure_pipe_end(pfd_[0]);
  configure_pipe_end(pfd_[1]);
}

void usb_monitor::configure_pipe_end(int fd) {
  if (ec_) return;

  auto flags = fcntl(fd, F_GETFL);
  if (flags == -1) {
    return set_error_code(errno);
  }

  flags |= O_NONBLOCK;
  if (fcntl(fd, F_SETFL, flags) == -1) {
    return set_error_code(errno);
  }
}

std::optional<detail::device> usb_monitor::observe() {
  if (ec_) return std::nullopt;

  int mon_fd = udev_mon_.descriptor();
  int pipe_fd = pfd_[0];
  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(mon_fd, &readfds);
  FD_SET(pipe_fd, &readfds);

  std::optional<detail::device> dev;
  int nfd = std::max(pipe_fd, mon_fd);
  int ret = select(nfd + 1, &readfds, nullptr, nullptr, nullptr);
  if (ret == -1) {
    set_error_code(errno);
  } else if (FD_ISSET(mon_fd, &readfds)) {
    dev = udev_.device_from_monitor(udev_mon_);
  } else if (FD_ISSET(pipe_fd, &readfds)) {
    clean_up_pipe();
  }

  return dev;
}

void usb_monitor::clean_up_pipe() {
  char ch;
  if (read(pfd_[0], &ch, 1) == -1) {
    set_error_code(errno);
  }
}

void usb_monitor::cancel() {
  if (write(pfd_[1], "x", 1) == -1) {
    set_error_code(errno);
  }
}

std::vector<detail::device> usb_monitor::scan_existing() {
  detail::device_enumeration en{udev_.handle};
  en.match_by_subsystem("scsi");
  en.match_by_device_property("scsi_device");

  std::vector<detail::device> devices;
  auto scs = en.scan();
  for (auto& s : scs) {
    auto block = s.retrieve_child("block", "partition");
    auto disk = s.retrieve_child("scsi_disk", nullptr);
    auto parent = s.retrieve_parent("usb", "usb_device");

    if (block && disk && parent) {
      devices.push_back(std::move(*block));
    }
  }

  return devices;
}

void usb_monitor::set_error_code(int error) {
  ec_ = std::error_code{errno, std::generic_category()};
}

}  // namespace service