#pragma once

#include <optional>
#include <system_error>
#include "udev/device.hpp"
#include "udev/device_ctl.hpp"

namespace service {
class usb_monitor {
 public:
  usb_monitor();
  ~usb_monitor();
  std::vector<detail::device> scan_existing();
  std::optional<detail::device> observe();
  void cancel();
  std::error_code error() const { return ec_; }

 private:
  void configure_self_pipe();
  void configure_pipe_end(int pfd);
  void clean_up_pipe();
  void set_error_code(int error);

 private:
  detail::device_ctl udev_;
  detail::device_monitor udev_mon_;
  int pfd_[2];
  std::error_code ec_;
};
}  // namespace service