#include "kv-service/usb_watch/usb_watcher.hpp"
#include "kv-service/usb_watch/usb_monitor.hpp"

namespace service {

const char* usb_watcher::USB_MOUNT_PATH = "/media/the-usb";

usb_watcher::usb_watcher() : mon_{std::make_unique<usb_monitor>()} {}

usb_watcher::~usb_watcher() { stop_watch(); }

void usb_watcher::start_watch() {
  if (observer_thread_) return;

  auto existing = mon_->scan_existing();
  for (auto& e : existing) {
    process_device(e);
  }

  observer_thread_ = std::thread([this]() { observe_changes(); });
}

void usb_watcher::observe_changes() {
  while (auto device = mon_->observe()) {
    process_device(*device);
  }
}

void usb_watcher::process_device(detail::device& dev) {
  if (dev.has_parent_as("usb", "usb_device")) {
    auto path = dev.device_node();
    auto action = dev.device_action();
    if (action.empty() || action == "add") {
      inserted(path);
    } else if (action == "remove") {
      removed(path);
    }
  }
}

void usb_watcher::stop_watch() {
  if (observer_thread_) {
    mon_->cancel();
    observer_thread_->join();
    observer_thread_ = std::nullopt;
  }
}

}  // namespace service