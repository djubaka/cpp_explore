#pragma once

#include <libudev.h>
#include <vector>
#include "device.hpp"
#include "device_monitor.hpp"

namespace service {
namespace detail {
struct device_ctl {
  device_ctl() : handle(udev_new()) {}

  ~device_ctl() {
    if (handle) {
      udev_unref(handle);
    }
  }

  device_ctl(const device_ctl&) = delete;
  device_ctl& operator=(const device_ctl&) = delete;

  device_ctl(device_ctl&& x) noexcept : handle{x.handle} { x.handle = nullptr; }

  device_ctl& operator=(device_ctl&& x) noexcept {
    handle = x.handle;
    x.handle = nullptr;
    return *this;
  }

  device_monitor create_monitor() const { return device_monitor{handle}; }

  device device_from_monitor(device_monitor& udev_mon) const {
    return device{udev_mon.handle};
  }

  device device_from_syspath(const char* syspath) const {
    return device{handle, syspath};
  }

  udev* handle{nullptr};
};
}  // namespace detail
}  // namespace service