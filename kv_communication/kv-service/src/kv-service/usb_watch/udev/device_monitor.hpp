#pragma once

#include <libudev.h>

namespace service {
namespace detail {
struct device_monitor {
  explicit device_monitor(udev* udev_handle) {
    handle = udev_monitor_new_from_netlink(udev_handle, "udev");
    udev_monitor_enable_receiving(handle);
  }

  ~device_monitor() { udev_monitor_unref(handle); }

  device_monitor(const device_monitor&) = delete;
  device_monitor& operator=(const device_monitor&) = delete;

  device_monitor(device_monitor&& x) noexcept : handle{x.handle} {
    x.handle = nullptr;
  }

  device_monitor& operator=(device_monitor&& x) {
    handle = x.handle;
    x.handle = nullptr;
    return *this;
  }

  void observe_by_subsystem(const char* subsystem, const char* devtype) {
    udev_monitor_filter_add_match_subsystem_devtype(handle, subsystem, devtype);
  }

  int descriptor() const { return udev_monitor_get_fd(handle); }

  udev_monitor* handle{nullptr};
};
}  // namespace detail
}  // namespace service