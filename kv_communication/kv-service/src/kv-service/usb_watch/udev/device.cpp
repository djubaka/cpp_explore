#include "device.hpp"
#include "device_enumeration.hpp"

namespace service {
namespace detail {

device::device(udev_device* dev_handle) : handle{udev_device_ref(dev_handle)} {}

device::device(udev* udev_handle, const char* syspath)
    : handle{udev_device_new_from_syspath(udev_handle, syspath)} {}

device::device(udev_monitor* udev_mon)
    : handle{udev_monitor_receive_device(udev_mon)} {}

device::~device() {
  if (handle) {
    udev_device_unref(handle);
  }
}

std::string device::device_action() const {
  const char* act = udev_device_get_action(handle);
  return (act != nullptr) ? std::string{act} : std::string{};
}

std::string device::device_path() const {
  return udev_device_get_devpath(handle);
}

std::string device::device_node() const {
  const char* node = udev_device_get_devnode(handle);
  return (node != nullptr) ? std::string{node} : std::string{};
}

std::string device::device_system_path() const {
  return udev_device_get_syspath(handle);
}

std::optional<device> device::retrieve_child(const char* sub, const char* dev) {
  auto udev_handle = udev_device_get_udev(handle);
  device_enumeration en{udev_handle};
  en.match_by_parent(*this);
  en.match_by_subsystem(sub);
  if (dev != nullptr) {
    en.match_by_device_property(dev);
  }
  return en.scan_first();
}

std::optional<device> device::retrieve_parent(const char* sub,
                                              const char* dev) {
  auto parent = udev_device_get_parent_with_subsystem_devtype(handle, sub, dev);
  return (parent != nullptr) ? std::make_optional(device{parent})
                             : std::nullopt;
}

bool device::has_parent_as(const char* subsystem, const char* devtype) {
  auto parent = retrieve_parent(subsystem, devtype);
  return parent.has_value();
}

}  // namespace detail
}  // namespace service