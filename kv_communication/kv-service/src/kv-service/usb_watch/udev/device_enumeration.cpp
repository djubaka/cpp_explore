#include "device_enumeration.hpp"

namespace service {
namespace detail {
void device_enumeration::match_by_parent(device& parent) {
  udev_enumerate_add_match_parent(handle, parent.handle);
}

void device_enumeration::match_by_device_property(const char* property) {
  udev_enumerate_add_match_property(handle, "DEVTYPE", property);
}

void device_enumeration::match_by_subsystem(const char* subsystem) {
  udev_enumerate_add_match_subsystem(handle, subsystem);
}

std::vector<device> device_enumeration::scan() const {
  std::vector<device> devices{};

  udev_enumerate_scan_devices(handle);
  udev_list_entry* entries = udev_enumerate_get_list_entry(handle);
  udev_list_entry* entry;

  udev_list_entry_foreach(entry, entries) {
    const char* path = udev_list_entry_get_name(entry);
    auto udev_handle = udev_enumerate_get_udev(handle);
    devices.emplace_back(udev_handle, path);
  }

  return devices;
}

std::optional<device> device_enumeration::scan_first() const {
  udev_enumerate_scan_devices(handle);
  udev_list_entry* entries = udev_enumerate_get_list_entry(handle);
  udev_list_entry* entry;

  udev_list_entry_foreach(entry, entries) {
    const char* path = udev_list_entry_get_name(entry);
    auto udev_handle = udev_enumerate_get_udev(handle);
    return device{udev_handle, path};
  }

  return std::nullopt;
}

}  // namespace detail
}  // namespace service