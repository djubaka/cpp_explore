#pragma once

#include <libudev.h>
#include <optional>
#include <vector>
#include "device.hpp"

namespace service {
namespace detail {
struct device_enumeration {
  explicit device_enumeration(udev* udev_handle) {
    handle = udev_enumerate_new(udev_handle);
  }

  ~device_enumeration() {
    if (handle) {
      udev_enumerate_unref(handle);
    }
  }

  device_enumeration(const device_enumeration&) = delete;
  device_enumeration& operator=(const device_enumeration&) = delete;

  device_enumeration(device_enumeration&& x) noexcept : handle{x.handle} {
    x.handle = nullptr;
  }

  device_enumeration& operator=(device_enumeration&& x) noexcept {
    handle = x.handle;
    x.handle = nullptr;
    return *this;
  }

  void match_by_parent(device& parent);
  void match_by_device_property(const char* property);
  void match_by_subsystem(const char* subsystem);
  std::vector<device> scan() const;
  std::optional<device> scan_first() const;

  udev_enumerate* handle{nullptr};
};
}  // namespace detail
}  // namespace service