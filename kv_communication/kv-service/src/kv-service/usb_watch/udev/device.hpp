#pragma once

#include <libudev.h>
#include <optional>
#include <string>
#include <vector>

namespace service {
namespace detail {
struct device {
  explicit device(udev_device* dev_handle);
  explicit device(udev* udev_handle, const char* syspath);
  explicit device(udev_monitor* udev_mon);
  ~device();

  device(const device&) = delete;
  device& operator=(const device&) = delete;

  device(device&& x) noexcept : handle{x.handle} { x.handle = nullptr; }

  device& operator=(device&& x) noexcept {
    handle = x.handle;
    x.handle = nullptr;
    return *this;
  }

  std::string device_path() const;
  std::string device_node() const;
  std::string device_action() const;
  std::string device_system_path() const;
  std::optional<device> retrieve_child(const char* sub, const char* dev);
  std::optional<device> retrieve_parent(const char* sub, const char* dev);
  bool has_parent_as(const char* sub, const char* dev);

  udev_device* handle;
};
}  // namespace detail
}  // namespace service