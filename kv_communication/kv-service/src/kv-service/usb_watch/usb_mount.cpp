#include "kv-service/usb_watch/usb_mount.hpp"
#include <kv-util/filesystem.hpp>

namespace service {

const char *usb_mount::USB_MOUNT_PREFIX = "/media/the-usb-";

std::string usb_mount::mount(const std::string &dev_node, std::error_code &ec) {
  if (ec) {
    return std::string{};
  }

  auto mount_path = USB_MOUNT_PREFIX + dev_name(dev_node);
  if (filesystem::mount_usb(dev_node.c_str(), mount_path.c_str(), ec); ec) {
    return std::string{};
  }

  devices_.insert(std::make_pair(dev_node, mount_path));
  return mount_path;
}

void usb_mount::unmount(const std::string &dev_node, std::error_code &ec) {
  if (ec) {
    return;
  }

  if (auto it = devices_.find(dev_node); it != devices_.end()) {
    auto mount_path = it->second.c_str();
    if (filesystem::unmount_usb(mount_path, ec); ec.value() == EINVAL) {
      // this means that some other udev rule has allready unmounted
      // we should delete this directory anyway.
      ec = std::error_code{};
      filesystem::remove_dir(mount_path, ec);
    }

    devices_.erase(it);
  }
}

std::string usb_mount::dev_name(const std::string &dev_node) {
  auto last_fw_slash = dev_node.find_last_of('/');
  return (last_fw_slash != std::string::npos)
             ? dev_node.substr(last_fw_slash + 1)
             : dev_node;
}

}  // namespace service