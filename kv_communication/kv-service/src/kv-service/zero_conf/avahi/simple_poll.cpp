#include "simple_poll.hpp"
#include <avahi-common/error.h>
#include <avahi-common/simple-watch.h>
#include "error_code.hpp"

namespace service {
namespace avahi {

simple_poll::simple_poll() {
  std::error_code ec;
  if (init_poll(ec); ec) {
    throw avahi_exception{ec};
  }
}

void simple_poll::init_poll(std::error_code& ec) {
  poll_ = avahi_simple_poll_new();
  if (!poll_) {
    ec = make_error_code(AVAHI_ERR_FAILURE);
  }
}

simple_poll::~simple_poll() {
  if (poll_) {
    avahi_simple_poll_free(poll_);
    poll_ = nullptr;
  }
}

void simple_poll::run() { avahi_simple_poll_loop(poll_); }

void simple_poll::cancel() { avahi_simple_poll_quit(poll_); }

const AvahiPoll* simple_poll::handle() const {
  return avahi_simple_poll_get(poll_);
}

}  // namespace avahi
}  // namespace service