#pragma once

#include <sigslot/signal.hpp>
#include <string>
#include <system_error>

struct AvahiClient;
struct AvahiEntryGroup;

namespace service {

struct zero_conf_cfg;
namespace avahi {
class entry_group {
 public:
  explicit entry_group(AvahiClient*);
  ~entry_group() = default;

  void add_service(const service::zero_conf_cfg&, std::error_code& ec);
  void update_text_record(const service::zero_conf_cfg&, std::error_code& ec);
  void reset(std::error_code& ec);

  sigslot::signal<> established;
  sigslot::signal<> collision;
  sigslot::signal<const std::error_code&> failure;

 private:
  void init_entry(AvahiClient*, std::error_code&);
  void add_to_entry(const service::zero_conf_cfg&, std::error_code&);

 private:
  AvahiEntryGroup* handle_{nullptr};
};
}  // namespace avahi
}  // namespace service