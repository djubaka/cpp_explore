#pragma once

#include <exception>
#include <string>
#include <system_error>

namespace service {
namespace avahi {
class avahi_exception : public std::exception {
 public:
  explicit avahi_exception(std::error_code avahi_ec)
      : message_{avahi_ec.message()} {}

  const char* what() const noexcept override { return message_.c_str(); }

 private:
  std::string message_;
};

const std::error_category& category();

inline std::error_code make_error_code(int avahi_error) {
  return std::error_code{avahi_error, avahi::category()};
}

}  // namespace avahi
}  // namespace service