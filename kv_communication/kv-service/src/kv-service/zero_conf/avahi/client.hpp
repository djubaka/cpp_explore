#pragma once

#include <memory>
#include <sigslot/signal.hpp>
#include <string>
#include <system_error>
#include "kv-service/zero_conf/zero_conf.hpp"

struct AvahiClient;
struct AvahiPoll;

namespace service {

namespace avahi {

void client_callback(AvahiClient*, int, void*);
class entry_group;
class client {
 public:
  explicit client(zero_conf_cfg cfg);
  ~client();

  void commit(const AvahiPoll* poll);
  void modify_text_record(std::string);
  void reset();

  sigslot::signal<const std::error_code&> error;
  sigslot::signal<> established;

  friend void client_callback(AvahiClient*, int, void*);

 private:
  void init_client(const AvahiPoll*, std::error_code&);
  void init_entry_group();
  void on_group_collision();
  void on_group_error(const std::error_code& ec);
  void register_service();

 private:
  zero_conf_cfg cfg_;
  AvahiClient* handle_{nullptr};
  std::unique_ptr<entry_group> entry_;
};

}  // namespace avahi
}  // namespace service