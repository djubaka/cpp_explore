#include "error_code.hpp"
#include <avahi-common/error.h>

namespace service {
namespace avahi {
class avahi_error_category : public std::error_category {
  const char* name() const noexcept override {
    return "avahi::avahi_error_category";
  }

  std::string message(int value) const override {
    return ::avahi_strerror(value);
  }
};

const std::error_category& category() {
  static avahi_error_category instance;
  return instance;
}

}  // namespace avahi
}  // namespace service