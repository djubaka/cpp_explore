#include "client.hpp"
#include <avahi-client/client.h>
#include <avahi-common/alternative.h>
#include <avahi-common/error.h>
#include <avahi-common/malloc.h>
#include <avahi-common/watch.h>
#include "entry_group.hpp"
#include "error_code.hpp"

namespace service {
namespace avahi {

static void avahi_client_callback(AvahiClient* c, AvahiClientState state,
                                  void* user_data) {
  client_callback(c, static_cast<int>(state), user_data);
}

void client_callback(AvahiClient* c, int state, void* user_data) {
  auto* instance = static_cast<client*>(user_data);
  AvahiClientState av_state = static_cast<AvahiClientState>(state);

  if (!instance->handle_) {
    instance->handle_ = c;
    instance->init_entry_group();
  }

  switch (av_state) {
    case AVAHI_CLIENT_S_RUNNING:
      /* The server has startup successfully and registered its host
       * name on the network, so it's time to create our services */
      instance->register_service();
      break;

    case AVAHI_CLIENT_FAILURE:
      instance->error(make_error_code(avahi_client_errno(c)));
      break;

    case AVAHI_CLIENT_S_COLLISION:
    /* Let's drop our registered services. When the server is back
     * in AVAHI_SERVER_RUNNING state we will register them
     * again with the new host name. */
    case AVAHI_CLIENT_S_REGISTERING:
      /* The server records are now being established. This
       * might be caused by a host name change. We need to wait
       * for our own records to register until the host name is
       * properly established. */
      instance->reset();
      break;
    case AVAHI_CLIENT_CONNECTING:
      break;
  }
}

client::client(zero_conf_cfg cfg) : cfg_{std::move(cfg)} {}

void client::commit(const AvahiPoll* poll) {
  if (handle_) return;

  std::error_code ec;
  if (init_client(poll, ec); ec) {
    error(ec);
  }
}

void client::init_client(const AvahiPoll* poll, std::error_code& ec) {
  int err = 0;
  avahi_client_new(poll, static_cast<AvahiClientFlags>(0),
                   avahi_client_callback, this, &err);

  ec = make_error_code(err);
}

client::~client() {
  if (handle_) {
    avahi_client_free(handle_);
  }
}

void client::modify_text_record(std::string data) {
  cfg_.service_data = std::move(data);
  std::error_code ec;
  if (entry_->update_text_record(cfg_, ec); ec) {
    error(ec);
  }
}

void client::reset() {
  std::error_code ec;
  if (entry_->reset(ec); ec) {
    error(ec);
  }
}

void client::init_entry_group() {
  entry_ = std::make_unique<entry_group>(handle_);
  entry_->failure.connect(&client::on_group_error, this);
  entry_->collision.connect(&client::on_group_collision, this);
  entry_->established.connect([this] { established(); });
}

void client::on_group_collision() {
  char* alt_name = avahi_alternative_service_name(cfg_.service_name.c_str());
  cfg_.service_name = std::string{alt_name};
  avahi_free(alt_name);
  register_service();
}

void client::on_group_error(const std::error_code& ec) { error(ec); }

void client::register_service() {
  std::error_code ec;
  if (entry_->add_service(cfg_, ec); ec) {
    error(ec);
  }
}

}  // namespace avahi
}  // namespace service