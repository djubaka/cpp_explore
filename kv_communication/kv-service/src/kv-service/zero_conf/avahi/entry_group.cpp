#include "entry_group.hpp"
#include <avahi-client/client.h>
#include <avahi-client/publish.h>
#include <avahi-common/error.h>
#include "error_code.hpp"
#include "kv-service/zero_conf/zero_conf.hpp"

namespace service {
namespace avahi {

static void signal_failure(AvahiEntryGroup* g, entry_group* entry) {
  auto client = avahi_entry_group_get_client(g);
  auto error = avahi_client_errno(client);
  entry->failure(make_error_code(error));
}

static void entry_add_service(AvahiEntryGroup* handle,
                              const service::zero_conf_cfg& cfg,
                              std::error_code& ec) {
  int ret = avahi_entry_group_add_service(
      handle, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC,
      static_cast<AvahiPublishFlags>(0), cfg.service_name.c_str(),
      cfg.discovery_name.c_str(), nullptr, nullptr,
      static_cast<unsigned short>(cfg.port), cfg.service_data.c_str(), nullptr);

  ec = make_error_code(ret);
}

static void entry_update_service(AvahiEntryGroup* handle,
                                 const service::zero_conf_cfg& cfg,
                                 std::error_code& ec) {
  int ret = avahi_entry_group_update_service_txt(
      handle, AVAHI_IF_UNSPEC, AVAHI_PROTO_UNSPEC,
      static_cast<AvahiPublishFlags>(0), cfg.service_name.c_str(),
      cfg.discovery_name.c_str(), nullptr, cfg.service_data.c_str(), nullptr);

  ec = make_error_code(ret);
}

static void entry_group_callback(AvahiEntryGroup* g, AvahiEntryGroupState state,
                                 void* user_data) {
  auto instance = static_cast<entry_group*>(user_data);

  /* Called whenever the entry group state changes */
  switch (state) {
    case AVAHI_ENTRY_GROUP_ESTABLISHED:
      /* The entry group has been established successfully */
      instance->established();
      break;
    case AVAHI_ENTRY_GROUP_COLLISION:
      // A service name collision with a remote service happened.
      instance->collision();
      break;
    case AVAHI_ENTRY_GROUP_FAILURE:
      signal_failure(g, instance);
      break;
    case AVAHI_ENTRY_GROUP_UNCOMMITED:
    case AVAHI_ENTRY_GROUP_REGISTERING:
      break;
  }
}

entry_group::entry_group(AvahiClient* client) {
  std::error_code ec;
  if (init_entry(client, ec); ec) {
    throw avahi_exception{ec};
  }
}

void entry_group::init_entry(AvahiClient* client, std::error_code& ec) {
  handle_ = avahi_entry_group_new(client, entry_group_callback, this);
  if (!handle_) {
    ec = make_error_code(avahi_client_errno(client));
  }
}

void entry_group::add_service(const service::zero_conf_cfg& cfg,
                              std::error_code& ec) {
  if (ec) return;

  if (avahi_entry_group_is_empty(handle_)) {
    add_to_entry(cfg, ec);
  }
}

void entry_group::add_to_entry(const service::zero_conf_cfg& cfg,
                               std::error_code& ec) {
  if (ec) return;

  if (entry_add_service(handle_, cfg, ec); !ec) {
    auto res = avahi_entry_group_commit(handle_);
    ec = make_error_code(res);
  }

  if (ec.value() == AVAHI_ERR_COLLISION) {
    collision();
  }
}

void entry_group::update_text_record(const service::zero_conf_cfg& cfg,
                                     std::error_code& ec) {
  if (ec) return;
  entry_update_service(handle_, cfg, ec);
}

void entry_group::reset(std::error_code& ec) {
  if (ec) return;

  auto res = avahi_entry_group_reset(handle_);
  ec = make_error_code(res);
}

}  // namespace avahi
}  // namespace service