#pragma once

#include <system_error>

struct AvahiPoll;
struct AvahiSimplePoll;

namespace service {
namespace avahi {

class simple_poll {
 public:
  simple_poll();
  ~simple_poll();

  void run();
  void cancel();

  const AvahiPoll* handle() const;

 private:
  void init_poll(std::error_code&);

 private:
  AvahiSimplePoll* poll_{nullptr};
};
}  // namespace avahi
}  // namespace service