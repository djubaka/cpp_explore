#include "kv-service/zero_conf/zero_conf.hpp"
#include <algorithm>
#include "avahi/client.hpp"
#include "avahi/simple_poll.hpp"

namespace service {
zero_conf::zero_conf() : poll_{std::make_unique<avahi::simple_poll>()} {}

zero_conf::~zero_conf() {
  if (poll_thread_) {
    poll_->cancel();
    poll_thread_->join();
    poll_thread_ = std::nullopt;
  }
}

void zero_conf::publish(zero_conf_cfg cfg) {
  if (poll_thread_) {
    return;
  }

  client_ = std::make_unique<avahi::client>(std::move(cfg));
  client_->established.connect([this] { established(); });
  client_->error.connect([this](const auto& ec) { error_occured(ec); });
  client_->commit(poll_->handle());
  poll_thread_ = std::thread([this] { poll_->run(); });
}

void zero_conf::modify_service_data(std::string data) {
  if (client_) {
    client_->modify_text_record(std::move(data));
  }
}

}  // namespace service