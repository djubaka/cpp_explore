#include "fs_watcher_impl.hpp"
#include <fcntl.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <cerrno>
#include "kv-service/fs_watch/fs_watcher.hpp"

namespace service {
fs_watcher_impl::fs_watcher_impl(std::string path) : path_{std::move(path)} {
  init_fd();
  init_wd();
  configure_self_pipe();
}

fs_watcher_impl::~fs_watcher_impl() {
  if (wd_) {
    inotify_rm_watch(fd_, wd_);
    close(fd_);
  }

  if (pfd_[1]) {
    close(pfd_[1]);
  }

  if (pfd_[0]) {
    close(pfd_[0]);
  }
}

void fs_watcher_impl::init_fd() {
  if (ec_) return;

  if (fd_ = inotify_init(); fd_ == -1) {
    ec_ = std::error_code{errno, std::generic_category()};
  }
}

void fs_watcher_impl::init_wd() {
  if (ec_) return;

  std::uint32_t mask = IN_MODIFY | IN_DELETE | IN_CREATE;
  if (wd_ = inotify_add_watch(fd_, path_.c_str(), mask); wd_ == -1) {
    ec_ = std::error_code{errno, std::generic_category()};
  }
}

void fs_watcher_impl::configure_self_pipe() {
  if (ec_) return;

  if (pipe(pfd_) == -1) {
    return set_error_code(errno);
  }

  configure_pipe_end(pfd_[0]);
  configure_pipe_end(pfd_[1]);
}

void fs_watcher_impl::configure_pipe_end(int fd) {
  if (ec_) return;

  auto flags = fcntl(fd, F_GETFL);
  if (flags == -1) {
    return set_error_code(errno);
  }

  flags |= O_NONBLOCK;
  if (fcntl(fd, F_SETFL, flags) == -1) {
    return set_error_code(errno);
  }
}

std::vector<event_t> fs_watcher_impl::observe(std::error_code& ec) {
  if (ec_) {
    ec = ec_;
    return std::vector<event_t>{};
  }

  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(fd_, &readfds);
  FD_SET(pfd_[0], &readfds);

  std::vector<event_t> events;
  auto nfd = std::max(pfd_[0], fd_);
  auto ret = select(nfd + 1, &readfds, nullptr, nullptr, nullptr);
  if (ret == -1) {
    set_error_code(errno);
  } else if (FD_ISSET(fd_, &readfds)) {
    events = process_event();
  } else if (FD_ISSET(pfd_[0], &readfds)) {
    clean_up_pipe();
  }

  ec = ec_;
  return events;
}

std::vector<event_t> fs_watcher_impl::process_event() {
  std::vector<event_t> all_events;

  std::array<char, 16384> buff;
  auto length = read(fd_, buff.data(), buff.size());
  if (length == -1) {
    set_error_code(errno);
    return all_events;
  }

  int i = 0;
  while (i < length) {
    auto iev = reinterpret_cast<const inotify_event*>(&buff[i]);
    if (iev->len) {
      auto ev = which_event_type(iev->mask);
      auto path = which_event_path(iev->name, iev->len);
      all_events.emplace_back(std::move(path), ev);
    }

    i += sizeof(inotify_event) + iev->len;
  }

  return all_events;
}

std::string fs_watcher_impl::which_event_path(const char* event_name,
                                              std::uint32_t len) const {
  std::string full_path;
  full_path.reserve(path_.size() + len + 1);
  full_path += path_;
  full_path += '/';
  full_path += event_name;
  return full_path;
}

event_t::event fs_watcher_impl::which_event_type(std::uint32_t event_mask) {
  if (event_mask & IN_CREATE)
    return event_t::event::added;
  else if (event_mask & IN_DELETE) {
    return event_t::event::removed;
  } else if (event_mask & IN_MODIFY)
    return event_t::event::modified;

  return event_t::event::null;
}

void fs_watcher_impl::clean_up_pipe() {
  char ch;
  if (read(pfd_[0], &ch, 1) == -1) {
    set_error_code(errno);
  }
}

void fs_watcher_impl::cancel(std::error_code& ec) {
  if (write(pfd_[1], "x", 1) == -1) {
    set_error_code(errno);
    ec = ec_;
  }
}

void fs_watcher_impl::set_error_code(int error) {
  ec_ = std::error_code{errno, std::generic_category()};
}

}  // namespace service
