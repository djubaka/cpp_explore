#include "kv-service/fs_watch/fs_watcher.hpp"
#include "fs_watcher_impl.hpp"

namespace service {
fs_watcher::fs_watcher(std::string path)
    : impl_{std::make_unique<fs_watcher_impl>(std::move(path))} {}

fs_watcher::~fs_watcher() { stop_watch(); }

fs_watcher::fs_watcher(fs_watcher&&) = default;
fs_watcher& fs_watcher::operator=(fs_watcher&&) = default;

void fs_watcher::start_watch() {
  if (observer_thread_) return;

  observer_thread_ = std::thread([this]() { observe_event(); });
}

void fs_watcher::observe_event() {
  std::error_code ec;
  do {
    auto events = impl_->observe(ec);
    for (const auto& ev : events) {
      process_event(ev);
    }
  } while (!ec);

  if (ec) {
    error_occured(ec);
  }
}

void fs_watcher::process_event(const event_t& event) {
  switch (event.ev) {
    case event_t::event::added:
      created(event.file_name);
      break;
    case event_t::event::modified:
      modified(event.file_name);
      break;
    case event_t::event::removed:
      deleted(event.file_name);
      break;
    default:
      break;
  }
}

void fs_watcher::stop_watch() {
  if (observer_thread_) {
    std::error_code ec;
    impl_->cancel(ec);
    observer_thread_->join();
    observer_thread_ = std::nullopt;
  }
}

}  // namespace service