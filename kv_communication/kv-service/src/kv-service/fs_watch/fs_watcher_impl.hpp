#pragma once

#include <string>
#include <system_error>
#include <vector>

namespace service {
struct event_t {
  enum class event {
    null = 0,
    added = 1,
    removed = 2,
    modified = 3,
    renamed_old_name = 4,
    renamed_new_name = 5
  };

  std::string file_name;
  event ev;

  event_t() : ev{event::null} {}
  event_t(std::string f, event e) : file_name{std::move(f)}, ev{e} {}
};

class fs_watcher_impl {
 public:
  fs_watcher_impl(std::string path);
  ~fs_watcher_impl();

  fs_watcher_impl(const fs_watcher_impl&) = delete;
  fs_watcher_impl& operator=(const fs_watcher_impl&) = delete;

  fs_watcher_impl(fs_watcher_impl&&) = delete;
  fs_watcher_impl& operator=(fs_watcher_impl&&) = delete;

  std::vector<event_t> observe(std::error_code& ec);
  void cancel(std::error_code& ec);

 private:
  void init_fd();
  void init_wd();
  void configure_self_pipe();
  void configure_pipe_end(int pfd);
  void clean_up_pipe();
  void set_error_code(int error);
  std::vector<event_t> process_event();
  std::string which_event_path(const char* event_name, std::uint32_t len) const;
  static event_t::event which_event_type(std::uint32_t event_mask);

 private:
  int fd_{-1};
  int wd_{-1};
  int pfd_[2]{-1, -1};
  std::string path_;
  std::error_code ec_;
};
}  // namespace service