#pragma once

#include <memory>
#include <optional>
#include <sigslot/signal.hpp>
#include <string>
#include <system_error>
#include <thread>

namespace service {

class event_t;
class fs_watcher_impl;

class fs_watcher {
 public:
  explicit fs_watcher(std::string path);
  ~fs_watcher();

  fs_watcher(fs_watcher&&);
  fs_watcher& operator=(fs_watcher&&);

  void start_watch();
  void stop_watch();

  sigslot::signal<const std::string&> created;
  sigslot::signal<const std::string&> modified;
  sigslot::signal<const std::string&> deleted;
  sigslot::signal<const std::error_code&> error_occured;

 private:
  void observe_event();
  void process_event(const event_t& event);

 private:
  std::unique_ptr<fs_watcher_impl> impl_;
  std::optional<std::thread> observer_thread_;
};

}  // namespace service