#pragma once

#include <string>
#include <system_error>
#include <unordered_map>

namespace service {

class usb_mount {
  using mounted_devices = std::unordered_map<std::string, std::string>;

 public:
  std::string mount(const std::string& dev_node, std::error_code& ec);
  void unmount(const std::string& dev_node, std::error_code& ec);

 private:
  static std::string dev_name(const std::string& dev_node);

 private:
  mounted_devices devices_;
  static const char* USB_MOUNT_PREFIX;
};
}  // namespace service