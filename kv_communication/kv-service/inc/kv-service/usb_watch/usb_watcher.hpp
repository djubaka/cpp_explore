#pragma once

#include <memory>
#include <optional>
#include <sigslot/signal.hpp>
#include <string>
#include <system_error>
#include <thread>

namespace service {
namespace detail {
struct device;
}

class usb_monitor;
class usb_watcher {
 public:
  explicit usb_watcher();
  ~usb_watcher();

  void start_watch();
  void stop_watch();

  sigslot::signal<const std::string&> inserted;
  sigslot::signal<const std::string&> removed;
  sigslot::signal<const std::error_code&> error;

 private:
  void observe_changes();
  void process_device(detail::device& dev);

 private:
  std::unique_ptr<usb_monitor> mon_;
  std::optional<std::thread> observer_thread_;
  static const char* USB_MOUNT_PATH;
};

}  // namespace service