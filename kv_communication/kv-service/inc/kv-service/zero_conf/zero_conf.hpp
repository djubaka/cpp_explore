#pragma once

#include <optional>
#include <sigslot/signal.hpp>
#include <string>
#include <system_error>
#include <thread>

namespace service {
struct zero_conf_cfg {
  std::string service_name;
  std::string discovery_name;
  std::string service_data;
  std::uint32_t port;
};

namespace avahi {
class simple_poll;
class client;
}  // namespace avahi

class zero_conf {
 public:
  zero_conf();
  ~zero_conf();

  void publish(zero_conf_cfg cfg);
  void modify_service_data(std::string data);

  sigslot::signal<> established;
  sigslot::signal<const std::error_code&> error_occured;

 private:
  std::unique_ptr<avahi::simple_poll> poll_;
  std::unique_ptr<avahi::client> client_;
  std::optional<std::thread> poll_thread_;
};

}  // namespace service