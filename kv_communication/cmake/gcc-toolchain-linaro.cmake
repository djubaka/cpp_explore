set(linaro_tools $ENV{HOME}/linaro/gcc-linaro-7.3.1-2018.05-x86_64_arm-linux-gnueabihf)
set(linaro_sysroot ${linaro_tools}/arm-linux-gnueabihf/libc)

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_SYSROOT ${linaro_sysroot})
set(CMAKE_C_COMPILER ${linaro_tools}/bin/arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER ${linaro_tools}/bin/arm-linux-gnueabihf-g++)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)