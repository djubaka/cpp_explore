#pragma once

#include <algorithm>
#include <string>
#include <string_view>
#include <vector>

namespace string_helper {

inline void pad_left(std::string& s, const std::size_t n,
                     const char padding_char = ' ') {
  if (n > s.length()) {
    s.reserve(n);
    s.insert(0, n - s.size(), padding_char);
  }
}

inline void pad_right(std::string& s, const std::size_t n,
                      const char padding_char = ' ') {
  if (n > s.length()) {
    s.reserve(n);
    s.append(n - s.length(), padding_char);
  }
}

inline std::string to_upper(const std::string& source) {
  std::string upper;
  upper.reserve(source.size());
  std::transform(std::begin(source), std::end(source),
                 std::back_inserter(upper), ::toupper);
  return upper;
}

inline std::string to_lower(const std::string& source) {
  std::string lower;
  lower.reserve(source.size());
  std::transform(std::begin(source), std::end(source),
                 std::back_inserter(lower), ::tolower);
  return lower;
}

std::vector<std::string> split(const std::string& str,
                               const std::string& delims = " ");

std::vector<std::string_view> split_view(std::string_view strv,
                                         std::string_view delims = " ");

}  // namespace string_helper