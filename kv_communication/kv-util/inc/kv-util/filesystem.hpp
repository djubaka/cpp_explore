#pragma once

#include <system_error>

namespace filesystem {
bool file_exists(const char* path);
bool directory_exists(const char* path);
void create_dir(const char* path, std::error_code& ec);
void remove_dir(const char* path, std::error_code& ec);
void remove_empty_dir(const char* path, std::error_code& ec);
void mount_usb(const char* dev_node, const char* mount_path,
               std::error_code& ec);

void unmount_usb(const char* mount_path, std::error_code& ec);
void sync_fd(int fd, std::error_code& ec);

}  // namespace filesystem