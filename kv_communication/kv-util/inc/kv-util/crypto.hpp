#pragma once

#include <gsl/span>
#include <string>
#include <vector>

namespace crypto {
std::string encrypt(const std::string& in);
std::string decrypt(const std::string& in);

std::vector<std::string> encrypt(gsl::span<const std::string> lines);
std::vector<std::string> decrypt(gsl::span<const std::string> lines);
}  // namespace crypto