#pragma once

#include <cstdint>
#include <gsl/span>
#include <string_view>
#include <vector>

namespace compression {
std::vector<std::uint8_t> deflate(std::string_view);
std::vector<std::uint8_t> deflate(gsl::span<const std::uint8_t>);
std::vector<std::uint8_t> inflate(gsl::span<const std::uint8_t>);
}  // namespace compression