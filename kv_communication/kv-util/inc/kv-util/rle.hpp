#pragma once

#include <cstdint>
#include <gsl/span>
#include <vector>

namespace rle {
std::vector<std::uint8_t> encode(gsl::span<const std::uint8_t> in);
std::vector<std::uint8_t> decode(gsl::span<const std::uint8_t> in);
}  // namespace rle