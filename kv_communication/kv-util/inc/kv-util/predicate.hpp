#pragma once

#include <algorithm>
#include <cstring>

namespace predicate {
template <typename C1, typename C2>
bool are_equal(const C1& c1, const C2& c2) {
  return std::equal(std::begin(c1), std::end(c1), std::begin(c2), std::end(c2));
}

template <typename C>
bool are_equal(const C& c1, const char* c2) {
  return std::equal(std::begin(c1), std::end(c1), c2, c2 + std::strlen(c2));
}

template <typename C1, typename C2>
bool contains(const C1& haystack, const C2& needle) {
  return std::search(std::begin(haystack), std::end(haystack),
                     std::begin(needle),
                     std::end(needle)) != std::end(haystack);
}

template <typename C>
bool contains(const C& haystack, const char* needle) {
  return std::search(std::begin(haystack), std::end(haystack), needle,
                     needle + std::strlen(needle)) != std::end(haystack);
}

template <typename C>
bool starts_with(const C& haystack, const C& needle) {
  return needle.size() <= haystack.size() &&
         std::equal(needle.begin(), needle.end(), haystack.begin());
}

template <typename C>
bool starts_with(const C& haystack, const char* needle) {
  auto needle_len = std::strlen(needle);
  return needle_len <= haystack.size() &&
         std::equal(needle, needle + needle_len, haystack.begin());
}

template <typename C>
bool ends_with(const C& haystack, const C& needle) {
  return needle.size() <= haystack.size() &&
         std::equal(needle.rbegin(), needle.rend(), haystack.rbegin());
}

template <typename C>
bool ends_with(const C& haystack, const char* needle) {
  auto needle_len = std::strlen(needle);
  return needle_len <= haystack.size() &&
         std::equal(needle, needle + needle_len, haystack.end() - needle_len);
}

}  // namespace predicate