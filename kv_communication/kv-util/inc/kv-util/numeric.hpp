#pragma once

#include <array>
#include <cstdint>
#include <gsl/span>
#include <random>
#include <string_view>
#include <vector>

namespace numeric {

inline bool are_equal(double d1, double d2, double diff = 0.0001) {
  return std::abs(d1 - d2) <= diff;
}

inline bool is_zero(double d) { return are_equal(d, 0, 0.000001); }

inline double decimal_round(double value, unsigned decimal_places) {
  double multpl_fact = pow(10, decimal_places);
  double ceiling = static_cast<int>(value * multpl_fact + .5);
  return ceiling / multpl_fact;
}

std::vector<std::uint8_t> hex_to_numeric(gsl::span<const std::uint8_t> ascii);

std::array<std::uint16_t, 256> crc_16_table();

std::uint16_t crc16(gsl::span<const std::uint8_t> values);

int checksum(gsl::span<const std::uint8_t> values);

gsl::span<const std::uint8_t> as_bytes(std::string_view msg);

gsl::span<const std::uint8_t> as_bytes(const char* msg, std::size_t len);

std::vector<std::uint8_t> as_hex_string(int value, int digits);

template <typename Integral, std::size_t size>
std::array<Integral, size> random_bytes() {
  std::array<Integral, size> values{};

  std::random_device rd;
  std::mt19937 rng{rd()};
  std::uniform_int_distribution<Integral> uni{0x20, 0x4f};

  for (std::size_t i = 0; i < size; ++i) {
    values[i] = uni(rng);
  }

  return values;
}

}  // namespace numeric