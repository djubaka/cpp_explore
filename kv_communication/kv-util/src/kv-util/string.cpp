#include "kv-util/string.hpp"

namespace string_helper {
std::vector<std::string> split(const std::string& str,
                               const std::string& delims) {
  std::vector<std::string> output;
  auto first = std::cbegin(str);

  while (first != std::cend(str)) {
    const auto second = std::find_first_of(
        first, std::cend(str), std::cbegin(delims), std::cend(delims));

    if (first != second) output.emplace_back(first, second);

    if (second == std::cend(str)) break;

    first = std::next(second);
  }

  return output;
}

std::vector<std::string_view> split_view(std::string_view strv,
                                         std::string_view delims) {
  std::vector<std::string_view> output;
  auto first = strv.begin();

  while (first != strv.end()) {
    const auto second = std::find_first_of(
        first, std::cend(strv), std::cbegin(delims), std::cend(delims));
    if (first != second) {
      auto begin = std::distance(strv.begin(), first);
      auto len = std::distance(first, second);
      output.emplace_back(strv.substr(begin, len));
    }

    if (second == strv.end()) break;

    first = std::next(second);
  }

  return output;
}

}  // namespace string_helper