#include "kv-util/numeric.hpp"
#include <gsl/span_ext>
#include <iomanip>
#include <numeric>
#include <sstream>

namespace numeric {
std::vector<uint8_t> hex_to_numeric(gsl::span<const uint8_t> ascii) {
  auto ascii_to_num = [](std::uint8_t val) -> std::uint8_t {
    // if it's in the ascii range '0' to '9' => return actual values 0 to 9
    // if it's in the ascii range 'A' to 'F' => return actual values 10 to 15

    if (val >= 0x30 && val <= 0x39)
      return (val - 0x30);
    else
      return (val - 0x41 + 10);
  };

  std::vector<std::uint8_t> res;
  res.reserve(static_cast<decltype(res)::size_type>(ascii.size() / 2));

  for (int i = 0; i < ascii.size() / 2; ++i) {
    std::uint8_t val =
        16 * ascii_to_num(ascii[2 * i]) + ascii_to_num(ascii[2 * i + 1]);
    res.push_back(val);
  }

  return res;
}

std::array<uint16_t, 256> crc_16_table() {
  std::uint16_t p16 = 0xA001;
  std::array<std::uint16_t, 256> table{};

  for (std::size_t i = 0; i < table.size(); i++) {
    std::uint16_t crc = 0x00;
    std::uint16_t c = static_cast<std::uint16_t>(i);

    for (std::uint8_t j = 0; j < 8; j++) {
      if (((crc ^ c) & 0x0001) != 0)
        crc = ((crc >> 1) ^ p16);
      else
        crc = (crc >> 1);

      c = (c >> 1);
    }

    table[i] = crc;
  }

  return table;
}

uint16_t crc16(gsl::span<const uint8_t> values) {
  static auto crc_table = crc_16_table();

  auto accum_crc = [&](std::uint16_t prev, std::uint16_t current) {
    std::uint16_t short_c = (0x00ff & current);
    std::uint16_t tmp = (prev ^ short_c);

    return ((prev >> 8) ^ crc_table[tmp & 0xff]);
  };

  return static_cast<std::uint16_t>(
      std::accumulate(std::begin(values), std::end(values), 0x00, accum_crc));
}

gsl::span<const uint8_t> as_bytes(std::string_view msg) {
  return as_bytes(msg.data(), msg.length());
}

gsl::span<const uint8_t> as_bytes(const char *msg, std::size_t len) {
  return gsl::make_span(reinterpret_cast<const std::uint8_t *>(msg),
                        static_cast<int>(len));
}

int checksum(gsl::span<const uint8_t> values) {
  std::uint8_t sum = 0;
  for (auto b : values) {
    sum += b;
  }

  sum ^= 0xFF;
  sum++;
  return sum;
}

std::vector<uint8_t> as_hex_string(int value, int digits) {
  std::stringstream stream;
  stream << std::setfill('0');
  stream << std::setw(digits);
  stream << std::uppercase;
  stream << std::hex;
  stream << value;

  std::vector<std::uint8_t> out;
  std::copy(std::istream_iterator<std::uint8_t>{stream},
            std::istream_iterator<std::uint8_t>{}, std::back_inserter(out));

  return out;
}

}  // namespace numeric