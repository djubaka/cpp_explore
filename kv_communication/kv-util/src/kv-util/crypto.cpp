#include "kv-util/crypto.hpp"
#include <openssl/des.h>
#include <openssl/md5.h>
#include <cassert>
#include <functional>
#include <gsl/span_ext>
#include "kv-util/base64.hpp"
#include "kv-util/numeric.hpp"

namespace {

using key_pair_t = std::pair<DES_key_schedule, DES_key_schedule>;
key_pair_t generate_key_pair() {
  static std::array<std::uint8_t, 20> key = {'S', 'y', 'e', 'd', ' ', 'M', 'o',
                                             's', 'h', 'i', 'u', 'r', ' ', 'M',
                                             'u', 'r', 's', 'h', 'e', 'd'};

  std::array<std::uint8_t, 16> hash;
  MD5(key.data(), key.size(), hash.data());

  DES_cblock key1, key2;
  std::copy(hash.begin(), hash.begin() + 8, key1);
  std::copy(hash.begin() + 8, hash.end(), key2);

  DES_key_schedule key1_sch, key2_sch;
  DES_set_key_unchecked(&key1, &key1_sch);
  DES_set_key_unchecked(&key2, &key2_sch);

  return {key1_sch, key2_sch};
}

std::array<std::uint8_t, 8> do_encryption(gsl::span<const std::uint8_t> in,
                                          DES_key_schedule *key1_sch,
                                          DES_key_schedule *key2_sch, int enc) {
  assert(in.size() == 8);
  DES_cblock clear_text;
  DES_cblock cipher;
  std::array<std::uint8_t, 8> out;

  if (enc == DES_ENCRYPT) {
    std::copy(in.begin(), in.end(), clear_text);
    DES_ecb3_encrypt(&clear_text, &cipher, key1_sch, key2_sch, key1_sch, enc);
    std::copy(cipher, cipher + 8, out.begin());
  } else {
    std::copy(in.begin(), in.end(), cipher);
    DES_ecb3_encrypt(&cipher, &clear_text, key1_sch, key2_sch, key1_sch, enc);
    std::copy(clear_text, clear_text + 8, out.begin());
  }

  return out;
}

std::array<std::uint8_t, 8> encrypt_block(gsl::span<const std::uint8_t> in,
                                          DES_key_schedule *key1_sch,
                                          DES_key_schedule *key2_sch) {
  return do_encryption(in, key1_sch, key2_sch, DES_ENCRYPT);
}

std::array<std::uint8_t, 8> decrypt_block(gsl::span<const std::uint8_t> in,
                                          DES_key_schedule *key1_sch,
                                          DES_key_schedule *key2_sch) {
  return do_encryption(in, key1_sch, key2_sch, DES_DECRYPT);
}

std::pair<std::uint64_t, std::uint8_t> block_padding_info(
    const std::string &in) {
  // there is always one extra block for padding
  auto blocks = in.size() / 8 + 1;
  auto padding = blocks * 8 - in.size();
  return {blocks, padding};
}

std::vector<std::uint8_t> padded_input(const std::string &in,
                                       std::uint8_t padding) {
  std::vector<std::uint8_t> padded_in(in.size() + padding);
  std::copy(in.begin(), in.end(), padded_in.begin());

  auto fill_beg = padded_in.begin() + static_cast<long>(in.size());
  auto fill_end = padded_in.end();
  std::fill(fill_beg, fill_end, padding);

  return padded_in;
}

std::string encrypt_string(const std::string &in, DES_key_schedule *key1_sch,
                           DES_key_schedule *key2_sch) {
  auto [blocks, padding] = block_padding_info(in);
  auto padded_in = padded_input(in, padding);

  std::vector<std::uint8_t> out;
  out.reserve(padded_in.size());

  for (auto i = 0u; i < blocks; i++) {
    auto block_bytes = gsl::make_span(padded_in.data() + 8 * i, 8);
    auto cipher = encrypt_block(block_bytes, key1_sch, key2_sch);
    out.insert(out.end(), cipher.begin(), cipher.end());
  }

  return base64::encode(out.data(), static_cast<unsigned>(out.size()));
}

std::string decrypt_string(const std::string &in, DES_key_schedule *key1_sch,
                           DES_key_schedule *key2_sch) {
  auto decoded = base64::decode(in);
  auto blocks = decoded.size() / 8;

  std::vector<std::uint8_t> out;
  out.reserve(decoded.size());

  for (auto i = 0u; i < blocks; i++) {
    auto block_bytes = numeric::as_bytes(decoded.data() + 8 * i, 8);
    auto clear_text = decrypt_block(block_bytes, key1_sch, key2_sch);
    out.insert(out.end(), clear_text.begin(), clear_text.end());
  }

  // the last value gives away the number of padding bytes
  // ignore them.
  return std::string{out.begin(), out.end() - out.back()};
}

using encrypt_action_t = std::function<std::string(
    const std::string &, DES_key_schedule *, DES_key_schedule *)>;

std::vector<std::string> encrypt_collection(gsl::span<const std::string> lines,
                                            encrypt_action_t enc) {
  std::vector<std::string> out;
  out.reserve(static_cast<std::size_t>(lines.size()));

  auto [key1_sch, key2_sch] = generate_key_pair();
  for (const auto &in : lines) {
    out.push_back(enc(in, &key1_sch, &key2_sch));
  }

  return out;
}

}  // namespace

namespace crypto {
std::string encrypt(const std::string &in) {
  auto [key1_sch, key2_sch] = generate_key_pair();
  return encrypt_string(in, &key1_sch, &key2_sch);
}

std::string decrypt(const std::string &in) {
  auto [key1_sch, key2_sch] = generate_key_pair();
  return decrypt_string(in, &key1_sch, &key2_sch);
}

std::vector<std::string> encrypt(gsl::span<const std::string> lines) {
  return encrypt_collection(lines, encrypt_string);
}

std::vector<std::string> decrypt(gsl::span<const std::string> lines) {
  return encrypt_collection(lines, decrypt_string);
}
}  // namespace crypto