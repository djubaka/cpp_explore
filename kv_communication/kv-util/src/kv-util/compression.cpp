#include "kv-util/compression.hpp"
#include <zlib.h>
#include <algorithm>
#include <array>
#include <stdexcept>
#include "kv-util/numeric.hpp"

namespace compression {
namespace detail {

enum class compression_t { deflate, inflate };

template <compression_t compr>
std::vector<std::uint8_t> operate(gsl::span<const std::uint8_t> source) {
  z_stream zs;
  zs.zalloc = Z_NULL;
  zs.zfree = Z_NULL;
  zs.opaque = Z_NULL;

  if constexpr (compr == compression_t::deflate) {
    if (deflateInit(&zs, Z_BEST_COMPRESSION) != Z_OK) {
      throw std::runtime_error(zs.msg);
    }
  } else {
    if (inflateInit(&zs) != Z_OK) {
      throw std::runtime_error(zs.msg);
    }
  }

  zs.next_in = const_cast<std::uint8_t*>(source.data());
  zs.avail_in = source.size();

  std::array<std::uint8_t, 32768> buff;
  std::vector<std::uint8_t> result;
  result.reserve(source.size());

  int ret = Z_OK;
  do {
    zs.next_out = buff.data();
    zs.avail_out = buff.size();

    if constexpr (compr == compression_t::deflate) {
      ret = deflate(&zs, Z_FINISH);
    } else {
      ret = inflate(&zs, Z_NO_FLUSH);
    }

    auto consumed = buff.size() - zs.avail_out;
    if (result.size() < zs.total_out) {
      std::copy(buff.data(), buff.data() + consumed,
                std::back_inserter(result));
    }
  } while (ret == Z_OK);

  if constexpr (compr == compression_t::deflate) {
    deflateEnd(&zs);
  } else {
    inflateEnd(&zs);
  }

  if (ret != Z_STREAM_END) {
    throw std::runtime_error(zs.msg);
  }

  return result;
}

}  // namespace detail

std::vector<std::uint8_t> deflate(std::string_view source) {
  return deflate(numeric::as_bytes(source));
}

std::vector<std::uint8_t> deflate(gsl::span<const std::uint8_t> source) {
  return detail::operate<detail::compression_t::deflate>(source);
}

std::vector<std::uint8_t> inflate(gsl::span<const std::uint8_t> compressed) {
  return detail::operate<detail::compression_t::inflate>(compressed);
}

}  // namespace compression