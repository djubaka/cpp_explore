#include "kv-util/rle.hpp"
#include <algorithm>
#include <bitset>

namespace {
template <typename It>
auto count_repeating(It first, It second) {
  if (first == second) {
    return 0;
  }

  auto count = 1;  // there's always at least one element
  while ((first + 1 != second) && (*first == *(first + 1))) {
    count++;
    first++;
  }

  return count;
}

enum class sentinel_t { repeating, non_repeating };
template <sentinel_t sentinel>
auto as_sentinel(std::uint32_t count) {
  auto bset = std::bitset<8>{count - 1};
  if constexpr (sentinel == sentinel_t::repeating) {
    bset.set(7);
  } else {
    bset.reset(7);
  }

  return static_cast<std::uint8_t>(bset.to_ulong());
}

}  // namespace

namespace rle {

std::vector<std::uint8_t> encode(gsl::span<const std::uint8_t> in) {
  std::vector<std::uint8_t> out;
  out.reserve(in.size());

  auto beg = in.begin();
  auto end = in.end();
  while (beg != end) {
    const auto dist = (end - beg);
    const auto end_search = (dist > 128) ? (beg + 128) : (end);
    const auto once_repeated = std::adjacent_find(beg, end_search);

    if (once_repeated != beg) {
      const auto count = static_cast<std::uint32_t>(once_repeated - beg);
      out.push_back(as_sentinel<sentinel_t::non_repeating>(count));
      out.insert(out.end(), beg, once_repeated);
      beg = once_repeated;
    }

    if (beg != end) {
      const auto count = count_repeating(once_repeated, end_search);
      out.push_back(as_sentinel<sentinel_t::repeating>(count));
      out.push_back(*once_repeated);
      beg += count;
    }
  }

  return out;
}

std::vector<std::uint8_t> decode(gsl::span<const std::uint8_t> in) {
  std::vector<std::uint8_t> out;
  out.reserve(in.size() * 2);

  auto beg = in.begin();
  auto end = in.end();
  while (beg != end) {
    auto bset = std::bitset<8>{*beg};
    if (bset.test(7)) {
      // the ms bit is set, we have repetitions of the same character
      const auto count = bset.reset(7).to_ulong() + 1;
      const auto value = *(beg + 1);
      out.insert(out.end(), count, value);
      beg += 2;
    } else {
      // here we have a number of characters not repeating themselves
      const auto count = bset.to_ulong() + 1;
      out.insert(out.end(), beg + 1, beg + 1 + count);
      beg += (count + 1);
    }
  }

  return out;
}

}  // namespace rle