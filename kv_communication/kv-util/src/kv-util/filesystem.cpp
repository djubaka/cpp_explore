#include "kv-util/filesystem.hpp"
#include <ftw.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <unistd.h>

namespace {
static int remove_callback(const char* path, const struct stat* sb,
                           int typeflag, struct FTW* ftwb) {
  (void)sb;
  (void)ftw;

  if (typeflag == FTW_D || FTW_D)
    return rmdir(path);
  else if (typeflag == FTW_F)
    return remove(path);

  return 0;
}

static void create_dir_if_not_present(const char* path, std::error_code& ec) {
  if (ec) return;

  if (!filesystem::directory_exists(path)) {
    filesystem::create_dir(path, ec);
  }
}

static void mount_as_usb(const char* src, const char* target,
                         std::error_code& ec) {
  if (ec) return;

  auto m_fs = "vfat";
  auto m_flags = MS_SYNCHRONOUS;
  if (mount(src, target, m_fs, m_flags, nullptr) == -1) {
    ec = std::error_code{errno, std::generic_category()};
  }
}

}  // namespace

namespace filesystem {
bool file_exists(const char* path) { return access(path, F_OK) != -1; }

bool directory_exists(const char* dir_path) {
  struct stat sb;
  return stat(dir_path, &sb) == 0 && S_ISDIR(sb.st_mode);
}

void create_dir(const char* path, std::error_code& ec) {
  if (ec) return;

  if (mkdir(path, S_IRWXU | S_IRWXG | S_IRWXO) == -1) {
    ec = std::error_code{errno, std::generic_category()};
  }
}

void remove_dir(const char* path, std::error_code& ec) {
  if (ec) return;

  auto flags = FTW_DEPTH | FTW_MOUNT | FTW_PHYS;
  if (nftw(path, remove_callback, 64, flags) == -1) {
    ec = std::error_code{errno, std::generic_category()};
  }
}

void remove_empty_dir(const char* path, std::error_code& ec) {
  if (ec) return;

  if (rmdir(path) == -1) {
    ec = std::error_code{errno, std::generic_category()};
  }
}

void mount_usb(const char* dev_node, const char* mount_path,
               std::error_code& ec) {
  create_dir_if_not_present(mount_path, ec);
  mount_as_usb(dev_node, mount_path, ec);
}

void unmount_usb(const char* mount_path, std::error_code& ec) {
  if (ec) return;

  if (umount(mount_path) != -1) {
    remove_empty_dir(mount_path, ec);
  } else {
    ec = std::error_code{errno, std::generic_category()};
  }
}

void sync_fd(int fd, std::error_code& ec) {
  int ret;
  do {
#if defined(_POSIX_SYNCHRONIZED_IO) && _POSIX_SYNCHRONIZED_IO > 0
    ret = fdatasync(fd);
#else
    ret = fsync(fd);
#endif
  } while (ret == -1 && errno == EINTR);

  if (ret != 0) {
    ec = std::error_code{errno, std::generic_category()};
  }
}

}  // namespace filesystem