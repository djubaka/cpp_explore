#!/bin/sh 

# exit if any command fails with error code
set -e 

export QT_QPA_EGLFS_TSLIB=1
export TSLIB_TSDEVICE=/dev/input/touchscreen0
export TSLIB_CALIBFILE=/etc/pointercal
export QT_QPA_FB_DISABLE_INPUT=1
export QT_QPA_EGLFS_NO_LIBINPUT=1
export QT_QPA_EGLFS_WIDTH=1280
export QT_QPA_EGLFS_HEIGHT=800
export QT_EGLFS_IMX6_NO_FB_MULTI_BUFFER=1

cd /opt/serial-gui

# start the app
# pass the necessary plaftorm specification for qt 
./serial-gui --platform eglfs &