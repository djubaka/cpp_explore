#!/bin/bash

if [ -d "build-imx6" ]; then
	rm -r build-imx6
fi

mkdir build-imx6 && cd build-imx6
cmake \
-DCMAKE_BUILD_TYPE=Release \
-DCMAKE_TOOLCHAIN_FILE=../cmake/gcc-toolchain-yocto.cmake \
-DKV_CONAN_PROFILE=../conan/conan-profile-yocto .. \
-G "Ninja"

ninja -j8