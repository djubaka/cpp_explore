#!/bin/bash

## clean up if anything left from previous attempts
if [ -d "deploy" ]; then
	rm -r deploy
fi

## check if user provided any input to build directory
if [ $# -eq 0 ]; then
	echo "Path to build directory needed."
	exit 1
fi

## check if the specified build directory exists
if [ ! -d "$1" ]; then
	echo "$1 does not exist."
	exit 1
fi

build_dir=$1

## create folder structure that simulates the target machine
mkdir -p deploy/opt/serial-gui
mkdir -p deploy/usr/bin

## copy all files needed to run the application on the target machine
cp $build_dir/bin/serial-gui deploy/opt/serial-gui

## copy all script files needed on the target machine
cp serial_init.sh deploy/usr/bin

cd deploy

## create the compressed tar file
tar -cf serial-gui.tar opt
tar -rf serial-gui.tar usr
gzip serial-gui.tar
mv serial-gui.tar.gz ../serial-gui.setup

## clean up
cd ..
rm -r deploy