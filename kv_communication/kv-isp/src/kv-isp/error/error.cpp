#include "kv-isp/error/error.hpp"

namespace {

struct isp_error_category : public std::error_category {
 public:
  const char *name() const noexcept override { return "isp_error"; }

  std::string message(int ev) const override {
    switch (static_cast<isp::isp_error>(ev)) {
      case isp::isp_error::init_timedout:
        return "isp init timed out";
      case isp::isp_error::init_failure:
        return "isp init failed";
      case isp::isp_error::memory_erase_timedout:
        return "memory erase timed out";
      case isp::isp_error::memory_erase_failure:
        return "memory erase failed";
      case isp::isp_error::page_write_timedout:
        return "page write timed out";
      case isp::isp_error::page_write_failure:
        return "page write failed";
      case isp::isp_error::crc_timedout:
        return "crc timed out";
      case isp::isp_error::crc_verification_failure:
        return "crc values verification failed";
      case isp::isp_error::send_to_boot_failure:
        return "sending to boot failed";
      case isp::isp_error::leave_from_boot_failure:
        return "leaving boot mode failed";
      case isp::isp_error::not_in_boot_mode:
        return "processor not in boot mode";
      case isp::isp_error::boot_echo_failure:
        return "boot echo failed";
      case isp::isp_error::boot_baudrate_failure:
        return "failed to find proper boot baudrate";
      case isp::isp_error::dvfc_read_failure:
        return "dvfc file read error";
      case isp::isp_error::dvfc_checksum_error:
        return "dvfc checksum error. file corrupted";
      case isp::isp_error::dvfc_identity_record_error:
        return "cannot identify device from dvfc file";
      case isp::isp_error::dvfc_hdc_mismatch:
        return "the hdc on the device and the dvfc don't match";
      case isp::isp_error::timed_execution_error:
        return "deferred execution failure";
    }

    return "uknown isp error.";
  }
};

const isp_error_category isp_category{};

}  // namespace

namespace isp {
std::error_code make_error_code(isp::isp_error error) {
  return {static_cast<int>(error), isp_category};
}
}  // namespace isp