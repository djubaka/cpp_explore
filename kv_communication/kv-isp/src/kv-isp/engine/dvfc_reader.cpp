#include "dvfc_reader.hpp"
#include <algorithm>
#include <iterator>
#include <kv-util/crypto.hpp>
#include <kv-util/numeric.hpp>
#include <kv-util/predicate.hpp>
#include <kv-util/string.hpp>
#include <sstream>
#include <string>
#include <vector>
#include "kv-isp/error/error.hpp"

namespace {

int hex_string_to_int(std::string_view hex_val) {
  std::stringstream sstr;
  sstr << std::hex;
  sstr << hex_val;

  int res;
  sstr >> res;

  return res;
}

std::string parse_line(const std::string& line, const std::string& cks,
                       std::error_code& ec) {
  auto decrypted_line = crypto::decrypt(line);
  auto lcks = numeric::checksum(numeric::as_bytes(decrypted_line));
  auto decrypted_cks = crypto::decrypt(cks);
  auto fcks = hex_string_to_int(decrypted_cks);

  if (lcks != fcks) {
    ec = isp::isp_error::dvfc_checksum_error;
  }

  return decrypted_line;
}

}  // namespace

namespace isp {
std::vector<std::string> dvfc_to_hex_lines(std::istream& stream,
                                           std::error_code& ec) {
  std::vector<std::string> out;
  out.reserve(15000);

  std::string line;
  std::string cks;
  while (stream) {
    std::getline(stream, line);
    std::getline(stream, cks);
    if (!stream) {
      // it's just the end of file, not an error.
      if (!stream.eof()) {
        ec = isp_error::dvfc_read_failure;
      }
      break;
    }

    auto hex_line = parse_line(line, cks, ec);
    if (ec) break;
    out.push_back(std::move(hex_line));
  }

  // there should be something in there ate least.
  if (out.empty()) {
    ec = isp_error::dvfc_read_failure;
  }

  return out;
}

std::vector<std::string> raw_hex_lines(std::istream& stream,
                                       std::error_code& ec) {
  std::vector<std::string> out;
  out.reserve(15000);

  std::string line;
  while (std::getline(stream, line)) {
    out.push_back(line);
  }

  return out;
}

void validate_hdc_code(const std::string& id_line_from_dvfc,
                       const std::string& device_hdc, std::error_code& ec) {
  if (ec) return;

  // the first line should be an identity line.
  // it should contain info about:
  // 1. Is the processor Master or Slave
  // 2. Type of processor (ARM, ATMEGA, ...)
  // 3. the complete device code, including HDC
  // the id should look like this =>
  // Master Processor ARM, Code: TRT400_TR40-11.01_RTB40050

  if (!predicate::contains(id_line_from_dvfc, "Master Processor ARM, Code: ")) {
    ec = isp_error::dvfc_identity_record_error;
  }

  auto id_line_split = string_helper::split_view(id_line_from_dvfc, "_");
  if (id_line_split.empty() || id_line_split.back() != device_hdc) {
    ec = isp_error::dvfc_hdc_mismatch;
  }
}

}  // namespace isp