#pragma once

#include <istream>
#include <string>
#include <system_error>
#include <vector>

namespace isp {
std::vector<std::string> dvfc_to_hex_lines(std::istream& stream,
                                           std::error_code& ec);

std::vector<std::string> raw_hex_lines(std::istream& stream,
                                       std::error_code& ec);

void validate_hdc_code(const std::string& id_line_from_dvfc,
                       const std::string& device_hdc, std::error_code& ec);
}  // namespace isp