#include "kv-isp/engine/firmware_upgrade.hpp"
#include <fstream>
#include <gsl/span_ext>
#include <kv-communication/icommunication.hpp>
#include <kv-util/predicate.hpp>
#include "dvfc_reader.hpp"
#include "kv-isp/error/error.hpp"
#include "kv-isp/processor/device_io.hpp"

namespace isp {
firmware_upgrade::firmware_upgrade(KvCommunication::ICommunication *io,
                                   processor_preparation prep,
                                   processor_creation creation)
    : io_{io}, prep_{std::move(prep)}, creation_{std::move(creation)} {}

firmware_upgrade::~firmware_upgrade() {
  if (reading_thread_) {
    reading_thread_->join();
  }
}

void firmware_upgrade::execute(const std::string &fw_file_path) {
  if (reading_thread_) return;

  reading_thread_ = std::thread([this, path = fw_file_path] {
    auto read_delegate =
        predicate::ends_with(path, ".hex") ? from_hex_file : from_dvfc_file;

    read_firmware(path, read_delegate);
  });
}

std::vector<std::string> firmware_upgrade::from_hex_file(
    const std::string &path, std::error_code &ec) {
  std::ifstream is{path};
  return raw_hex_lines(is, ec);
}

std::vector<std::string> firmware_upgrade::from_dvfc_file(
    const std::string &path, std::error_code &ec) {
  std::ifstream is{path};

  auto hex_lines_with_id = dvfc_to_hex_lines(is, ec);
  // we should validate the id now. TO DO.
  // validate_hdc_code(hex_lines_with_id[0], "SOME_HDC_CODE", ec);

  // when reading from dvfc, the first line is the id line. skip it.
  return {hex_lines_with_id.begin() + 1, hex_lines_with_id.end()};
}

void firmware_upgrade::read_firmware(const std::string &path,
                                     firmware_read_t read_delegate) {
  reading_file_begin();
  std::error_code ec;
  auto hex_lines = read_delegate(path, ec);
  reading_file_end(ec);

  if (!ec) {
    on_hex_lines_read(std::move(hex_lines));
  }
}

bool firmware_upgrade::is_hdc_mismatch(const std::error_code &ec) const {
  return ec == isp_error::dvfc_hdc_mismatch;
}

bool firmware_upgrade::is_dvfc_corrupted(const std::error_code &ec) const {
  return ec == isp_error::dvfc_read_failure ||
         ec == isp_error::dvfc_checksum_error ||
         ec == isp_error::dvfc_identity_record_error;
}

void firmware_upgrade::on_hex_lines_read(std::vector<std::string> hex_lines) {
  proc_ = creation_.create(hex_lines);
  send_to_boot();
}

void firmware_upgrade::send_to_boot() {
  entering_boot_begin();

  // just in case there were some garbage values inside the buffer.
  std::error_code err;
  io_->clear_buffer(err);

  prep_.send_to_boot_async(io_, [this](const std::error_code &ec) {
    entering_boot_end(ec);

    // whatever the outcome is here, try configuring boot
    // because the device could be inside the bootloader from a previous run
    configure_boot();
  });
}

void firmware_upgrade::configure_boot() {
  configuring_boot_begin();
  prep_.configure_io_in_boot(io_, [this](const std::error_code &ec) {
    if (!ec) {
      check_if_in_boot();
    } else {
      configuring_boot_end(ec);
    }
  });
}

void firmware_upgrade::check_if_in_boot() {
  prep_.check_if_in_boot_async(io_, [this](const std::error_code &ec) {
    configuring_boot_end(ec);
    if (!ec) {
      proc_init();
    }
  });
}

void firmware_upgrade::proc_init() {
  processor_init_begin();
  proc_->init(io_, [this](const std::error_code &ec) {
    processor_init_end(ec);
    if (!ec) {
      erase_memory();
    }
  });
}

void firmware_upgrade::erase_memory() {
  erasing_memory_begin();
  proc_->memory_erase(io_, [this](const std::error_code &ec) {
    erasing_memory_end(ec);
    if (!ec) {
      program_memory();
    }
  });
}

void firmware_upgrade::program_memory() {
  auto progress = [this](int value) { programming_memory_progress(value); };

  auto stage = [this](const std::error_code &ec) {
    programming_memory_end(ec);
    if (!ec) {
      verify();
    }
  };

  programming_memory_begin();
  proc_->program(io_, stage, progress);
}

void firmware_upgrade::verify() {
  auto progress = [this](int value) { verifying_memory_progress(value); };

  auto stage = [this](const std::error_code &ec) {
    verifying_memory_end(ec);
    if (!ec) {
      leave_boot();
    }
  };

  verifying_memory_begin();
  proc_->verify(io_, stage, progress);
}

void firmware_upgrade::leave_boot() {
  leaving_boot_begin();
  prep_.leave_from_boot_async(io_, [this](const std::error_code &ec) {
    detail::configure_default_baudrate();
    leaving_boot_end(ec);
  });
}

}  // namespace isp