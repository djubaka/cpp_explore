#pragma once

#include <array>
#include <functional>
#include <system_error>

namespace KvCommunication {
class ICommunication;
}

namespace isp {

// implements processor_preparation_concept
class stm_arm_preparation {
 public:
  using callback_t = std::function<void(const std::error_code&)>;
  void send_to_boot_async(KvCommunication::ICommunication* io,
                          callback_t callback) const;
  void configure_io_in_boot(KvCommunication::ICommunication* io,
                            callback_t callback) const;
  void leave_from_boot_async(KvCommunication::ICommunication* io,
                             callback_t callback) const;
  void check_if_in_boot_async(KvCommunication::ICommunication* io,
                              callback_t callback) const;

 private:
  static const std::array<std::uint8_t, 5> APP_START_CMD;
  static const std::array<std::uint8_t, 5> BOOT_CMD;
};
}  // namespace isp