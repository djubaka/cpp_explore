#include "stm_boot_check.hpp"
#include <kv-communication/icommunication.hpp>
#include "kv-isp/error/error.hpp"
#include "kv-util/predicate.hpp"

namespace isp {

using namespace KvCommunication;

const std::array<std::uint8_t, 3> stm_boot_check::BOOT_CHECK = {'B', 'I', '\n'};

std::shared_ptr<stm_boot_check> stm_boot_check::create(
    KvCommunication::ICommunication *io, user_cb cb) {
  return std::shared_ptr<stm_boot_check>(new stm_boot_check{io, std::move(cb)});
}

stm_boot_check::stm_boot_check(KvCommunication::ICommunication *io, user_cb cb)
    : io_{io}, cb_{std::move(cb)} {}

void stm_boot_check::execute() {
  auto on_res = [this, self = shared_from_this()](data_span data,
                                                  const std::error_code &ec) {
    if (ec) {
      return cb_(isp_error::not_in_boot_mode);
    }

    if (predicate::contains(data, "BI")) {
      cb_(ec);
    } else if (predicate::are_equal(data, "NOK\r\n")) {
      try_again();
    } else {
      cb_(isp_error::not_in_boot_mode);
    }
  };

  io_->async_upload(BOOT_CHECK, std::move(on_res), match<new_line>());
}

void stm_boot_check::try_again() {
  retry_count_++;
  return retry_count_ < 3 ? execute() : cb_(isp_error::not_in_boot_mode);
}

}  // namespace isp