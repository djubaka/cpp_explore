#include "stm_arm_max_baudrate_search.hpp"
#include <kv-communication/icommunication.hpp>
#include <kv-util/numeric.hpp>
#include <kv-util/predicate.hpp>
#include "device_io.hpp"
#include "kv-isp/error/error.hpp"
#include "stm_arm_preparation.hpp"

namespace isp {

using namespace KvCommunication;

stm_max_baudrate_search::stm_max_baudrate_search(uint8_t baud_index,
                                                 baudrate_search_context ctx)
    : stm_initial_baudrate_search(baud_index, std::move(ctx)) {}

void stm_max_baudrate_search::find_baudrate() { configure_temp_baud(); }

void stm_max_baudrate_search::configure_temp_baud() {
  // this will ask the processor to temporarily set this baudrate
  // if no communication is done in a couple of seconds, the processor returns
  // to the previous baud
  auto on_res = [this, self = shared_from_this()](const std::error_code &ec) {
    if (ec) {
      revert_to_previous_baud();
    } else {
      on_temp_baud_config();
    }
  };

  configure_baud(current_as_ascii_char(), '1', std::move(on_res));
}

void stm_max_baudrate_search::on_temp_baud_config() {
  auto on_res = [this, self = shared_from_this()](const std::error_code &ec) {
    if (ec) {
      // the previous was the maximum possible.
      revert_to_previous_baud();
    } else {
      // since this one is ok, make it permanent
      configure_permanent_baud();
    }
  };

  test_baudrate(current_baud(), std::move(on_res));
}

void stm_max_baudrate_search::configure_permanent_baud() {
  // this will set the processor baudrate to a permanent value
  auto on_res = [this, self = shared_from_this()](const std::error_code &ec) {
    if (ec) {
      revert_to_previous_baud();
    } else {
      on_permanent_baud_config();
    }
  };

  configure_baud(current_as_ascii_char(), '2', std::move(on_res));
}

void stm_max_baudrate_search::on_permanent_baud_config() {
  if (has_next()) {
    // repeat the cycle.
    move_to_next_baud();
    find_baudrate();
  } else {
    // we've reached the end. declare victory
    ctx_.callback(std::error_code{});
  }
}

void stm_max_baudrate_search::configure_baud(
    uint8_t baud_index, uint8_t baud_mode,
    stm_initial_baudrate_search::callback_t callback) {
  auto on_res = [cb = std::move(callback)](data_span data,
                                           const std::error_code &ec) {
    if (ec) {
      cb(ec);
    } else {
      if (predicate::are_equal(data, "OK\r\n")) {
        cb(std::error_code{});
      } else {
        cb(isp_error::boot_baudrate_failure);
      }
    }
  };

  baud_cfg_cmd_ = {'B', 'C', 'S', baud_index, baud_mode, '\n'};
  ctx_.io->async_upload(baud_cfg_cmd_, on_res, match<new_line>());
}

void stm_max_baudrate_search::revert_to_previous_baud() {
  // reconfigure io with the previous options.
  // signal completion in 2 seconds, since this is time it takes for the
  // processor to revert back

  auto baud = move_to_prev_baud();
  detail::configure_baudrate(baud);

  std::error_code ec;
  detail::wait_for(std::chrono::milliseconds{2000}, ec);
  ctx_.callback(ec);
}
}  // namespace isp