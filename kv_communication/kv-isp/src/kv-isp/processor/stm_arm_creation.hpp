#pragma once

#include <array>
#include <cstdint>
#include <gsl/span>
#include <string>
#include <vector>
#include "kv-isp/processor/processor.hpp"

namespace isp {
enum class intel_record_type {
  data = 0x00,
  end_of_file = 0x01,
  ext_seg_addr = 0x02,
  start_seg_addr = 0x03,
  ext_lin_addr = 0x04,
  start_lin_addr = 0x05
};

struct intel_line_info {
  using data_t = std::vector<std::uint8_t>;

  data_t data;
  std::uint32_t data_size;
  std::uint32_t data_address;
  intel_record_type record_type;
};

// implements processor_creation concept
class stm_arm_creation {
  static const std::uint32_t PAGE_SIZE = 2048;
  static const std::uint32_t CPU_FLASH_SIZE = 512 * 1024;

 public:
  using hex_lines_t = gsl::span<const std::string>;
  stm_arm_creation();
  processor create(hex_lines_t command_lines) const;

 private:
  void populate_cpu_info(gsl::span<const intel_line_info> line_infos) const;
  void populate_cpu_flash_data(std::uint32_t seg_addr,
                               std::uint32_t start_lin_addr,
                               const intel_line_info& li) const;
  void populate_cpu_page_data() const;
  static std::vector<intel_line_info> as_line_infos(hex_lines_t command_lines);
  static intel_line_info parse_hex_line(std::string_view hex_line);
  static intel_line_info recreate_line_info(gsl::span<const std::uint8_t>);
  static void throw_on_checksum_failure(gsl::span<const std::uint8_t>,
                                        intel_record_type);
  static std::uint32_t calculate_address(std::uint8_t high, std::uint8_t low);

 private:
  mutable std::array<std::uint8_t, CPU_FLASH_SIZE> cpu_data_buff_;
  mutable std::array<std::uint32_t, CPU_FLASH_SIZE / PAGE_SIZE> cpu_page_buff_;
};
}  // namespace isp