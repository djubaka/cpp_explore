#include "stm_arm_preparation.hpp"
#include <kv-communication/icommunication.hpp>
#include <kv-util/predicate.hpp>
#include "device_io.hpp"
#include "kv-isp/error/error.hpp"
#include "stm_arm_initial_baudrate_search.hpp"
#include "stm_boot_check.hpp"

namespace isp {

using namespace KvCommunication;
using namespace std::chrono;

static bool boot_response_right(gsl::span<const std::uint8_t> data) {
  std::array<std::uint8_t, 4> default_res{'O', 'K', 0x0d, 0x0a};
  std::array<std::uint8_t, 6> measure_res{0x1b, 'm', 'O', 'K', 0x0d, 0x0a};
  return predicate::are_equal(data, default_res) ||
         predicate::are_equal(data, measure_res);
}

static void execute_in(std::chrono::milliseconds time,
                       const stm_arm_preparation::callback_t& callb) {
  std::error_code ec;
  detail::wait_for(time, ec);
  callb(ec);
}

const std::array<std::uint8_t, 5> stm_arm_preparation::APP_START_CMD = {
    'A', 'S', 'A', 'S', '\n'};

const std::array<std::uint8_t, 5> stm_arm_preparation::BOOT_CMD = {
    0x1b, 'R', 'B', 'S', 0x00};

void stm_arm_preparation::send_to_boot_async(
    KvCommunication::ICommunication* io,
    stm_arm_preparation::callback_t callback) const {
  auto on_res = [cb = std::move(callback), this](
                    KvCommunication::data_span data,
                    const std::error_code& ec) {
    if (ec) {
      cb(isp_error::send_to_boot_failure);
    } else {
      if (boot_response_right(data)) {
        execute_in(milliseconds{1000}, cb);
      } else {
        cb(isp_error::send_to_boot_failure);
      }
    }
  };

  io->async_upload(BOOT_CMD, std::move(on_res), match<new_line>());
}

void stm_arm_preparation::configure_io_in_boot(
    KvCommunication::ICommunication* io,
    stm_arm_preparation::callback_t callback) const {
  baudrate_search_context ctx = {io, callback};
  auto baudrate_search = std::make_shared<stm_initial_baudrate_search>(ctx);
  baudrate_search->execute();
}

void stm_arm_preparation::leave_from_boot_async(
    KvCommunication::ICommunication* io,
    stm_arm_preparation::callback_t callback) const {
  auto on_res = [cb = std::move(callback)](KvCommunication::data_span data,
                                           const std::error_code& ec) {
    if (detail::is_timeout_error(ec)) {
      // it's supposed to time out.
      cb(std::error_code{});
    } else if (ec) {
      // any other error is really an error.
      cb(isp_error::leave_from_boot_failure);
    } else {
      if (predicate::are_equal(data, "NOK\r\n"))
        cb(isp_error::leave_from_boot_failure);
      else
        cb(std::error_code{});
    }
  };

  io->configure_timeout(milliseconds{2000});
  io->async_upload(APP_START_CMD, on_res, match<new_line>());
}

void stm_arm_preparation::check_if_in_boot_async(
    KvCommunication::ICommunication* io,
    stm_arm_preparation::callback_t callback) const {
  auto boot_check = stm_boot_check::create(io, std::move(callback));
  boot_check->execute();
}
}  // namespace isp