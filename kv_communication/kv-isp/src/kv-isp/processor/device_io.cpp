#include "device_io.hpp"
#include <sys/select.h>
#include <kv-communication/communication_engine.hpp>
#include "kv-isp/error/error.hpp"

namespace isp {
namespace detail {

void wait_for(std::chrono::milliseconds interval, std::error_code& ec) {
  timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = std::chrono::microseconds{interval}.count();
  if (auto res = select(0, nullptr, nullptr, nullptr, &tv); res != 0) {
    ec = (res == -1) ? std::error_code{errno, std::generic_category()}
                     : isp_error::timed_execution_error;
  }
}

void configure_default_baudrate() {
  communication_engine::change_baudrate(4800);
}

void configure_baudrate(std::uint32_t baud) {
  communication_engine::change_baudrate(baud);
}

bool is_timeout_error(const std::error_code& ec) {
  return communication_engine::is_timeout_error(ec);
}

}  // namespace detail
}  // namespace isp