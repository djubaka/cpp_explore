#include "stm_arm_initial_baudrate_search.hpp"
#include <kv-communication/icommunication.hpp>
#include <kv-util/numeric.hpp>
#include <kv-util/predicate.hpp>
#include "device_io.hpp"
#include "kv-isp/error/error.hpp"
#include "stm_arm_max_baudrate_search.hpp"

namespace isp {

using namespace KvCommunication;

const std::array<std::uint32_t, 6> stm_initial_baudrate_search::POSSIBLE_BAUDS =
    {38400, 57600, 115200, 230400, 460800, 921600};

stm_initial_baudrate_search::stm_initial_baudrate_search(
    baudrate_search_context ctx)
    : ctx_{std::move(ctx)} {
  init_boot_echo_msg();
}

stm_initial_baudrate_search::stm_initial_baudrate_search(
    uint8_t baud_index, baudrate_search_context ctx)
    : current_ind_{baud_index}, ctx_{std::move(ctx)} {
  init_boot_echo_msg();
}

void stm_initial_baudrate_search::init_boot_echo_msg() {
  boot_echo_msg_[0] = 'B';
  boot_echo_msg_[1] = 'C';
  boot_echo_msg_[2] = 'E';
  auto random_values = numeric::random_bytes<std::uint8_t, 16>();
  std::copy(random_values.begin(), random_values.end(),
            boot_echo_msg_.begin() + 3);
  boot_echo_msg_[19] = '\n';
}

void stm_initial_baudrate_search::execute() {
  ctx_.io->configure_timeout(std::chrono::milliseconds{1000});
  find_baudrate();
}

void stm_initial_baudrate_search::find_baudrate() {
  test_baudrate(current_baud(),
                [self = shared_from_this()](const std::error_code &ec) {
                  self->on_test_baudrate(ec);
                });
}

void stm_initial_baudrate_search::test_baudrate(
    uint32_t baud, stm_initial_baudrate_search::callback_t callback) {
  detail::configure_baudrate(baud);

  auto on_res = [cb = std::move(callback), this](data_span data,
                                                 const std::error_code &ec) {
    if (ec) {
      cb(ec);
    } else {
      if (predicate::contains(data, boot_echo_msg_))
        cb(std::error_code{});
      else if (predicate::are_equal(data, "NOK\r\n")) {
        find_baudrate();  // try again with the same baudrate.
      } else
        cb(isp_error::boot_echo_failure);
    }
  };

  ctx_.io->async_upload(boot_echo_msg_, on_res, match<new_line>());
}

void stm_initial_baudrate_search::on_test_baudrate(const std::error_code &ec) {
  if (ec) {
    on_test_baudrate_error();
  } else {
    on_test_baudrate_success();
  }
}

void stm_initial_baudrate_search::on_test_baudrate_error() {
  if (has_next()) {
    // try the next one.
    move_to_next_baud();
    find_baudrate();
  } else {
    // we've tested all possible baudrate.
    ctx_.callback(isp_error::boot_baudrate_failure);
  }
}

void stm_initial_baudrate_search::on_test_baudrate_success() {
  // we've actually succeeded on this particular baudrate.
  if (has_next()) {
    // now try finding the maximum possible baud
    // since the current baud is allready tested, proceed with the next one.
    auto max_search =
        std::make_shared<stm_max_baudrate_search>(current_ind_ + 1, ctx_);
    max_search->execute();
  } else {
    // else it's done. declare victory.
    ctx_.callback(std::error_code{});
  }
}

bool stm_initial_baudrate_search::has_next() const {
  return current_ind_ < (POSSIBLE_BAUDS.size() - 1);
}

uint32_t stm_initial_baudrate_search::current_baud() const {
  return POSSIBLE_BAUDS[current_ind_];
}

uint32_t stm_initial_baudrate_search::move_to_next_baud() {
  if (has_next()) {
    current_ind_++;
  }

  return current_baud();
}

uint32_t stm_initial_baudrate_search::move_to_prev_baud() {
  if (current_ind_ != 0) {
    current_ind_--;
  }

  return current_baud();
}

uint8_t stm_initial_baudrate_search::current_as_ascii_char() const {
  return current_ind_ + 48;
}
}  // namespace isp