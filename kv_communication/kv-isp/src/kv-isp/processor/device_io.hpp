#pragma once

#include <chrono>
#include <system_error>

namespace isp {
namespace detail {

void wait_for(std::chrono::milliseconds interval, std::error_code& ec);
void configure_default_baudrate();
void configure_baudrate(unsigned int baud);
bool is_timeout_error(const std::error_code& ec);

}  // namespace detail
}  // namespace isp