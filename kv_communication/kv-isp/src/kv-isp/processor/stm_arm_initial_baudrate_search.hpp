#pragma once

#include <array>
#include <functional>
#include <memory>
#include <system_error>

namespace KvCommunication {
class ICommunication;
}

namespace isp {

struct baudrate_search_context {
  using callback_t = std::function<void(const std::error_code&)>;
  KvCommunication::ICommunication* io;
  callback_t callback;
};

class stm_initial_baudrate_search
    : public std::enable_shared_from_this<stm_initial_baudrate_search> {
  static const std::array<std::uint32_t, 6> POSSIBLE_BAUDS;

 public:
  virtual ~stm_initial_baudrate_search() = default;
  using callback_t = baudrate_search_context::callback_t;
  explicit stm_initial_baudrate_search(baudrate_search_context ctx);
  explicit stm_initial_baudrate_search(std::uint8_t baud_index,
                                       baudrate_search_context ctx);

  void execute();

  bool has_next() const;
  std::uint32_t current_baud() const;
  std::uint32_t move_to_next_baud();
  std::uint32_t move_to_prev_baud();
  std::uint8_t current_as_ascii_char() const;

 protected:
  void test_baudrate(std::uint32_t baud, callback_t callback);

 private:
  void init_boot_echo_msg();
  void on_test_baudrate(const std::error_code& ec);
  void on_test_baudrate_error();
  void on_test_baudrate_success();
  virtual void find_baudrate();

 private:
  std::size_t current_ind_{0};
  std::array<std::uint8_t, 20> boot_echo_msg_;

 protected:
  baudrate_search_context ctx_;
};
}  // namespace isp