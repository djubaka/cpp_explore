#include "stm_arm_creation.hpp"
#include <gsl/span_ext>
#include <kv-util/numeric.hpp>
#include "kv-isp/error/error.hpp"
#include "stm_arm.hpp"

namespace isp {
stm_arm_creation::stm_arm_creation() {
  std::fill(cpu_data_buff_.begin(), cpu_data_buff_.end(), 0xFF);
  std::fill(cpu_page_buff_.begin(), cpu_page_buff_.end(), 0x00);
}

processor stm_arm_creation::create(
    stm_arm_creation::hex_lines_t command_lines) const {
  auto line_infos = as_line_infos(command_lines);
  populate_cpu_info(line_infos);

  return stm_arm{{cpu_data_buff_.begin(), cpu_data_buff_.end()},
                 {cpu_page_buff_.begin(), cpu_page_buff_.end()}};
}

std::vector<intel_line_info> stm_arm_creation::as_line_infos(
    stm_arm_creation::hex_lines_t command_lines) {
  std::vector<intel_line_info> line_infos{};
  line_infos.reserve(static_cast<std::size_t>(command_lines.size()));

  for (const auto& hl : command_lines) {
    line_infos.push_back(parse_hex_line(hl));
  }

  return line_infos;
}

intel_line_info stm_arm_creation::parse_hex_line(std::string_view hex_line) {
  // the line looks approximately like => :020000040801F1
  // skip the double colon

  auto hex_in_ascii_bytes =
      numeric::as_bytes(hex_line.data() + 1, hex_line.length() - 1);

  auto numeric_values = numeric::hex_to_numeric(hex_in_ascii_bytes);

  return recreate_line_info(numeric_values);
}

intel_line_info stm_arm_creation::recreate_line_info(
    gsl::span<const uint8_t> line_bytes) {
  std::uint8_t data_size = line_bytes[0];
  std::uint32_t addr_field = line_bytes[1] * 256 + line_bytes[2];
  auto record_type = static_cast<intel_record_type>(line_bytes[3]);

  intel_line_info line_info{};
  line_info.data_size = data_size;
  line_info.record_type = record_type;

  switch (record_type) {
    case intel_record_type::data:
      throw_on_checksum_failure(line_bytes, intel_record_type::data);
      line_info.data_address = addr_field;
      line_info.data.reserve(data_size);
      std::copy(line_bytes.begin() + 4, line_bytes.begin() + 4 + data_size,
                std::back_inserter(line_info.data));
      break;
    case intel_record_type::end_of_file:
      throw_on_checksum_failure(line_bytes, intel_record_type::end_of_file);
      break;
    case intel_record_type::ext_seg_addr:
      line_info.data_address = calculate_address(line_bytes[4], line_bytes[5]);
      break;
    case intel_record_type::start_seg_addr:
      break;
    case intel_record_type::ext_lin_addr:
      line_info.data_address = calculate_address(line_bytes[4], line_bytes[5]);
      break;
    case intel_record_type::start_lin_addr:
      line_info.data_address = calculate_address(line_bytes[4], line_bytes[5]);
      break;
  }

  return line_info;
}

void stm_arm_creation::throw_on_checksum_failure(
    gsl::span<const uint8_t> line_bytes, intel_record_type rec_type) {
  if (rec_type == intel_record_type::data) {
    auto data_size = line_bytes[0];
    auto expected = line_bytes[4 + data_size];
    auto actual = numeric::checksum(
        gsl::make_span(line_bytes.data(), line_bytes.data() + 4 + data_size));

    if (expected != actual) {
      throw checksum_exception{expected, actual};
    }
  } else if (rec_type == intel_record_type::end_of_file) {
    auto expected = line_bytes[4];
    auto actual = numeric::checksum(
        gsl::make_span(line_bytes.data(), line_bytes.data() + 4));

    if (expected != actual) {
      throw checksum_exception{expected, actual};
    }
  }
}

uint32_t stm_arm_creation::calculate_address(uint8_t high, uint8_t low) {
  std::uint32_t addr = high * 256 + low;
  addr <<= 16;
  return addr;
}

void stm_arm_creation::populate_cpu_info(
    gsl::span<const intel_line_info> line_infos) const {
  std::uint32_t seg_addr = 0;
  std::uint32_t start_lin_addr = 0;

  for (const auto& li : line_infos) {
    switch (li.record_type) {
      case intel_record_type::data:
        populate_cpu_flash_data(seg_addr, start_lin_addr, li);
        break;
      case intel_record_type::end_of_file:
        populate_cpu_page_data();
        break;
      case intel_record_type::ext_seg_addr:
        seg_addr = li.data_address;
        if ((start_lin_addr & 0x8000000) != 0) start_lin_addr -= 0x8000000;
        break;
      case intel_record_type::start_seg_addr:
        break;
      case intel_record_type::ext_lin_addr:
        start_lin_addr = li.data_address;
        if ((start_lin_addr & 0x8000000) != 0) start_lin_addr -= 0x8000000;
        break;
      case intel_record_type::start_lin_addr:
        start_lin_addr = li.data_address;
        if ((start_lin_addr & 0x8000000) != 0) start_lin_addr -= 0x8000000;
        break;
    }
  }
}

void stm_arm_creation::populate_cpu_flash_data(
    uint32_t seg_addr, uint32_t start_lin_addr,
    const intel_line_info& li) const {
  auto flash_offset = seg_addr + start_lin_addr + li.data_address;
  if (flash_offset >= CPU_FLASH_SIZE) return;

  for (std::uint32_t i = 0; i < li.data_size; ++i) {
    auto data_offset = i + flash_offset;
    if (data_offset >= CPU_FLASH_SIZE) break;

    cpu_data_buff_[data_offset] = li.data[i];
  }
}

void stm_arm_creation::populate_cpu_page_data() const {
  for (std::uint32_t i = 0; i < CPU_FLASH_SIZE; i += PAGE_SIZE) {
    cpu_page_buff_[i / PAGE_SIZE] = 0;
    bool reevaluate = false;

    for (std::uint32_t j = 0; j < PAGE_SIZE; ++j) {
      if (cpu_data_buff_[i + j] != 0xFF) {
        reevaluate = true;
      }
    }

    if (reevaluate) cpu_page_buff_[i / PAGE_SIZE] = PAGE_SIZE;
  }
}
}  // namespace isp