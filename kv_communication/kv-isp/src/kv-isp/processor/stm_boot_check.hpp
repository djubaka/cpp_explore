#pragma once

#include <array>
#include <functional>
#include <memory>
#include <system_error>

namespace KvCommunication {
class ICommunication;
}

namespace isp {

class stm_boot_check : public std::enable_shared_from_this<stm_boot_check> {
  using user_cb = std::function<void(const std::error_code& ec)>;

 public:
  static std::shared_ptr<stm_boot_check> create(
      KvCommunication::ICommunication* io, user_cb cb);
  void execute();

 private:
  stm_boot_check(KvCommunication::ICommunication* io, user_cb cb);
  void try_again();

  KvCommunication::ICommunication* io_;
  user_cb cb_;
  int retry_count_{0};

  static const std::array<std::uint8_t, 3> BOOT_CHECK;
};
}  // namespace isp