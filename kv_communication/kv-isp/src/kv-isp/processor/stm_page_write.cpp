#include "stm_page_write.hpp"
#include <kv-communication/icommunication.hpp>
#include <kv-util/numeric.hpp>
#include <kv-util/predicate.hpp>
#include "device_io.hpp"
#include "kv-isp/error/error.hpp"

namespace isp {

using namespace KvCommunication;

stm_page_write::stm_page_write(page_write_context ctx) : ctx_{std::move(ctx)} {}

void stm_page_write::execute() {
  ctx_.io->configure_timeout(std::chrono::milliseconds{5000});
  write_current_page();
}

void stm_page_write::write_current_page() {
  if (is_page_to_be_written(current_page_no_)) {
    do_write();
  } else {
    // mimic page write success.
    on_page_written();
  }
}

void stm_page_write::do_write() {
  current_page_ = prepare_page_bytes();
  ctx_.io->async_upload(
      current_page_,
      [self = shared_from_this()](data_view_t data, const std::error_code& ec) {
        self->on_page_response(data, ec);
      },
      match<new_line>());
}

bool stm_page_write::is_page_to_be_written(int page_no) const {
  return ctx_.page_data[page_no] != 0;
}

stm_page_write::data_t stm_page_write::prepare_page_bytes() const {
  auto page_write = page_write_bytes();
  auto page_size = page_size_info_bytes(current_page_no_);
  auto page_address = page_address_info_bytes(current_page_no_);
  auto page_data = page_data_bytes(current_page_no_);
  auto cks = checksum_bytes(current_page_no_);

  data_t page;
  page.reserve(page_write.size() + page_size.size() + page_address.size() +
               page_data.size() + cks.size() + 1);

  page.insert(page.end(), page_write.begin(), page_write.end());
  page.insert(page.end(), page_size.begin(), page_size.end());
  page.insert(page.end(), page_address.begin(), page_address.end());
  page.insert(page.end(), page_data.begin(), page_data.end());
  page.insert(page.end(), cks.begin(), cks.end());
  page.push_back('\n');

  return page;
}

stm_page_write::data_t stm_page_write::page_write_bytes() const {
  return {'P', 'W', 'N'};
}

stm_page_write::data_t stm_page_write::page_size_info_bytes(int page_no) const {
  // add size info, 8 bytes for the address, 2 * size for data, 2 for the
  // checksum

  auto size = ctx_.page_data[page_no];
  auto size_info = 8 + 2 * size + 2;
  return numeric::as_hex_string(size_info, 4);
}

stm_page_write::data_t stm_page_write::page_address_info_bytes(
    int page_no) const {
  auto addr = page_no * PAGE_SIZE;
  return numeric::as_hex_string(addr, 8);
}

stm_page_write::data_t stm_page_write::page_data_bytes(int page_no) const {
  auto size = ctx_.page_data[page_no];
  auto addr = page_no * PAGE_SIZE;

  data_t page_data;
  page_data.reserve(static_cast<std::size_t>(2 * size));

  for (int i = 0; i < size; ++i) {
    auto d = numeric::as_hex_string(ctx_.flash_data[addr + i], 2);
    page_data.insert(page_data.end(), d.begin(), d.end());
  }

  return page_data;
}

stm_page_write::data_t stm_page_write::checksum_bytes(int page_no) const {
  auto size = ctx_.page_data[page_no];
  auto addr = page_no * PAGE_SIZE;

  data_t d;
  d.reserve(static_cast<std::size_t>(size + 4));

  d.push_back((addr >> 24) & 0x0ff);
  d.push_back((addr >> 16) & 0x0ff);
  d.push_back((addr >> 8) & 0x0ff);
  d.push_back((addr >> 0) & 0x0ff);

  auto beg = ctx_.flash_data.begin() + addr;
  auto end = beg + size;
  d.insert(d.end(), beg, end);

  auto cks = numeric::checksum(d);
  return numeric::as_hex_string(cks, 2);
}

void stm_page_write::on_page_response(data_view_t data,
                                      const std::error_code& ec) {
  if (detail::is_timeout_error(ec)) {
    ctx_.stage(isp_error::page_write_timedout);
  } else if (ec) {
    ctx_.stage(isp_error::page_write_failure);
  } else {
    if (page_response_valid(data))
      on_page_written();
    else
      ctx_.stage(isp_error::page_write_failure);
  }
}

bool stm_page_write::page_response_valid(
    stm_page_write::data_view_t data) const {
  return predicate::are_equal(data, "PWNOK\r\n");
}

void stm_page_write::on_page_written() {
  invoke_progress();
  current_page_no_++;
  if (current_page_no_ < ctx_.page_count) {
    // move on to the next page.
    write_current_page();
  } else {
    // success. all pages written. mark end of programming stage.
    ctx_.stage(std::error_code{});
  }
}

void stm_page_write::invoke_progress() {
  const auto perc = numeric::decimal_round(
      static_cast<double>(current_page_no_) / ctx_.page_count * 100, 2);

  const auto prog = static_cast<int>(perc);
  ctx_.progress(prog);
}
}  // namespace isp