#pragma once

#include <functional>
#include <gsl/span>
#include <string>
#include <system_error>
#include <vector>

namespace KvCommunication {
class ICommunication;
}

namespace isp {

// implements processor concept
class stm_arm {
 public:
  using data_span = gsl::span<const std::uint8_t>;
  using page_write_callback =
      std::function<void(data_span data, const std::error_code& ec)>;
  using stage_callback_t = std::function<void(const std::error_code& ec)>;
  using progress_callback_t = std::function<void(int)>;
  using data_t = std::vector<std::uint8_t>;
  using page_t = std::vector<int>;

  explicit stm_arm(data_t data, page_t page_data);

  void init(KvCommunication::ICommunication* io,
            stage_callback_t callback) const;

  void memory_erase(KvCommunication::ICommunication* io,
                    stage_callback_t callback) const;

  void program(KvCommunication::ICommunication* io, stage_callback_t stage,
               progress_callback_t progress) const;

  void verify(KvCommunication::ICommunication* io, stage_callback_t stage,
              progress_callback_t progress) const;

 private:
  std::error_code verify(data_span response) const;
  std::uint16_t crc_validation_value() const;
  static std::uint16_t response_to_value(data_span response);

 private:
  data_t flash_data_;
  page_t page_data_;

 private:
  static const std::array<std::uint8_t, 3> BOOT_INFO;
  static const std::array<std::uint8_t, 3> BOOT_ERASE;
  static const std::array<std::uint8_t, 3> BOOT_CRC_CODE;

  static const std::size_t CPU_FLASH_SIZE = 512 * 1024;
  static const std::size_t CPU_FLASH_START_APP = 0x10000;
  static const std::size_t CPU_FLASH_APP_SIZE = 0x70000;
};

}  // namespace isp