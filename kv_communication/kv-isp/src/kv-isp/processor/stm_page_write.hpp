#pragma once

#include <functional>
#include <gsl/span>
#include <memory>

namespace KvCommunication {
class ICommunication;
}

namespace isp {
struct page_write_context {
  int page_count;
  KvCommunication::ICommunication* io;
  gsl::span<const std::uint8_t> flash_data;
  gsl::span<const int> page_data;
  std::function<void(const std::error_code&)> stage;
  std::function<void(int)> progress;
};

class stm_page_write : public std::enable_shared_from_this<stm_page_write> {
  using data_t = std::vector<std::uint8_t>;
  using data_view_t = gsl::span<const std::uint8_t>;

 public:
  static const std::int32_t PAGE_SIZE = 2048;
  explicit stm_page_write(page_write_context ctx);

  void execute();

 private:
  void write_current_page();
  void do_write();
  bool is_page_to_be_written(int page_no) const;
  data_t prepare_page_bytes() const;
  data_t page_write_bytes() const;
  data_t page_size_info_bytes(int page_no) const;
  data_t page_address_info_bytes(int page_no) const;
  data_t page_data_bytes(int page_no) const;
  data_t checksum_bytes(int page_no) const;
  void on_page_response(data_view_t data, const std::error_code& ec);
  bool page_response_valid(data_view_t data) const;
  void on_page_written();
  void invoke_progress();

 private:
  int current_page_no_{0};
  data_t current_page_;
  page_write_context ctx_;
};
}  // namespace isp