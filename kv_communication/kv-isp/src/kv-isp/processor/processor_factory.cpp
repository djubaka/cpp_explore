#include "kv-isp/processor/processor_factory.hpp"
#include "kv-isp/processor/stm_arm_creation.hpp"
#include "kv-isp/processor/stm_arm_preparation.hpp"

namespace isp {

processor_creation arm_builder() { return stm_arm_creation{}; }
processor_preparation arm_preparation() { return stm_arm_preparation{}; }

}  // namespace isp