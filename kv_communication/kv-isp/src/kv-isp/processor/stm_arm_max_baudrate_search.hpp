#pragma once

#include "stm_arm_initial_baudrate_search.hpp"

namespace isp {
class stm_max_baudrate_search : public stm_initial_baudrate_search {
 public:
  explicit stm_max_baudrate_search(std::uint8_t baud_index,
                                   baudrate_search_context ctx);

  // stm_initial_baudrate_search interface
 private:
  void find_baudrate() override;

 private:
  void configure_temp_baud();
  void on_temp_baud_config();
  void configure_permanent_baud();
  void on_permanent_baud_config();
  void configure_baud(std::uint8_t baud_index, std::uint8_t baud_mode,
                      callback_t callback);
  void revert_to_previous_baud();

 private:
  std::array<std::uint8_t, 6> baud_cfg_cmd_;
};
}  // namespace isp