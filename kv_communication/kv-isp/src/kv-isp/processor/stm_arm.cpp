#include "stm_arm.hpp"
#include <gsl/span_ext>
#include <kv-communication/icommunication.hpp>
#include <kv-util/numeric.hpp>
#include <kv-util/predicate.hpp>
#include <sstream>
#include "device_io.hpp"
#include "kv-isp/error/error.hpp"
#include "stm_page_write.hpp"

namespace isp {

using namespace KvCommunication;
using namespace std::chrono;

const std::array<std::uint8_t, 3> stm_arm::BOOT_INFO = {'B', 'I', '\n'};
const std::array<std::uint8_t, 3> stm_arm::BOOT_ERASE = {'D', 'E', '\n'};
const std::array<std::uint8_t, 3> stm_arm::BOOT_CRC_CODE = {'D', 'C', '\n'};

stm_arm::stm_arm(stm_arm::data_t flash_data, stm_arm::page_t page_data)
    : flash_data_{std::move(flash_data)}, page_data_{std::move(page_data)} {}

void stm_arm::init(KvCommunication::ICommunication* io,
                   stm_arm::stage_callback_t callback) const {
  auto on_init_response = [cb = std::move(callback)](
                              data_span data, const std::error_code& ec) {
    if (detail::is_timeout_error(ec)) {
      cb(isp_error::init_timedout);
    } else if (ec) {
      cb(isp_error::init_failure);
    } else {
      if (predicate::contains(data, "BI"))
        cb(std::error_code{});
      else
        cb(isp_error::init_failure);
    }
  };

  io->configure_timeout(milliseconds{1000});
  io->async_upload(BOOT_INFO, std::move(on_init_response), match<new_line>());
}

void stm_arm::memory_erase(KvCommunication::ICommunication* io,
                           stm_arm::stage_callback_t callback) const {
  auto on_mem_response = [cb = std::move(callback)](data_span data,
                                                    const std::error_code& ec) {
    if (detail::is_timeout_error(ec)) {
      cb(isp_error::memory_erase_timedout);
    } else if (ec) {
      cb(isp_error::memory_erase_failure);
    } else {
      if (predicate::are_equal(data, "DEOK\r\n")) {
        cb(std::error_code{});
      } else {
        cb(isp_error::memory_erase_failure);
      }
    }
  };

  io->configure_timeout(milliseconds{5000});
  io->async_upload(BOOT_ERASE, std::move(on_mem_response), match<new_line>());
}

void stm_arm::program(KvCommunication::ICommunication* io,
                      stm_arm::stage_callback_t stage,
                      stm_arm::progress_callback_t progress) const {
  const auto pages = CPU_FLASH_SIZE / stm_page_write::PAGE_SIZE;
  page_write_context ctx = {pages,
                            io,
                            flash_data_,
                            page_data_,
                            std::move(stage),
                            std::move(progress)};

  auto page_write = std::make_shared<stm_page_write>(std::move(ctx));
  page_write->execute();
}

void stm_arm::verify(KvCommunication::ICommunication* io,
                     stm_arm::stage_callback_t stage,
                     stm_arm::progress_callback_t progress) const {
  auto on_ver_response = [this, stage_ = std::move(stage),
                          progress_ = std::move(progress)](
                             data_span data, const std::error_code& ec) {
    if (detail::is_timeout_error(ec)) {
      stage_(isp_error::crc_timedout);
    } else if (ec) {
      stage_(isp_error::crc_verification_failure);
    } else {
      auto ver_ec = verify(data);
      progress_(100);
      stage_(ver_ec);
    }
  };

  io->configure_timeout(milliseconds{3000});
  io->async_upload(BOOT_CRC_CODE, on_ver_response, match<new_line>());
}

std::error_code stm_arm::verify(data_span response) const {
  if (predicate::contains(response, "DC:")) {
    auto crc = crc_validation_value();
    auto ver = response_to_value(response);
    if (crc == ver) return std::error_code{};
  }

  return isp_error::crc_verification_failure;
}

uint16_t stm_arm::crc_validation_value() const {
  auto beg = flash_data_.data() + CPU_FLASH_START_APP;
  auto end = beg + CPU_FLASH_APP_SIZE;
  return numeric::crc16(gsl::make_span<const std::uint8_t>(beg, end));
}

uint16_t stm_arm::response_to_value(data_span response) {
  std::stringstream sstr;
  sstr << std::hex;

  auto beg = response.begin() + 3;
  auto end = beg + 4;
  std::copy(beg, end, std::ostream_iterator<std::uint8_t>{sstr});

  std::uint16_t res{0};
  sstr >> res;
  return res;
}

}  // namespace isp