#pragma once

#include <functional>
#include <memory>
#include <system_error>

namespace KvCommunication {
class ICommunication;
}

namespace isp {
class processor {
  using stage_callback_t = std::function<void(const std::error_code& ec)>;
  using progress_callback_t = std::function<void(int)>;

 public:
  template <typename T>
  processor(T x) : self_{std::make_shared<processor_model<T>>(std::move(x))} {}

  void init(KvCommunication::ICommunication* io,
            stage_callback_t callback) const {
    self_->init_(io, std::move(callback));
  }

  void memory_erase(KvCommunication::ICommunication* io,
                    stage_callback_t callback) const {
    self_->memory_erase_(io, std::move(callback));
  }

  void program(KvCommunication::ICommunication* io,
               stage_callback_t stage_callback,
               progress_callback_t progress_callback) const {
    self_->program_(io, std::move(stage_callback),
                    std::move(progress_callback));
  }

  void verify(KvCommunication::ICommunication* io,
              stage_callback_t stage_callback,
              progress_callback_t progress_callback) const {
    self_->verify_(io, std::move(stage_callback), std::move(progress_callback));
  }

 private:
  struct processor_concept {
    virtual ~processor_concept() = default;

    virtual void init_(KvCommunication::ICommunication* io,
                       stage_callback_t) const = 0;

    virtual void memory_erase_(KvCommunication::ICommunication* io,
                               stage_callback_t) const = 0;

    virtual void program_(KvCommunication::ICommunication* io, stage_callback_t,
                          progress_callback_t) const = 0;

    virtual void verify_(KvCommunication::ICommunication* io, stage_callback_t,
                         progress_callback_t) const = 0;
  };

  template <typename T>
  struct processor_model final : processor_concept {
    processor_model(T x) : proc{std::move(x)} {}

    void init_(KvCommunication::ICommunication* io,
               stage_callback_t callback) const override {
      proc.init(io, std::move(callback));
    }

    void memory_erase_(KvCommunication::ICommunication* io,
                       stage_callback_t callback) const override {
      proc.memory_erase(io, std::move(callback));
    }

    void program_(KvCommunication::ICommunication* io, stage_callback_t stage,
                  progress_callback_t progress) const override {
      proc.program(io, std::move(stage), std::move(progress));
    }

    void verify_(KvCommunication::ICommunication* io, stage_callback_t stage,
                 progress_callback_t progress) const override {
      proc.verify(io, std::move(stage), std::move(progress));
    }

    T proc;
  };

  std::shared_ptr<const processor_concept> self_;
};
}  // namespace isp