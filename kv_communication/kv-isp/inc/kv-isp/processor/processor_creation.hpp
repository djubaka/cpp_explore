#pragma once

#include <gsl/span>
#include <memory>
#include <string>
#include "processor.hpp"

namespace isp {
class processor_creation {
  using hex_lines_t = gsl::span<const std::string>;

 public:
  template <typename T>
  processor_creation(T x)
      : self_{std::make_shared<processor_creation_model<T>>(std::move(x))} {}

  processor create(hex_lines_t command_lines) const {
    return self_->create_(command_lines);
  }

 private:
  struct processor_creation_concept {
    virtual ~processor_creation_concept() = default;
    virtual processor create_(hex_lines_t command_lines) const = 0;
  };

  template <typename T>
  struct processor_creation_model final : processor_creation_concept {
   public:
    processor_creation_model(T x) : creator{std::move(x)} {}
    processor create_(hex_lines_t command_lines) const override {
      return creator.create(command_lines);
    }

    T creator;
  };

  std::shared_ptr<const processor_creation_concept> self_;
};
}  // namespace isp