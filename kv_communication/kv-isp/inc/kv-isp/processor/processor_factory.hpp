#pragma once

#include <string_view>
#include "processor_creation.hpp"
#include "processor_preparation.hpp"

namespace isp {
processor_creation arm_builder();
processor_preparation arm_preparation();
}  // namespace isp