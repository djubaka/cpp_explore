#pragma once

#include <functional>
#include <memory>
#include <system_error>

namespace KvCommunication {
class ICommunication;
}

namespace isp {

class processor_preparation {
 public:
  using callback_t = std::function<void(const std::error_code& ec)>;

  template <typename T>
  processor_preparation(T x)
      : self_{std::make_shared<processor_preparation_model<T>>(std::move(x))} {}

  void send_to_boot_async(KvCommunication::ICommunication* io,
                          callback_t callback) const {
    self_->send_to_boot_async_(io, std::move(callback));
  }

  void configure_io_in_boot(KvCommunication::ICommunication* io,
                            callback_t callback) const {
    self_->configure_io_in_boot_(io, std::move(callback));
  }

  void leave_from_boot_async(KvCommunication::ICommunication* io,
                             callback_t callback) const {
    self_->leave_from_boot_async_(io, std::move(callback));
  }

  void check_if_in_boot_async(KvCommunication::ICommunication* io,
                              callback_t callback) const {
    return self_->check_if_in_boot_async_(io, std::move(callback));
  }

 private:
  struct processor_preparation_concept {
    virtual ~processor_preparation_concept() = default;
    virtual void send_to_boot_async_(KvCommunication::ICommunication* io,
                                     callback_t callback) const = 0;

    virtual void configure_io_in_boot_(KvCommunication::ICommunication* io,
                                       callback_t callback) const = 0;

    virtual void leave_from_boot_async_(KvCommunication::ICommunication* io,
                                        callback_t callback) const = 0;

    virtual void check_if_in_boot_async_(KvCommunication::ICommunication* io,
                                         callback_t callback) const = 0;
  };

  template <typename T>
  struct processor_preparation_model final : processor_preparation_concept {
    processor_preparation_model(T x) : prep{std::move(x)} {}

    void send_to_boot_async_(KvCommunication::ICommunication* io,
                             callback_t callback) const override {
      prep.send_to_boot_async(io, std::move(callback));
    }

    void configure_io_in_boot_(KvCommunication::ICommunication* io,
                               callback_t callback) const override {
      prep.configure_io_in_boot(io, std::move(callback));
    }

    void leave_from_boot_async_(KvCommunication::ICommunication* io,
                                callback_t callback) const override {
      prep.leave_from_boot_async(io, std::move(callback));
    }

    void check_if_in_boot_async_(KvCommunication::ICommunication* io,
                                 callback_t callback) const override {
      prep.check_if_in_boot_async(io, std::move(callback));
    }

    T prep;
  };

  std::shared_ptr<const processor_preparation_concept> self_;
};
}  // namespace isp