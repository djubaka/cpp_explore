#pragma once

#include <functional>
#include <optional>
#include <sigslot/signal.hpp>
#include <string>
#include <system_error>
#include <thread>
#include <vector>
#include "kv-isp/processor/processor.hpp"
#include "kv-isp/processor/processor_creation.hpp"
#include "kv-isp/processor/processor_preparation.hpp"

namespace KvCommunication {
class ICommunication;
}

namespace isp {
class firmware_upgrade {
  using firmware_read_t = std::function<std::vector<std::string>(
      const std::string&, std::error_code& ec)>;

 public:
  explicit firmware_upgrade(KvCommunication::ICommunication* io,
                            processor_preparation prep,
                            processor_creation creation);

  ~firmware_upgrade();

  void execute(const std::string& fw_file_path);
  bool is_hdc_mismatch(const std::error_code& ec) const;
  bool is_dvfc_corrupted(const std::error_code& ec) const;

  sigslot::signal<> reading_file_begin;
  sigslot::signal<const std::error_code&> reading_file_end;

  sigslot::signal<> entering_boot_begin;
  sigslot::signal<const std::error_code&> entering_boot_end;

  sigslot::signal<> configuring_boot_begin;
  sigslot::signal<const std::error_code&> configuring_boot_end;

  sigslot::signal<> processor_init_begin;
  sigslot::signal<const std::error_code&> processor_init_end;

  sigslot::signal<> erasing_memory_begin;
  sigslot::signal<const std::error_code&> erasing_memory_end;

  sigslot::signal<> programming_memory_begin;
  sigslot::signal<int> programming_memory_progress;
  sigslot::signal<const std::error_code&> programming_memory_end;

  sigslot::signal<> verifying_memory_begin;
  sigslot::signal<int> verifying_memory_progress;
  sigslot::signal<const std::error_code&> verifying_memory_end;

  sigslot::signal<> leaving_boot_begin;
  sigslot::signal<const std::error_code&> leaving_boot_end;

 private:
  static std::vector<std::string> from_hex_file(const std::string& path,
                                                std::error_code& ec);
  static std::vector<std::string> from_dvfc_file(const std::string& path,
                                                 std::error_code& ec);

  void read_firmware(const std::string& path, firmware_read_t read_delegate);
  void on_hex_lines_read(std::vector<std::string> hex_lines);
  void send_to_boot();
  void configure_boot();
  void check_if_in_boot();
  void proc_init();
  void erase_memory();
  void program_memory();
  void verify();
  void leave_boot();

 private:
  KvCommunication::ICommunication* io_{nullptr};
  processor_preparation prep_;
  processor_creation creation_;
  std::optional<processor> proc_;
  std::optional<std::thread> reading_thread_;
};
}  // namespace isp