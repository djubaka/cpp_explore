#pragma once

#include <exception>
#include <sstream>
#include <system_error>

namespace isp {

class checksum_exception : public std::exception {
  // exception interface
 public:
  explicit checksum_exception(int expected, int actual) {
    std::stringstream sstr;
    sstr << "checksum wrong.";
    sstr << " expected: " << expected << ".";
    sstr << " actual: " << actual << ".";
    what_ = sstr.str();
  }

  const char *what() const noexcept { return what_.c_str(); }

 private:
  std::string what_;
};

enum class isp_error {
  init_timedout = 1,
  init_failure,
  memory_erase_timedout,
  memory_erase_failure,
  page_write_timedout,
  page_write_failure,
  crc_timedout,
  crc_verification_failure,
  send_to_boot_failure,
  not_in_boot_mode,
  leave_from_boot_failure,
  boot_echo_failure,
  boot_baudrate_failure,
  dvfc_read_failure,
  dvfc_checksum_error,
  dvfc_identity_record_error,
  dvfc_hdc_mismatch,
  timed_execution_error,
};

std::error_code make_error_code(isp::isp_error);

}  // namespace isp

namespace std {
template <>
struct is_error_code_enum<isp::isp_error> : public std::true_type {};
}  // namespace std