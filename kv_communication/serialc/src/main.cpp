#include <CLI/CLI.hpp>
#include <iostream>
#include <system_error>
#include "config.hpp"
#include "exec/execution.hpp"

std::optional<exec::exec_t> exe{};

static void select_sub(CLI::App& app, config& cfg) {
  auto select = app.add_subcommand("select", "Select device channel");
  std::set<config::channel_t> chs = {
      config::channel_t::control,  config::channel_t::measurement,
      config::channel_t::printer,  config::channel_t::module_1,
      config::channel_t::module_2, config::channel_t::module_3};

  std::string ch_descr =
      "control=0, measurement=1, printer=2, "
      "module_1=3, module_2=4, module_3=5";

  select->add_set("--channel", cfg.channel, std::move(chs), std::move(ch_descr))
      ->required();
  select->callback([&] { exe = exec::channel_select(cfg); });
}

static void command_sub(CLI::App& app, config& cfg) {
  auto exec = app.add_subcommand("exec", "Execute command");
  exec->add_option("-c,--cmd", cfg.cmds, "Commands to execute")->required();
  exec->callback([&] { exe = exec::commander(cfg); });
}

static void file_sub(CLI::App& app, config& cfg) {
  auto upload = app.add_subcommand("upload", "Send file");
  upload->add_option("-f,--file", cfg.file, "Path to file")->required();
  upload->callback([&] { exe = exec::file_sending(cfg); });
}

static void printer_file_sub(CLI::App& app, config& cfg) {
  auto upload =
      app.add_subcommand("upload-printer-image", "Send image to printer");
  upload->add_option("-f, --file", cfg.file, "Path to file")->required();
  upload->callback([&] { exe = exec::print_image(cfg); });
}

static void fw_sub(CLI::App& app, config& cfg) {
  auto fw_upgrade = app.add_subcommand("fw-update", "Update firmware");
  fw_upgrade->add_option("-f,--file", cfg.file, "Path to file")->required();
  fw_upgrade->callback([&] { exe = exec::firmware_update(cfg); });
}

static void collection_sub(CLI::App& app, config& cfg) {
  auto collect =
      app.add_subcommand("collect-samples", "Download measurement samples");
  collect
      ->add_option("-c,--count", cfg.sample_count,
                   "Number of samples to download")
      ->required();

  collect->callback([&] { exe = exec::collect_samples(cfg); });
}

static void gpio_export_sub(CLI::App& app, config& cfg) {
  std::set<config::pin_t> modes = {config::pin_t::input, config::pin_t::output};
  std::string mode_descr = "input = 0, output = 1";

  auto exp = app.add_subcommand("export-pin", "Export gpio pin to kernel");
  exp->add_option("--sodimm", cfg.pin_number, "sodimm number");
  exp->add_set("--mode", cfg.pin_mode, std::move(modes), std::move(mode_descr))
      ->required();
  exp->callback([&] { exe = exec::pin_export(cfg); });
}

static void gpio_lookup_sub(CLI::App& app, config& cfg) {
  auto lookup = app.add_subcommand("lookup-pin", "Lookup gpio level");
  lookup->add_option("--sodimm", cfg.pin_number, "sodimm number");
  lookup->callback([&] { exe = exec::pin_lookup(cfg); });
}

int main(int argc, char** argv) {
  config cfg{};

  CLI::App app{"A serial port client"};
  app.require_subcommand(1);
  app.add_option("-p,--port", cfg.port, "Serial port name", true);
  app.add_option("-b,--baud", cfg.baudrate, "Serial port baudrate", true);

  for (auto& add_sub :
       {select_sub, command_sub, file_sub, printer_file_sub, fw_sub,
        collection_sub, gpio_export_sub, gpio_lookup_sub}) {
    add_sub(app, cfg);
  }

  CLI11_PARSE(app, argc, argv);

  std::error_code ec{};
  exe->execute(ec);

  std::cin.get();
  std::cout << "status: " << ec.message() << std::endl;
}
