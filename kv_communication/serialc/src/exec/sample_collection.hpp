#pragma once

#include <cstdint>
#include <gsl/span>
#include <memory>
#include <sigslot/signal.hpp>
#include <system_error>
#include "collection_state.hpp"

namespace KvCommunication {
class dispatcher;
}

class package_visitor;
class sample_collection
    : public std::enable_shared_from_this<sample_collection> {
 public:
  friend class package_visitor;
  using data_t = gsl::span<const std::uint8_t>;
  using dispatch_t = KvCommunication::dispatcher*;

  static std::shared_ptr<sample_collection> create(std::size_t sample_count,
                                                   dispatch_t dispatch);
  void execute(std::error_code& ec);

  sigslot::signal<const std::error_code&> on_error;
  sigslot::signal<std::uint32_t, gsl::span<const std::uint8_t>> on_sane_package;
  sigslot::signal<std::uint32_t> on_corrupted_package;
  sigslot::signal<> on_collection_finished;

 private:
  sample_collection(std::size_t expected, dispatch_t disp)
      : sample_count_{expected}, dispatch_{disp} {}

  void collect();
  void recollect();
  void start_collecting(std::uint8_t collection_byte);
  void on_package_arrival(data_t data);

 private:
  std::size_t sample_count_;
  dispatch_t dispatch_;
  collection_state state_;
};

struct sane_package;
struct corrupted_package;
struct end_package;
struct package_visitor {
  package_visitor(sample_collection* sc) : sc_{sc} {}
  void operator()(const sane_package& sp) const;
  void operator()(const corrupted_package& cp) const;
  void operator()(const end_package& e) const;
  sample_collection* sc_;
};