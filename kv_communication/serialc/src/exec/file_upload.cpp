#include "file_upload.hpp"
#include <fmt/core.h>
#include <fmt/printf.h>
#include <fmt/ranges.h>
#include <cstring>
#include <fstream>
#include <gsl/span_ext>
#include <kv-communication/communication_engine.hpp>
#include <kv-communication/icommunication.hpp>
#include <kv-gpio/device_selection.hpp>
#include <kv-util/numeric.hpp>
#include <kv-util/string.hpp>

void file_upload::execute(std::error_code &ec) {
  if (ec) return;

  std::ifstream in{path_};
  std::array<char, 2048> buffer;
  do {
    in.read(buffer.data(), buffer.size());
    auto bytes_read = in.gcount();
    auto as_byte = reinterpret_cast<const std::uint8_t *>(buffer.data());
    auto as_span = gsl::make_span(as_byte, bytes_read);
    comm_->write(as_span, ec);
  } while (!ec && in);
}

void printer_file_upload::execute(std::error_code &ec) {
  if (ec) {
    return;
  }

  std::ifstream in{path_};
  std::string line;
  std::vector<std::vector<unsigned int>> rgbs;
  while ((not ec) && std::getline(in, line)) {
    const auto line_values = string_helper::split(line, ",");
    std::vector<unsigned int> row;
    row.reserve(line_values.size());
    for (auto l : line_values) {
      if (l.find_first_not_of(' ') != std::string::npos) {
        row.push_back(std::stoul(l));
      }
    }

    rgbs.push_back(std::move(row));
  }

  print(rgbs, ec);
}

void printer_file_upload::print(gsl::span<const std::vector<unsigned>> rgbs,
                                std::error_code &ec) const {
  static const std::array<std::uint8_t, 2> init_bytes = {0x1b, 0x40};
  static const std::array<std::uint8_t, 3> line_space = {0x1b, 0x33, 24};
  static const std::array<std::uint8_t, 3> bit_image_mode = {0x1b, 0x2A, 33};
  static const std::array<std::uint8_t, 2> print_and_feed = {0x1b, 0x4A};
  static const std::array<std::uint8_t, 2> lf_cr = {0x0A, 0x0D};
  static const std::array<std::uint8_t, 1> xon = {0x11};

  device_selection::select_printer(ec);
  communication_engine::change_parity_to_none();
  comm_->write(init_bytes, ec);
  comm_->write(line_space, ec);

  for (auto y = 0u; y < rgbs.size(); y += 24) {
    const auto width = image_width(rgbs[y].size());
    comm_->write(bit_image_mode, ec);
    comm_->write(width, ec);
    for (auto x = 0u; x < rgbs[y].size(); x++) {
      const auto to_print = recollect_slice(y, x, rgbs);
      comm_->write(to_print, ec);
      if (x % 25 == 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds{10});
      }
    }

    comm_->write(lf_cr, ec);
  }
}

auto printer_file_upload::recollect_slice(
    unsigned y, unsigned x, gsl::span<const std::vector<unsigned int>> rgbs)
    -> std::array<std::uint8_t, 3> {
  std::array<std::uint8_t, 3> slices{};
  for (auto row = y, i = 0u; row < y + 24 && i < 3; row += 8, i++) {
    std::uint8_t slice{0};
    for (auto b = 0; b < 8; b++) {
      const auto current_row = row + b;
      if (current_row >= rgbs.size()) {
        continue;
      }

      const auto rgb = rgbs[current_row][x];
      const auto print = print_rgb(rgb);
      slice |= static_cast<std::uint8_t>((print ? 1 : 0) << (7 - b));
    }

    slices[i] = slice;
  }

  return slices;
}

auto printer_file_upload::print_rgb(unsigned rgb) -> bool {
  static const int threshold = 150;

  const auto a = (rgb >> 24) & 0xff;
  if (a != 0xff) {
    return false;
  }

  const auto r = (rgb >> 16) & 0xff;
  const auto g = (rgb >> 8) & 0xff;
  const auto b = rgb & 0xff;
  const auto luminance = (int)(0.299 * r + 0.587 * g + 0.114 * b);
  return (luminance < threshold);
}

auto printer_file_upload::image_width(unsigned len)
    -> std::array<std::uint8_t, 2> {
  return std::array<std::uint8_t, 2>{
      static_cast<std::uint8_t>(0x00ff & len),
      static_cast<std::uint8_t>((0xff00 & len) >> 8)};
}