#pragma once

#include <array>
#include <gsl/span>
#include <memory>
#include <system_error>
#include <vector>

namespace KvCommunication {
class dispatcher;
}

struct download_t {
  using dispatch_t = std::unique_ptr<KvCommunication::dispatcher>;
  download_t(std::size_t count, dispatch_t disp)
      : count_{count}, disp_{std::move(disp)} {}

  void execute(std::error_code& ec);
  void on_sane(std::uint32_t id, gsl::span<const std::uint8_t> encoded);
  void on_corrupt(std::uint32_t id);
  void on_done();
  void on_error(const std::error_code& ec);

  std::size_t count_;
  dispatch_t disp_;
  std::array<std::vector<std::uint16_t>, 12> channels_;
};