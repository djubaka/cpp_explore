#include "command_execution.hpp"
#include <fmt/ostream.h>
#include <kv-communication/dispatcher.hpp>
#include <kv-communication/log_bytes.hpp>

std::shared_ptr<command_execution> command_execution::create(
    std::vector<std::string> cmds, dispatch_t dispatch) {
  return std::shared_ptr<command_execution>(
      new command_execution(std::move(cmds), dispatch));
}

command_execution::command_execution(std::vector<std::string> cmds,
                                     dispatch_t dispatch)
    : cmds_{std::move(cmds)}, dispatch_{dispatch} {}

void command_execution::execute(std::error_code& ec, callback_t callback) {
  using namespace KvCommunication;

  if (ec) {
    return;
  }

  callback_ = std::move(callback);
  for (const auto& cmd : cmds_) {
    auto cmd_bytes = create_cmd(cmd);
    auto me = shared_from_this();
    auto cb = [me](data_t data, const std::error_code& ec) {
      me->on_command_done(data, ec);
    };

    dispatch_->dispatch(dispatch_ingredients{std::move(cmd_bytes),
                                             match<new_line>(), std::move(cb)});
  }
}

void command_execution::on_command_done(data_t data,
                                        const std::error_code& err) {
  if (callback_) {
    callback_(data, err);
  }
}

std::vector<std::uint8_t> command_execution::create_cmd(
    const std::string& cmd) {
  std::vector<std::uint8_t> cmd_bytes{};
  cmd_bytes.reserve(cmd.size() + 2);
  cmd_bytes.push_back(27);
  cmd_bytes.insert(cmd_bytes.end(), cmd.begin(), cmd.end());
  cmd_bytes.push_back(0);

  return cmd_bytes;
}

commander_t::commander_t(std::vector<std::string> cmds,
                         std::unique_ptr<KvCommunication::dispatcher> disp)
    : cmds_{std::move(cmds)}, disp_{std::move(disp)} {}

void commander_t::execute(std::error_code& ec) {
  auto cmd = command_execution::create(cmds_, disp_.get());
  auto cb = [](gsl::span<const std::uint8_t> data, const std::error_code& err) {
    err ? fmt::print("error: {}\n", err.message())
        : fmt::print("response: {}\n", KvCommunication::log_bytes(data));
  };

  cmd->execute(ec, cb);
}