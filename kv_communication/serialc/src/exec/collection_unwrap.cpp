#include "collection_unwrap.hpp"
#include <algorithm>
#include <gsl/span_ext>
#include <kv-util/crc.hpp>
#include "collection_state.hpp"

namespace {
auto compute_crc(gsl::span<const std::uint8_t> samples) {
  crc_cpp::crc16_xmodem crc;
  for (auto c : samples) {
    crc.update(c);
  }
  return crc.final();
}
}  // namespace

auto collection_unwrap::process(gsl::span<const std::uint8_t> data) const
    -> package_t {
  if (not state_->has_more()) {
    return end_package{};
  }

  if (not completed_properly(data)) {
    return corrupted_package{state_->package_id()};
  }

  return package_samples(data);
}

bool collection_unwrap::completed_properly(
    gsl::span<const std::uint8_t> data) const {
  const auto len = data.size();
  if (len < 2) {
    return false;
  }

  const auto r = data[len - 2];
  const auto n = data[len - 1];
  return (r == '\r') and (n == '\n');
}

auto collection_unwrap::package_samples(
    gsl::span<const std::uint8_t> data) const -> package_t {
  const auto dc = std::find(data.begin(), data.end(), ':');
  const auto samples_beg = dc + 1;
  const auto samples_end = data.end() - 2;  // ignore trailing \r\n
  const auto msg_size = std::distance(samples_beg, samples_end);
  const auto sane_size = (msg_size == state_->package_size());
  if (not sane_size) {
    return corrupted_package{state_->package_id()};
  }

  const auto sample_span = gsl::make_span(&*samples_beg, msg_size);
  const auto msg_crc = compute_crc(sample_span);
  const auto sane_crc = (msg_crc == state_->package_crc());
  if (not sane_crc) {
    return corrupted_package{state_->package_id()};
  }

  return sane_package{state_->package_id(), sample_span};
}