#include "channel_selection.hpp"
#include <kv-gpio/device_selection.hpp>

void channel_selection::execute(std::error_code &ec) {
  if (ec) return;

  switch (channel_) {
    case config::channel_t::control:
      device_selection::select_control_board(ec);
      break;
    case config::channel_t::measurement:
      device_selection::select_measurement(ec);
      break;
    case config::channel_t::printer:
      device_selection::select_printer(ec);
      break;
    case config::channel_t::module_1:
      device_selection::select_module_1(ec);
      break;
    case config::channel_t::module_2:
      device_selection::select_module_2(ec);
      break;
    case config::channel_t::module_3:
      device_selection::select_module_3(ec);
      break;
  }
}