#pragma once

#include <gsl/span>
#include <string>
#include <system_error>
#include <vector>

namespace KvCommunication {
class ICommunication;
}

struct file_upload {
  file_upload(std::string path, KvCommunication::ICommunication* comm)
      : path_{std::move(path)}, comm_{comm} {}

  file_upload(const file_upload&) = delete;
  file_upload& operator=(const file_upload&) = delete;

  file_upload(file_upload&&) noexcept = default;
  file_upload& operator=(file_upload&&) noexcept = default;

  virtual void execute(std::error_code& ec);

 protected:
  std::string path_;
  KvCommunication::ICommunication* comm_;
};

struct printer_file_upload : file_upload {
  printer_file_upload(std::string path, KvCommunication::ICommunication* comm)
      : file_upload(std::move(path), comm) {}

  void execute(std::error_code& ec);

  void print(gsl::span<const std::vector<unsigned>> rgbs,
             std::error_code& ec) const;

  static auto recollect_slice(unsigned y, unsigned x,
                              gsl::span<const std::vector<unsigned>> rgbs)
      -> std::array<std::uint8_t, 3>;

  static auto print_rgb(unsigned rgb) -> bool;

  static auto image_width(unsigned len) -> std::array<std::uint8_t, 2>;
};