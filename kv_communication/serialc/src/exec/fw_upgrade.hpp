#pragma once

#include <memory>
#include <string>
#include <system_error>

namespace isp {
class firmware_upgrade;
}

class fw_upgrade {
 public:
  explicit fw_upgrade(std::string file_path,
                      std::unique_ptr<isp::firmware_upgrade> exec);
  ~fw_upgrade();

  fw_upgrade(fw_upgrade&&) noexcept;
  fw_upgrade& operator=(fw_upgrade&&) noexcept;

  void execute(std::error_code& ec);

 private:
  void connect_to_upgrade_events(std::error_code& ec);

 private:
  std::string file_path_;
  std::unique_ptr<isp::firmware_upgrade> exec_;
};