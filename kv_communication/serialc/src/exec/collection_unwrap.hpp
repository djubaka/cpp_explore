#pragma once

#include <cstdint>
#include <gsl/span>
#include <variant>

struct sane_package {
  std::uint32_t id;
  gsl::span<const std::uint8_t> encoded;
};

struct corrupted_package {
  std::uint32_t id;
};

struct end_package {};

class collection_state;
class collection_unwrap {
  using package_t = std::variant<sane_package, corrupted_package, end_package>;

 public:
  explicit collection_unwrap(const collection_state* state) : state_{state} {}
  auto process(gsl::span<const std::uint8_t> data) const -> package_t;

 private:
  bool completed_properly(gsl::span<const std::uint8_t> data) const;
  auto package_samples(gsl::span<const std::uint8_t> data) const -> package_t;

  const collection_state* state_;
};