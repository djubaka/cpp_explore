#include "collection_state.hpp"
#include <cstdio>
#include <kv-util/predicate.hpp>

namespace {
auto from_string(const std::string& value) {
  std::uint32_t result;
  std::sscanf(value.c_str(), "%4u", &result);
  return result;
}

auto from_hex_string(const std::string& value) {
  std::uint32_t result;
  std::sscanf(value.c_str(), "0x%4X", &result);
  return result;
}
}  // namespace

bool collection_state::done(gsl::span<const std::uint8_t> samples) {
  has_more_ = (not predicate::ends_with(samples, "DTSO\r\n"));
  if (has_more_) {
    return true;
  }

  const auto sentinel = std::find(samples.begin(), samples.end(), ':');
  if (sentinel == samples.end()) {
    return false;
  }

  parse_prefix(samples.begin(), sentinel);
  return match_expected(samples, sentinel);
}

void collection_state::parse_prefix(iterator beg, iterator end) {
  const auto psc = parse_package_id(beg, end);
  const auto ssc = parse_package_size(psc + 1, end);
  const auto cdc = parse_package_crc(ssc + 1, end);
};

auto collection_state::parse_package_id(iterator beg, iterator end)
    -> iterator {
  // we're trying to parse a form of P=XXX;
  const auto eq = std::find(beg, end, '=');
  const auto sc = std::find(beg, end, ';');
  package_id_ = from_string(std::string{eq + 1, sc});
  return sc;
}

auto collection_state::parse_package_size(iterator beg, iterator end)
    -> iterator {
  // we're trying to parse a form of D=0xYYYY;
  const auto eq = std::find(beg, end, '=');
  const auto sc = std::find(beg, end, ';');
  package_sz_ = from_hex_string(std::string{eq + 1, sc});
  return sc;
}

auto collection_state::parse_package_crc(iterator beg, iterator end)
    -> iterator {
  // we're trying to parse a form of CRC=0xYYYY:
  const auto eq = std::find(beg, end, '=');
  package_crc_ = from_hex_string(std::string{eq + 1, end});
  return end;
}

bool collection_state::match_expected(gsl::span<const std::uint8_t> samples,
                                      iterator sentinel) const {
  if (not package_identified()) {
    return false;
  }

  const auto sz = std::distance(sentinel + 1, samples.end());
  return (sz > *package_sz_);
}