#pragma once

#include <functional>
#include <memory>
#include <system_error>
#include "config.hpp"

namespace CLI {
class App;
}

namespace exec {

class executioner {
 public:
  template <typename T>
  executioner(T x)
      : self_{std::make_unique<executioner_model<T>>(std::move(x))} {}

  executioner(executioner&&) noexcept = default;
  executioner& operator=(executioner&&) noexcept = default;

  void execute(std::error_code& ec) { self_->execute_(ec); }

 private:
  struct executioner_concept {
    virtual ~executioner_concept() = default;
    virtual void execute_(std::error_code& ec) = 0;
  };

  template <typename T>
  struct executioner_model final : public executioner_concept {
    executioner_model(T x) : exec_{std::move(x)} {}
    void execute_(std::error_code& ec) override { exec_.execute(ec); }

    T exec_;
  };

  std::unique_ptr<executioner_concept> self_;
};

using exec_t = executioner;
exec_t commander(const config& cfg);
exec_t channel_select(const config& cfg);
exec_t file_sending(const config& cfg);
exec_t print_image(const config& cfg);
exec_t firmware_update(const config& cfg);
exec_t collect_samples(const config& cfg);
exec_t pin_export(const config& cfg);
exec_t pin_lookup(const config& cfg);

}  // namespace exec