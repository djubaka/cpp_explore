#pragma once

#include <system_error>
#include "config.hpp"

struct channel_selection {
  explicit channel_selection(config::channel_t channel) : channel_{channel} {}
  void execute(std::error_code& ec);
  config::channel_t channel_;
};