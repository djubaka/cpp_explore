#include "sample_collection.hpp"
#include <kv-communication/dispatcher.hpp>
#include "collection_unwrap.hpp"

using namespace KvCommunication;

std::shared_ptr<sample_collection> sample_collection::create(
    std::size_t sample_count, dispatch_t dispatch) {
  return std::shared_ptr<sample_collection>(
      new sample_collection(sample_count, dispatch));
}

void sample_collection::execute(std::error_code& ec) {
  if (not ec) {
    collect();
  }
}

void sample_collection::collect() { start_collecting('G'); }

void sample_collection::recollect() { start_collecting('R'); }

void sample_collection::start_collecting(std::uint8_t collection_byte) {
  state_.reset();

  auto me = shared_from_this();
  auto cb = [me](data_t data, const std::error_code& ec) {
    return ec ? me->on_error(ec) : me->on_package_arrival(data);
  };

  auto end_condition = [me](gsl::span<const std::uint8_t> samples) {
    return me->state_.done(samples);
  };

  dispatch_->dispatch(dispatch_ingredients{
      {collection_byte}, match(end_condition), std::move(cb)});
}

void sample_collection::on_package_arrival(data_t data) {
  collection_unwrap wr{&state_};
  const auto package = wr.process(data);
  std::visit(package_visitor{this}, package);
}

void package_visitor::operator()(const sane_package& sp) const {
  sc_->on_sane_package(sp.id, sp.encoded);
  sc_->collect();
}

void package_visitor::operator()(const corrupted_package& cp) const {
  sc_->on_corrupted_package(cp.id);
  sc_->recollect();
}

void package_visitor::operator()(const end_package& ep) const {
  sc_->on_collection_finished();
}