#pragma once

#include <cstdint>
#include <gsl/span>
#include <optional>

class collection_state {
 public:
  using iterator = gsl::span<const std::uint8_t>::iterator;
  bool done(gsl::span<const std::uint8_t> samples);
  auto package_id() const { return package_id_.value_or(0); }
  auto package_size() const { return package_sz_.value_or(0); }
  auto package_crc() const { return package_crc_.value_or(0); }
  auto reset() {
    has_more_ = true;
    package_id_.reset();
    package_sz_.reset();
    package_crc_.reset();
  }

  auto has_more() const { return has_more_; }

 private:
  void parse_prefix(iterator beg, iterator end);
  auto parse_package_id(iterator beg, iterator end) -> iterator;
  auto parse_package_size(iterator beg, iterator end) -> iterator;
  auto parse_package_crc(iterator beg, iterator end) -> iterator;
  bool match_expected(gsl::span<const std::uint8_t> samples,
                      iterator sentinel) const;

  bool package_identified() const {
    return package_id_ and package_sz_ and package_crc_;
  }

 private:
  bool has_more_{true};
  std::optional<std::uint32_t> package_id_;
  std::optional<std::uint32_t> package_sz_;
  std::optional<std::uint32_t> package_crc_;
};