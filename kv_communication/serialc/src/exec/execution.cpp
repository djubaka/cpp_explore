#include "execution.hpp"
#include <kv-communication/communication_engine.hpp>
#include <kv-isp/engine/firmware_upgrade.hpp>
#include <kv-isp/processor/processor_factory.hpp>
#include "channel_selection.hpp"
#include "command_execution.hpp"
#include "download_t.hpp"
#include "file_upload.hpp"
#include "fw_upgrade.hpp"
#include "pin_ctl.hpp"

namespace exec {
using namespace communication_engine;

exec_t channel_select(const config& cfg) {
  return channel_selection{cfg.channel};
}

exec_t file_sending(const config& cfg) {
  return file_upload{*cfg.file, uart(cfg.port, cfg.baudrate)};
}

exec_t print_image(const config& cfg) {
  return printer_file_upload(*cfg.file, uart(cfg.port, cfg.baudrate));
}

exec_t commander(const config& cfg) {
  auto comm = uart(cfg.port, cfg.baudrate);
  return commander_t{cfg.cmds, device_dispatch(comm)};
}

exec_t firmware_update(const config& cfg) {
  auto comm = uart(cfg.port, cfg.baudrate);
  auto fw = std::make_unique<isp::firmware_upgrade>(
      comm, isp::arm_preparation(), isp::arm_builder());

  return fw_upgrade{*cfg.file, std::move(fw)};
}

exec_t collect_samples(const config& cfg) {
  auto comm = uart(cfg.port, cfg.baudrate);
  return download_t{*cfg.sample_count, device_dispatch(comm)};
}

exec_t pin_export(const config& cfg) {
  return export_pin{*cfg.pin_number, cfg.pin_mode};
}

exec_t pin_lookup(const config& cfg) { return lookup_pin{*cfg.pin_number}; }

}  // namespace exec