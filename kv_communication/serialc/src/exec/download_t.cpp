#include "download_t.hpp"
#include <fmt/format.h>
#include <cstdio>
#include <kv-communication/dispatcher.hpp>
#include <kv-util/rle.hpp>
#include "command_execution.hpp"
#include "sample_collection.hpp"

namespace {
auto as_sample(const std::string& quartet) {
  std::uint16_t result;
  std::sscanf(quartet.c_str(), "%4hX", &result);
  return result;
}
}  // namespace

void download_t::execute(std::error_code& ec) {
  auto cb = [this, &ec](gsl::span<const std::uint8_t> data,
                        const std::error_code& err) {
    ec = err;
    if (not err) {
      auto collect = sample_collection::create(count_, disp_.get());
      collect->on_sane_package.connect(&download_t::on_sane, this);
      collect->on_corrupted_package.connect(&download_t::on_corrupt, this);
      collect->on_collection_finished.connect(&download_t::on_done, this);
      collect->on_error.connect(&download_t::on_error, this);
      collect->execute(ec);
    }
  };

  auto cmd = fmt::format("mGMDAC{:05}", count_);
  auto cmder = command_execution::create({cmd}, disp_.get());
  cmder->execute(ec, cb);
}

void download_t::on_sane(std::uint32_t id,
                         gsl::span<const std::uint8_t> encoded) {
  const auto decoded = rle::decode(encoded);
  fmt::print("encoded: {}. decoded: {}\n", encoded.size(), decoded.size());
  auto quartet = std::string{4, '0'};
  for (auto i = 0u; i < decoded.size(); i += 4) {
    std::copy(decoded.begin() + i, decoded.begin() + i + 4, quartet.begin());
    auto& channel = channels_[i / 400];
    channel.push_back(as_sample(quartet));
  }

  fmt::print("package {} sane. collecting next ...\n", id);
}

void download_t::on_corrupt(std::uint32_t id) {
  fmt::print("package {} corrupt. retrying ...\n", id);
}

void download_t::on_done() { fmt::print("all packages recieved ...\n"); }

void download_t::on_error(const std::error_code& ec) {
  fmt::print("error occured: {}\n", ec.message());
}