#pragma once

#include <cstdint>
#include <functional>
#include <gsl/span>
#include <memory>
#include <system_error>
#include <vector>

namespace KvCommunication {
class dispatcher;
}

struct command_execution : std::enable_shared_from_this<command_execution> {
  using data_t = gsl::span<const std::uint8_t>;
  using dispatch_t = KvCommunication::dispatcher*;
  using callback_t = std::function<void(data_t, const std::error_code&)>;

  static std::shared_ptr<command_execution> create(
      std::vector<std::string> cmds, dispatch_t dispatch);

  void execute(std::error_code& ec, callback_t callback);

 private:
  command_execution(std::vector<std::string> cmds, dispatch_t dispatch);
  void on_command_done(data_t data, const std::error_code& err);
  static std::vector<std::uint8_t> create_cmd(const std::string& cmd);

 private:
  std::vector<std::string> cmds_;
  callback_t callback_;
  dispatch_t dispatch_{nullptr};
};

struct commander_t {
  commander_t(std::vector<std::string> cmds,
              std::unique_ptr<KvCommunication::dispatcher> disp);

  void execute(std::error_code& ec);

  std::vector<std::string> cmds_;
  std::unique_ptr<KvCommunication::dispatcher> disp_;
};