#pragma once

#include <system_error>
#include "config.hpp"

struct export_pin {
  export_pin(int sodimm, config::pin_t mode) : sodimm_{sodimm}, mode_{mode} {}
  void execute(std::error_code& ec);

  int sodimm_;
  config::pin_t mode_;
};

struct lookup_pin {
  lookup_pin(int sodimm) : sodimm_{sodimm} {}
  void execute(std::error_code& ec);
  int sodimm_;
};