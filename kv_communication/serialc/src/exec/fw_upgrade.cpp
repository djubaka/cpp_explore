#include "fw_upgrade.hpp"
#include <fmt/ostream.h>
#include <fmt/printf.h>
#include <iostream>
#include <kv-communication/icommunication.hpp>
#include <kv-isp/engine/firmware_upgrade.hpp>

fw_upgrade::fw_upgrade(std::string file_path,
                       std::unique_ptr<isp::firmware_upgrade> exec)
    : file_path_(std::move(file_path)), exec_{std::move(exec)} {}

fw_upgrade::~fw_upgrade() = default;

fw_upgrade::fw_upgrade(fw_upgrade&&) noexcept = default;
fw_upgrade& fw_upgrade::operator=(fw_upgrade&&) noexcept = default;

void fw_upgrade::execute(std::error_code& ec) {
  connect_to_upgrade_events(ec);
  exec_->execute(file_path_);
}

void fw_upgrade::connect_to_upgrade_events(std::error_code& ec) {
  exec_->reading_file_begin.connect([] { fmt::print("reading file... "); });
  exec_->reading_file_end.connect([&](const std::error_code& err) {
    ec = err;
    fmt::print("\t{0}\n", ec.message());
  });

  exec_->entering_boot_begin.connect(
      [] { fmt::print("entering bootloader... "); });

  exec_->entering_boot_end.connect([&](const std::error_code& err) {
    ec = err;
    fmt::print("\t{0}\n", ec.message());
  });

  exec_->configuring_boot_begin.connect(
      [] { fmt::print("configuring boot io... "); });

  exec_->configuring_boot_end.connect([&](const std::error_code& err) {
    ec = err;
    fmt::print("\t{0}\n", ec.message());
  });

  exec_->erasing_memory_begin.connect([] {
    fmt::print("erasing memory... ");
    std::cout << std::flush;
  });

  exec_->erasing_memory_end.connect([&](const std::error_code& err) {
    ec = err;
    fmt::print("\t{0}\n", ec.message());
  });

  exec_->programming_memory_begin.connect(
      [] { fmt::print("programming memory... "); });

  exec_->programming_memory_progress.connect([](int progress) {
    fmt::print("\rprogramming memory... \t{} [%]", progress);
    std::cout << std::flush;
  });

  exec_->programming_memory_end.connect([&](const std::error_code& err) {
    ec = err;
    fmt::print("\rprogramming memory... \t{0}\n", ec.message());
  });

  exec_->verifying_memory_begin.connect(
      []() { fmt::print("verifying memory... "); });

  exec_->verifying_memory_progress.connect([](int progress) {
    fmt::print("\rverifying memory... \t{} [%]", progress);
  });

  exec_->verifying_memory_end.connect([&](const std::error_code& err) {
    ec = err;
    fmt::print("\rverifying memory... \t{}\n", err.message());
  });

  exec_->leaving_boot_begin.connect([] {
    fmt::print("leaving bootloader... ");
    std::cout << std::flush;
  });

  exec_->leaving_boot_end.connect([&](const std::error_code err) {
    ec = err;
    fmt::print("\t{0}\n", err.message());
  });
}