#include "pin_ctl.hpp"
#include <fmt/core.h>
#include <kv-gpio/gpio_export.hpp>
#include <kv-gpio/gpio_value.hpp>

void export_pin::execute(std::error_code &ec) {
  gpio_export exp{sodimm_gpio_pin{sodimm_}};
  return (mode_ == config::pin_t::input) ? exp.as_input(ec) : exp.as_output(ec);
}

void lookup_pin::execute(std::error_code &ec) {
  sodimm_gpio_pin sodim{sodimm_};
  fmt::print("reading from: {}\n", sodim.value_path());
  gpio_value gpio{sodimm_gpio_pin{sodim}};
  const auto lvl = gpio.read(ec);
  if (not ec) {
    fmt::print("pin {} lvl: {}\n", sodimm_, lvl);
  }
}