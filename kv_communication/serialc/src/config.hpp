#pragma once

#include <optional>
#include <string>
#include <vector>

struct config {
  enum class channel_t {
    control,
    measurement,
    printer,
    module_1,
    module_2,
    module_3
  };

  enum class pin_t { input, output };

  unsigned int baudrate{4800};
  std::string port{"/dev/ttyUSB0"};
  channel_t channel{channel_t::control};
  pin_t pin_mode{pin_t::input};
  std::vector<std::string> cmds;
  std::optional<std::string> file;
  std::optional<std::size_t> sample_count;
  std::optional<int> pin_number;
};