cmake_minimum_required(VERSION 3.15)
project(serialc
    VERSION 1.0.0.0
    LANGUAGES CXX
    DESCRIPTION "console app for device testing"
)

add_executable(serialc src/main.cpp
    src/config.hpp
    src/exec/execution.hpp
    src/exec/execution.cpp
    src/exec/file_upload.hpp
    src/exec/file_upload.cpp
    src/exec/command_execution.hpp
    src/exec/command_execution.cpp
    src/exec/channel_selection.hpp
    src/exec/channel_selection.cpp
    src/exec/fw_upgrade.hpp
    src/exec/fw_upgrade.cpp
    src/exec/sample_collection.hpp
    src/exec/sample_collection.cpp
    src/exec/collection_state.hpp
    src/exec/collection_state.cpp
    src/exec/collection_unwrap.hpp
    src/exec/collection_unwrap.cpp
    src/exec/download_t.hpp
    src/exec/download_t.cpp
    src/exec/pin_ctl.hpp
    src/exec/pin_ctl.cpp
)

target_include_directories(serialc
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

find_package(fmt REQUIRED)
find_package(CLI11 REQUIRED)
find_package(PalSigslot REQUIRED)

target_link_libraries(serialc
    PRIVATE
        fmt::fmt
        CLI11::CLI11
        Pal::Sigslot
        kv::communication
        kv::gpio
        kv::isp
        kv::util
)

target_link_options(serialc
	PRIVATE
		$<$<CONFIG:Release>:-s>
)

include(GNUInstallDirs)
install(TARGETS serialc
	RUNTIME DESTINATION	${CMAKE_INSTALL_BINDIR}
			COMPONENT	serialc_RunTime
)