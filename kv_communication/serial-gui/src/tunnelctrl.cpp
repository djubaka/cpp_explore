#include "tunnelctrl.hpp"
#include <kv-communication/communication_engine.hpp>
#include "commctrl.hpp"

TunnelCtrl::TunnelCtrl(QObject* parent)
    : QObject(parent),
      ext_conn_{CommCtrl::bridge()},
      dispatch_{CommCtrl::dispatch()} {}

TunnelCtrl::~TunnelCtrl() = default;

void TunnelCtrl::startListening() {
  ext_conn_.message_arrived.connect(&TunnelCtrl::onInput, this);
  ext_conn_.start_listening();
}

void TunnelCtrl::onInput(TunnelCtrl::message_view_t message) {
  using namespace KvCommunication;

  emit inputArrived(asHexEnabledMessage(message));

  using data_span_t = KvCommunication::dispatch_ingredients::data_span_t;
  auto callback = [this](data_span_t data, const std::error_code& ec) {
    onDevResponse(data, ec);
  };

  dispatch_->dispatch(dispatch_ingredients{
      {message.begin(), message.end()}, match<new_line>(), callback});
}

void TunnelCtrl::onDevResponse(TunnelCtrl::message_view_t message,
                               const std::error_code& ec) {
  if (ec) {
    emit errorOccured(QString::fromStdString(ec.message()));
  } else {
    emit outputArrived(asHexEnabledMessage(message));
    ext_conn_.send_message({message.begin(), message.end()});
  }
}

QString TunnelCtrl::asHexEnabledMessage(TunnelCtrl::message_view_t message) {
  QString result;
  for (auto c : message) {
    if (std::isprint(c)) {
      result.append(c);
    } else {
      result.append(QString("0x%1").arg(QString::number(c, 16)));
    }

    result.append(' ');
  }

  return result;
}
