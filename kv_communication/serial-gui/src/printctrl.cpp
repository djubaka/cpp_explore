#include "printctrl.hpp"
#include <kv-communication/communication_engine.hpp>
#include <kv-communication/icommunication.hpp>
#include "commctrl.hpp"

PrintCtrl::PrintCtrl(QObject* parent)
    : QObject(parent), comm_(CommCtrl::controlUart()) {
  communication_engine::change_baudrate(115200);
  communication_engine::change_parity_to_none();
}

PrintCtrl::~PrintCtrl() {
  communication_engine::change_baudrate(4800);
  communication_engine::change_parity_to_even();
}

void PrintCtrl::testPrinter() {
  std::vector<std::uint8_t> cmd = {'t', 'e', 's', 't', '\r'};
  std::error_code ec;
  comm_->write(cmd, ec);
  if (ec) {
    std::printf("print error: %s\n", ec.message().c_str());
  }
}