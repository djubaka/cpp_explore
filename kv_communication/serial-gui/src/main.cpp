#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "firmwarectrl.hpp"
#include "gpioctrl.hpp"
#include "printctrl.hpp"
#include "tunnelctrl.hpp"
#include "usbwatchctrl.hpp"

namespace SerialGui {
void registerTypes() {
  qmlRegisterType<GpioCtrl>("kvteam.control", 1, 0, "KvGpio");
  qmlRegisterType<TunnelCtrl>("kvteam.control", 1, 0, "Tunnel");
  qmlRegisterType<FirmwareCtrl>("kvteam.control", 1, 0, "Firmware");
  qmlRegisterType<UsbWatchCtrl>("kvteam.control", 1, 0, "UsbWatch");
  qmlRegisterType<PrintCtrl>("kvteam.control", 1, 0, "PrintIo");
}
}  // namespace SerialGui

int main(int argc, char *argv[]) {
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  SerialGui::registerTypes();

  QGuiApplication app(argc, argv);
  QQmlApplicationEngine engine;
  const QUrl url(QStringLiteral("qrc:/assets/qml/main.qml"));
  QObject::connect(
      &engine, &QQmlApplicationEngine::objectCreated, &app,
      [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl) QCoreApplication::exit(-1);
      },
      Qt::QueuedConnection);
  engine.load(url);

  return app.exec();
}
