#include "firmwarectrl.hpp"
#include <QStringBuilder>
#include <kv-communication/communication_engine.hpp>
#include <kv-isp/engine/firmware_upgrade.hpp>
#include <kv-isp/processor/processor_factory.hpp>
#include "commctrl.hpp"

FirmwareCtrl::FirmwareCtrl(QObject* parent) : QObject(parent) {}

FirmwareCtrl::~FirmwareCtrl() = default;

void FirmwareCtrl::executeUpdate(const QString& filePath) {
  if (upgrade_) return;

  upgrade_ = std::make_unique<isp::firmware_upgrade>(
      CommCtrl::controlUart(), isp::arm_preparation(), isp::arm_builder());

  connectToUpgradeEvents();

  upgrade_->execute(filePath.toStdString());
}

void FirmwareCtrl::connectToUpgradeEvents() {
  upgrade_->reading_file_begin.connect(
      [this] { emit updateMessage("reading file"); });

  upgrade_->reading_file_end.connect([this](const std::error_code& ec) {
    emit updateMessage(fromErrorCode("reading file: ", ec));
  });

  upgrade_->entering_boot_begin.connect(
      [this] { emit updateMessage("entering boot"); });

  upgrade_->entering_boot_end.connect([this](const std::error_code& ec) {
    emit updateMessage(fromErrorCode("entering boot: ", ec));
  });

  upgrade_->configuring_boot_begin.connect(
      [this] { emit updateMessage("configuring boot"); });

  upgrade_->configuring_boot_end.connect([this](const std::error_code& ec) {
    emit updateMessage(fromErrorCode("configuring boot: ", ec));
  });

  upgrade_->erasing_memory_begin.connect(
      [this] { emit updateMessage("erasing memory"); });

  upgrade_->erasing_memory_end.connect([this](const std::error_code& ec) {
    emit updateMessage(fromErrorCode("erasing memory: ", ec));
  });

  upgrade_->programming_memory_begin.connect(
      [this] { emit updateMessage("programming memory"); });

  upgrade_->programming_memory_progress.connect(
      [this](int progress) { emit updateProgress(progress); });

  upgrade_->programming_memory_end.connect([this](const std::error_code& ec) {
    emit updateMessage(fromErrorCode("programming memory: ", ec));
  });

  upgrade_->verifying_memory_begin.connect(
      [this] { emit updateMessage("memory verification"); });

  upgrade_->verifying_memory_progress.connect(
      [this](int progress) { emit updateProgress(progress); });

  upgrade_->verifying_memory_end.connect([this](const std::error_code& ec) {
    emit updateMessage(fromErrorCode("memory verification: ", ec));
  });

  upgrade_->leaving_boot_begin.connect(
      [this] { emit updateMessage("leaving boot"); });

  upgrade_->leaving_boot_end.connect([this](const std::error_code& ec) {
    emit updateMessage(fromErrorCode("leaving boot: ", ec));
    if (!ec) {
      emit updateFinished();
    }
  });
}

QString FirmwareCtrl::fromErrorCode(const QString& prefix,
                                    const std::error_code& ec) {
  return prefix % QString::fromStdString(ec.message());
}
