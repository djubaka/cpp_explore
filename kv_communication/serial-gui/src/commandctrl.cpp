#include "commandctrl.hpp"
#include <QDebug>
#include <QString>
#include <kv-communication/dispatcher.hpp>

namespace {
auto asByteArray(gsl::span<const std::uint8_t> data) {
  QByteArray byteArray;
  byteArray.reserve(data.size());
  for (auto b : data) {
    byteArray.push_back(b);
  }

  return byteArray;
}

auto asCmdBytes(std::string_view cmd) {
  std::vector<std::uint8_t> cmd_bytes;
  cmd_bytes.reserve(cmd.size() + 2);
  cmd_bytes.push_back(27);
  for (auto c : cmd) {
    cmd_bytes.push_back(c);
  }
  cmd_bytes.push_back(0);

  return cmd_bytes;
}

}  // namespace

std::shared_ptr<CommandCtrl> CommandCtrl::create(
    KvCommunication::dispatcher* disp) {
  return std::shared_ptr<CommandCtrl>(new CommandCtrl(disp));
}

void CommandCtrl::execute(const std::string& cmd, callback_t cb) const {
  auto io_cb = [cb_ = std::move(cb)](gsl::span<const std::uint8_t> data,
                                     const std::error_code& ec) {
    qDebug() << asByteArray(data);
    cb_(ec);
  };

  KvCommunication::dispatch_ingredients ingr;
  ingr.callback = std::move(io_cb);
  ingr.data = asCmdBytes(cmd);
  ingr.match = KvCommunication::match<KvCommunication::new_line>();
  dispatch_->dispatch(std::move(ingr));
}

void CommandCtrl::execute(const std::string& cmd,
                          response_callback_t cb) const {
  auto io_cb = [cb_ = std::move(cb)](gsl::span<const std::uint8_t> data,
                                     const std::error_code& ec) {
    if (data.size() > 3) {
      cb_(std::string{data.begin() + 1, data.end() - 2}, ec);
    } else {
      cb_(std::string{}, ec);
    }
  };

  KvCommunication::dispatch_ingredients ingr;
  ingr.callback = std::move(io_cb);
  ingr.data = asCmdBytes(cmd);
  ingr.match = KvCommunication::match<KvCommunication::new_line>();
  dispatch_->dispatch(std::move(ingr));
}