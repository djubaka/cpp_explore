#include "commctrl.hpp"
#include <kv-communication/communication_engine.hpp>

KvCommunication::ICommunication* CommCtrl::controlUart() {
  return communication_engine::uart("/dev/ttymxc2", 4800);
}

KvCommunication::external_connection CommCtrl::bridge() {
  return communication_engine::server_with_usb_handle("/dev/ttymxc1", 4800);
}

KvCommunication::dispatcher* CommCtrl::dispatch() {
  static auto dispatcher = communication_engine::device_dispatch(controlUart());
  return dispatcher.get();
}
