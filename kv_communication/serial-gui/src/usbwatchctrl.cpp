#include "usbwatchctrl.hpp"
#include <QDebug>

UsbWatchCtrl::UsbWatchCtrl(QObject* parent) : QObject(parent) {
  watch_.inserted.connect(&UsbWatchCtrl::onCreated, this);
  watch_.removed.connect(&UsbWatchCtrl::onDeleted, this);
  watch_.error.connect(&UsbWatchCtrl::onError, this);
  watch_.start_watch();
}

UsbWatchCtrl::~UsbWatchCtrl() { watch_.stop_watch(); }

bool UsbWatchCtrl::available() const { return usbPath_.has_value(); }

QUrl UsbWatchCtrl::usbPath() const { return usbPath_.value_or(QUrl{}); }

void UsbWatchCtrl::onCreated(const std::string& dev_node) {
  std::error_code ec;
  if (auto mount_path = mount_.mount(dev_node, ec); !ec) {
    usbPath_ = QUrl::fromLocalFile(QString::fromStdString(mount_path));
    emit availableChanged(true);
    emit usbPathChanged(*usbPath_);
  }

  onError(ec);
}

void UsbWatchCtrl::onDeleted(const std::string& dev_node) {
  std::error_code ec;
  if (mount_.unmount(dev_node, ec); !ec) {
    usbPath_ = std::nullopt;
    emit availableChanged(false);
    emit usbPathChanged(QUrl{});
  }

  onError(ec);
}

void UsbWatchCtrl::onError(const std::error_code& ec) {
  if (ec) {
    qDebug() << QString::fromStdString(ec.message());
  }
}