#pragma once

#include <kv-communication/dispatcher.hpp>
#include <kv-communication/external_connection.hpp>
#include <kv-communication/icommunication.hpp>
#include <memory>

namespace CommCtrl {
KvCommunication::ICommunication* controlUart();
KvCommunication::external_connection bridge();
KvCommunication::dispatcher* dispatch();
};  // namespace CommCtrl