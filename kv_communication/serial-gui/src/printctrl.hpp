#pragma once

#include <QObject>

namespace KvCommunication {
class ICommunication;
}

class PrintCtrl : public QObject {
  Q_OBJECT
 public:
  PrintCtrl(QObject* parent = nullptr);
  ~PrintCtrl();

  Q_INVOKABLE void testPrinter();

 private:
  KvCommunication::ICommunication* comm_;
};