#pragma once

#include <QAbstractListModel>
#include <QThread>
#include <functional>
#include <kv-gpio/sodimm_gpio_pin.hpp>
#include <system_error>
#include <vector>

class GpioModel {
 public:
  explicit GpioModel(sodimm_gpio_pin pin) : pin_(pin) {}
  auto pin() const { return pin_.sodimm_number(); }
  auto level(std::error_code &ec) const -> QString;
  auto direction(std::error_code &ec) const -> QString;

 private:
  sodimm_gpio_pin pin_;
};

class GpioEvent : public QThread {
  Q_OBJECT
 public:
  explicit GpioEvent(sodimm_gpio_pin pin, QObject *parent = nullptr);

 protected:
  void run() override;

 signals:
  void eventOnChange();

 private:
  sodimm_gpio_pin pin_;
};

class GpioCtrl : public QAbstractListModel {
  Q_OBJECT
  Q_PROPERTY(
      QString stopInputState READ stopInputState NOTIFY stopInputStateChanged)
 public:
  enum GpioRoles { SoDimm = Qt::UserRole, Level, Direction };
  explicit GpioCtrl(QObject *parent = nullptr);

  Q_INVOKABLE void refresh();
  Q_INVOKABLE void setTrigger();
  Q_INVOKABLE void resetTrigger();
  Q_INVOKABLE void selectControl();
  Q_INVOKABLE void selectMeasurement();
  Q_INVOKABLE void selectPrinter();
  Q_INVOKABLE void selectFirstDrmModule();
  Q_INVOKABLE void selectSecondDrmModule();
  Q_INVOKABLE void selectThirdDrmModule();
  Q_INVOKABLE void selectFourthDrmModule();
  Q_INVOKABLE void selectFifthDrmModule();
  Q_INVOKABLE void selectSixthDrmModule();
  Q_INVOKABLE void selectStop();
  Q_INVOKABLE void resetStop();

  QHash<int, QByteArray> roleNames() const override { return roleNames_; }
  QVariant data(const QModelIndex &index, int role) const override;
  int rowCount(const QModelIndex &idx) const override;

  QString stopInputState() const { return stopInputState_; }

 signals:
  void errorOccured(const QString &errorMessage);
  void selected(const QString &module);
  void onTriggerUpdated();
  void stopInputStateChanged(const QString &state);

 private:
  void configureGpioRoles();
  void configureOutPins();
  void configureInPins();
  void refreshTriggerPin();
  void refreshStartPin();
  void refreshControlPins();
  void refreshModulePins();
  void selectModule(const QString &module,
                    std::function<void(std::error_code &)>);
  void collectStopInputState();

 private:
  QHash<int, QByteArray> roleNames_;
  std::vector<GpioModel> gpios_;
  QString stopInputState_{"N/A"};
};
