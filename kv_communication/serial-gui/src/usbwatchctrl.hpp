#pragma once

#include <QObject>
#include <QString>
#include <QUrl>
#include <kv-service/usb_watch/usb_mount.hpp>
#include <kv-service/usb_watch/usb_watcher.hpp>
#include <optional>

namespace service {
class usb_watcher;
}

class UsbWatchCtrl : public QObject {
  Q_OBJECT
  Q_PROPERTY(bool available READ available NOTIFY availableChanged)
  Q_PROPERTY(QUrl usbPath READ usbPath NOTIFY usbPathChanged)

 public:
  explicit UsbWatchCtrl(QObject* parent = nullptr);
  ~UsbWatchCtrl();

  bool available() const;
  QUrl usbPath() const;

 signals:
  void availableChanged(bool available);
  void usbPathChanged(const QUrl&);

 private:
  void onCreated(const std::string& dev_node);
  void onDeleted(const std::string& dev_node);
  void onError(const std::error_code& ec);

 private:
  service::usb_mount mount_;
  service::usb_watcher watch_;
  std::optional<QUrl> usbPath_;
};