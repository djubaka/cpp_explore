#pragma once

#include <QObject>
#include <QString>
#include <memory>
#include <system_error>

namespace isp {
class firmware_upgrade;
}

class FirmwareCtrl : public QObject {
  Q_OBJECT
 public:
  explicit FirmwareCtrl(QObject* parent = nullptr);
  ~FirmwareCtrl();
  Q_INVOKABLE void executeUpdate(const QString& filePath);

 signals:
  void updateMessage(const QString& msg);
  void updateProgress(int prog);
  void updateFinished();

 private:
  void connectToUpgradeEvents();
  static QString fromErrorCode(const QString& prefix,
                               const std::error_code& ec);

 private:
  std::unique_ptr<isp::firmware_upgrade> upgrade_;
};
