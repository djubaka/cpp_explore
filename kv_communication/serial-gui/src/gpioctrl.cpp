#include "gpioctrl.hpp"
#include <QDebug>
#include <kv-gpio/device_selection.hpp>
#include <kv-gpio/gpio_direction.hpp>
#include <kv-gpio/gpio_event.hpp>
#include <kv-gpio/gpio_export.hpp>
#include <kv-gpio/gpio_value.hpp>
#include "commandctrl.hpp"
#include "commctrl.hpp"

auto GpioModel::level(std::error_code &ec) const -> QString {
  const gpio_value val{pin_};
  const auto lvl = val.level(ec);
  return lvl.has_value() ? QString(*lvl) : QString{"N/A"};
}

auto GpioModel::direction(std::error_code &ec) const -> QString {
  const gpio_direction dir{pin_};
  const auto drct = dir.direction(ec);
  return ec ? QString{"N/A"} : QString::fromStdString(drct);
}

GpioEvent::GpioEvent(sodimm_gpio_pin pin, QObject *parent)
    : pin_(pin), QThread(parent) {}

void GpioEvent::run() {
  while (true) {
    std::error_code ec;
    await_event_on_change(pin_.sodimm_number(), ec);
    if (ec) {
      qDebug() << "gpio event error: " << ec.message().c_str();
    } else {
      emit eventOnChange();
    }
  }
}

GpioCtrl::GpioCtrl(QObject *parent) : QAbstractListModel(parent) {
  configureGpioRoles();
  configureOutPins();
  configureInPins();

  connect(this, &GpioCtrl::onTriggerUpdated, this, &GpioCtrl::refreshTriggerPin,
          Qt::QueuedConnection);

  auto event = new GpioEvent(98, this);
  connect(event, &GpioEvent::eventOnChange, this, &GpioCtrl::refreshStartPin);
  connect(event, &GpioEvent::finished, event, &QObject::deleteLater);
  event->start();
}

void GpioCtrl::configureGpioRoles() {
  roleNames_[GpioRoles::SoDimm] = "soDimm";
  roleNames_[GpioRoles::Level] = "level";
  roleNames_[GpioRoles::Direction] = "direction";
}

void GpioCtrl::configureOutPins() {
  std::vector<int> outs = {103, 101, 97, 85, 79};
  std::error_code ec;
  for (auto o : outs) {
    sodimm_gpio_pin pin{o};
    gpio_export exp{pin};
    exp.as_output(ec);
    if (ec) {
      qDebug() << QString("Pin %1 error: %2").arg(o).arg(ec.message().c_str());
    } else {
      gpios_.push_back(GpioModel{pin});
    }
  }
}

void GpioCtrl::configureInPins() {
  std::vector<int> ins = {98, 133};
  std::error_code ec;
  for (auto i : ins) {
    sodimm_gpio_pin pin{i};
    gpio_export exp{pin};
    exp.as_input(ec);
    if (ec) {
      qDebug() << QString("Pin %1 error: %2").arg(i).arg(ec.message().c_str());
    } else {
      gpios_.push_back(GpioModel{pin});
    }
  }
}

void GpioCtrl::refresh() {
  const QModelIndex beg = createIndex(0, 0);
  const QModelIndex end = createIndex(gpios_.size() - 1, 0);
  emit dataChanged(beg, end);
}

void GpioCtrl::refreshTriggerPin() {
  const QModelIndex beg = createIndex(gpios_.size() - 1, 0);
  const QModelIndex end = createIndex(gpios_.size() - 1, 0);
  emit dataChanged(beg, end);
}

void GpioCtrl::refreshStartPin() {
  const QModelIndex beg = createIndex(gpios_.size() - 2, 0);
  const QModelIndex end = createIndex(gpios_.size() - 2, 0);
  emit dataChanged(beg, end);
}

void GpioCtrl::refreshControlPins() {
  const QModelIndex beg = createIndex(0, 0);
  const QModelIndex end = createIndex(1, 0);
  emit dataChanged(beg, end);
}

void GpioCtrl::refreshModulePins() {
  const QModelIndex beg = createIndex(2, 0);
  const QModelIndex end = createIndex(4, 0);
  emit dataChanged(beg, end);
}

void GpioCtrl::setTrigger() {
  selectMeasurement();
  const auto cmdCtrl = CommandCtrl::create(CommCtrl::dispatch());
  cmdCtrl->execute("mST1", [this](const std::error_code &ec) {
    if (ec) {
      qDebug() << "error: " << ec.message().c_str();
    }

    emit onTriggerUpdated();
  });
}

void GpioCtrl::resetTrigger() {
  selectMeasurement();
  const auto cmdCtrl = CommandCtrl::create(CommCtrl::dispatch());
  cmdCtrl->execute("mST0", [this](const std::error_code &ec) {
    if (ec) {
      qDebug() << "error: " << ec.message().c_str();
    }

    emit onTriggerUpdated();
  });
}

void GpioCtrl::selectControl() {
  selectModule("Control", device_selection::select_control_board);
  refreshControlPins();
}

void GpioCtrl::selectMeasurement() {
  selectModule("Measurement", device_selection::select_measurement);
  refreshControlPins();
}

void GpioCtrl::selectPrinter() {
  selectModule("Printer / Chopper", device_selection::select_printer);
  refreshControlPins();
}

void GpioCtrl::selectFirstDrmModule() {
  selectModule("DRM 1", device_selection::select_module_1);
  refreshModulePins();
}

void GpioCtrl::selectSecondDrmModule() {
  selectModule("DRM 2", device_selection::select_module_2);
  refreshModulePins();
}

void GpioCtrl::selectThirdDrmModule() {
  selectModule("DRM 3", device_selection::select_module_3);
  refreshModulePins();
}

void GpioCtrl::selectFourthDrmModule() {
  selectModule("DRM 4", device_selection::select_module_4);
}

void GpioCtrl::selectFifthDrmModule() {
  selectModule("DRM 5", device_selection::select_module_5);
}

void GpioCtrl::selectSixthDrmModule() {
  selectModule("DRM 6", device_selection::select_module_6);
}

void GpioCtrl::selectStop() {
  selectModule("STOP", device_selection::select_stop);
  refreshModulePins();
  collectStopInputState();
}

void GpioCtrl::resetStop() {
  selectModule("RESET STOP", device_selection::reset_stop);
  refreshModulePins();
  collectStopInputState();
}

void GpioCtrl::collectStopInputState() {
  selectMeasurement();
  const auto cmdCtrl = CommandCtrl::create(CommCtrl::dispatch());
  cmdCtrl->execute(
      "mRSP", [this](const std::string &response, const std::error_code &ec) {
        if (ec) {
          qDebug() << "error: " << ec.message().c_str();
        }

        stopInputState_ = QString::fromStdString(response);
        emit stopInputStateChanged(stopInputState_);
      });
}

int GpioCtrl::rowCount(const QModelIndex &idx) const { return gpios_.size(); }

QVariant GpioCtrl::data(const QModelIndex &index, int role) const {
  const auto row = index.row();
  if (row < 0 || row >= rowCount(index)) {
    return QVariant();
  }

  const auto &gpio = gpios_[row];
  std::error_code ec;
  switch (role) {
    case GpioRoles::SoDimm:
      return gpio.pin();
    case GpioRoles::Level:
      return gpio.level(ec);
    case GpioRoles::Direction:
      return gpio.direction(ec);
  }

  if (ec) {
    qDebug() << QString("Pin %1 error: %2")
                    .arg(gpio.pin())
                    .arg(ec.message().c_str());
  }

  return QVariant{};
}

void GpioCtrl::selectModule(
    const QString &module,
    std::function<void(std::error_code &)> module_selection) {
  std::error_code ec;
  module_selection(ec);

  if (ec) {
    emit errorOccured(QString::fromStdString(ec.message()));
  } else {
    emit selected(module);
  }
}
