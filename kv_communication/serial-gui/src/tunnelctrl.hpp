#pragma once

#include <QObject>
#include <QString>
#include <cstdint>
#include <gsl/span>
#include <kv-communication/external_connection.hpp>
#include <memory>
#include <system_error>

namespace KvCommunication {
class dispatcher;
}

class TunnelCtrl : public QObject {
  Q_OBJECT
 public:
  explicit TunnelCtrl(QObject* parent = nullptr);
  ~TunnelCtrl();

  Q_INVOKABLE void startListening();

 private:
  using message_view_t = gsl::span<const std::uint8_t>;
  void onInput(message_view_t message);
  void onDevResponse(message_view_t message, const std::error_code& ec);

  static QString asHexEnabledMessage(message_view_t message);

 signals:
  void inputArrived(const QString& input);
  void outputArrived(const QString& output);
  void errorOccured(const QString& error);

 private:
  KvCommunication::external_connection ext_conn_;
  KvCommunication::dispatcher* dispatch_;
};
