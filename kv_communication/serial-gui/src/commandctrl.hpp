#pragma once

#include <functional>
#include <memory>
#include <system_error>

namespace KvCommunication {
class dispatcher;
}

class CommandCtrl : public std::enable_shared_from_this<CommandCtrl> {
 public:
  using callback_t = std::function<void(const std::error_code&)>;
  using response_callback_t =
      std::function<void(const std::string&, const std::error_code& ec)>;
  static std::shared_ptr<CommandCtrl> create(KvCommunication::dispatcher* disp);
  void execute(const std::string& cmd, callback_t cb) const;
  void execute(const std::string& cmd, response_callback_t cb) const;

 private:
  explicit CommandCtrl(KvCommunication::dispatcher* disp) : dispatch_(disp) {}

 private:
  KvCommunication::dispatcher* dispatch_;
};