import QtQuick 2.9
import QtQuick.Controls 2.2
import kvteam.control 1.0

Item {
    id: root

    property KvGpio gpioCtl: null

    function leave() {
        StackView.view.pop(null)
    }

    ListView {
        id: gpioList
        anchors.topMargin: 10
        anchors.fill: parent
        spacing: 5
        clip: true
        model: gpioCtl
        delegate: GpioStatusItem {
        	width: gpioList.width
        	height: 50
        	pinNumber: String("Pin[%1]").arg(model.soDimm)
        	level: String("= %1").arg(model.level)
        }

        section.property: 'direction'
        section.delegate: Item {
            width: root.width
            height: 55

            Rectangle {
                anchors.fill: parent
                anchors.bottomMargin: 3
                radius: 3
                color: 'orange'

                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: 'black'
                    text: section
                    font.pixelSize: 20
                    font.bold: true
                }
            }
        }

        footer: Text {
            width: gpioList.width
            height: 50
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: gpioCtl.stopInputState
            font.bold: true
            font.pixelSize: 20
            color: 'white'
        }
    }

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        spacing: 10
        height: 100

        Button {
            id: setBtn
            height: 100
            width: 200
            contentItem: Text {
                height: resetBtn.height
                width: resetBtn.width
                font.pixelSize: 20
                font.bold: true
                color: 'white'
                text: 'SET TRIGGER'
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            background: Rectangle {
                color: 'black'
                height: 100
                width: 200
                radius: 5
                border.width: 3
                border.color: (setBtn.down) ? 'green' : 'orange'
            }

            onClicked: {
                gpioCtl.setTrigger()
            }
        }

        Button {
            id: resetBtn
            height: 100
            width: 200
            contentItem: Text {
                height: resetBtn.height
                width: resetBtn.width
                font.pixelSize: 20
                font.bold: true
                color: 'white'
                text: 'RESET TRIGGER'
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            background: Rectangle {
                color: 'black'
                height: 100
                width: 200
                radius: 5
                border.width: 3
                border.color: (resetBtn.down) ? 'green' : 'orange'
            }

            onClicked: {
                gpioCtl.resetTrigger()
            }
        }

        Button {
            id: btnSetStop
            height: 100
            width: 200
            contentItem: Text {
                height: resetBtn.height
                width: resetBtn.width
                font.pixelSize: 20
                font.bold: true
                color: 'white'
                text: 'SET STOP'
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            background: Rectangle {
                color: 'black'
                height: 100
                width: 200
                radius: 5
                border.width: 3
                border.color: (btnSetStop.down) ? 'green' : 'orange'
            }

            onClicked: {
                gpioCtl.selectStop()
            }
        }

        Button {
            id: btnResetStop
            height: 100
            width: 200
            contentItem: Text {
                height: resetBtn.height
                width: resetBtn.width
                font.pixelSize: 20
                font.bold: true
                color: 'white'
                text: 'RESET STOP'
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            background: Rectangle {
                color: 'black'
                height: 100
                width: 200
                radius: 5
                border.width: 3
                border.color: (btnResetStop.down) ? 'green' : 'orange'
            }

            onClicked: {
                gpioCtl.resetStop()
            }
        }
    }

    function logMessage(message) {
        console.log(String("[log]: %1\n").arg(message))
    }
}
