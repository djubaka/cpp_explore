import QtQuick 2.9
import QtQuick.Controls 2.2

Item {
    id: root

    property alias text: txtFile.text
    property alias updateVisibile: btnRun.visible

    signal clicked
    signal updateClicked

    function updateActivity(activity) {
        progress.activityText = activity
    }

    function updateProgress(prog) {
        progress.value = prog
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            root.clicked()
        }
    }

    Text {
        id: txtFile
        width: parent.width / 3
        height: parent.height
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: enabled ? 'white' : 'grey'
        font.pixelSize: Qt.application.font.pixelSize * 2
    }

    Button {
        id: btnRun
        anchors.centerIn: parent
        width: parent.width * 0.25
        height: parent.height * 0.9
        text: 'update'
        onClicked: {
            progress.visible = true
            fwUpdate.executeUpdate(filePath)
            root.updateClicked()
        }
    }

    ProgressBar {
        id: progress
        padding: 2
        visible: false
        width: parent.width * 0.25
        height: parent.height * 0.9
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 10

        property alias activityText: txtActivity.text

        from: 0.0
        to: 100.0

        background: Rectangle {
            implicitWidth: 200
            implicitHeight: 6
            color: "#e6e6e6"
            radius: 3
        }

        contentItem: Item {
            implicitWidth: 200
            implicitHeight: 4

            Rectangle {
                width: progress.visualPosition * parent.width
                height: parent.height
                radius: 2
                color: "#17a81a"
            }

            Text {
                id: txtActivity
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }
}
