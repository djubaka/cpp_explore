import QtQuick 2.9
import QtQuick.Controls 2.2
import kvteam.control 1.0

Item {
    id: root

    function leave() {
        StackView.view.pop(null)
    }

    PrintIo {
        id: printIo
    }

    Button {
        text: 'Test Printer'
        width: 200
        height: 150
        font.bold: true
        font.pixelSize: 20
        anchors.centerIn: parent

        onClicked: {
            printIo.testPrinter()
        }
    }
}
