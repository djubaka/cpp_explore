import QtQuick 2.9
import QtQuick.Controls 2.2
import Qt.labs.folderlistmodel 2.1
import kvteam.control 1.0

Item {
    id: root

    property alias watchFolder: fwModel.folder

    FolderListModel {
        id: fwModel
        nameFilters: ["*.hex", "*.dvfc"]
        showDirs: false
    }

    function leave() {
        StackView.view.pop(null)
    }

    Firmware {
        id: fwUpdate
        onUpdateMessage: {
            fwList.currentItem.updateActivity(msg)
        }

        onUpdateProgress: {
            fwList.currentItem.updateProgress(prog)
        }

        onUpdateFinished: {
            root.leave()
        }
    }

    ListView {
        id: fwList
        anchors.topMargin: 10
        anchors.fill: parent
        spacing: 5
        clip: true
        model: fwModel
        delegate: FirmwareList {
            width: parent.width
            height: 50

            text: fileName
            updateVisibile: fwList.currentIndex === index

            onClicked: {
                fwList.currentIndex = index
            }

            onUpdateClicked: {
                fwList.enabled = false
            }

        }

        highlightFollowsCurrentItem: true
        highlight: Rectangle {
            width: parent.width
            color: 'transparent'
            border.color: 'white'
            border.width: 2
            radius: 5
            visible: enabled
        }

        Component.onCompleted: {
            fwList.currentIndex = 0
        }
    }
}
