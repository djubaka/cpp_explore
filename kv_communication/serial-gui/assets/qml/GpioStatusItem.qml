import QtQuick 2.9
import QtQuick.Controls 2.2

Rectangle {
	id: root
	property alias pinNumber: pinLbl.text
	property alias level: lvlLbl.text

	color: 'black'
	border.color: 'yellow'

	Row {
		anchors.centerIn: parent
		height: root.height
		spacing: 5

		Text {
			id: pinLbl
			color: 'white'
			font.pixelSize: Qt.application.font.pixelSize * 2
			anchors.verticalCenter: parent.verticalCenter
		}

		Text {
			id: lvlLbl
			color: 'white'
			font.pixelSize: Qt.application.font.pixelSize * 2
			anchors.verticalCenter: parent.verticalCenter
		}
	}
}