import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import kvteam.control 1.0

ApplicationWindow {
    id: window
    visible: true
    width: Screen.width
    height: Screen.height
    title: qsTr("serial-gui")

    KvGpio {
        id: gpioCtrl
        onErrorOccured: {
            stackView.currentItem.logMessage(errorMessage)
        }

        onSelected: {
            stackView.currentItem.logMessage(String("%1 selected").arg(module))
        }
    }

    Tunnel {
        id: tunnel
        onInputArrived: {
            stackView.currentItem.logInput(input)
        }

        onOutputArrived: {
            stackView.currentItem.logOutput(output)
        }

        onErrorOccured: {
            stackView.currentItem.logMessage(error)
        }
    }

    UsbWatch {
        id: usbWatch
    }

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: (stackView.depth > 1) ? '<' : '>'
            font.pixelSize: Qt.application.font.pixelSize * 2
            onClicked: {
                if(stackView.depth === 1)
                    drawer.open()
                else
                    stackView.pop(null)
            }
        }

        Label {
            text: "Device Io"
            font.pixelSize: Qt.application.font.pixelSize * 2
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("Control")
                width: parent.width
                onClicked: {
                    gpioCtrl.selectControl()
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("Measurement")
                width: parent.width
                onClicked: {
                    gpioCtrl.selectMeasurement()
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("Chopper")
                width: parent.width
                onClicked: {
                    gpioCtrl.selectPrinter()
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("DRM 1")
                width: parent.width
                onClicked: {
                    gpioCtrl.selectFirstDrmModule()
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("DRM 2")
                width: parent.width
                onClicked: {
                    gpioCtrl.selectSecondDrmModule()
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("DRM 3")
                width: parent.width
                onClicked: {
                    gpioCtrl.selectThirdDrmModule()
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("DRM 4")
                width: parent.width
                onClicked: {
                    gpioCtrl.selectFourthDrmModule()
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("DRM 5")
                width: parent.width
                onClicked: {
                    gpioCtrl.selectFifthDrmModule()
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("DRM 6")
                width: parent.width
                onClicked: {
                    gpioCtrl.selectSixthDrmModule()
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("Gpio")
                width: parent.width
                onClicked: {
                    stackView.push("GpioStatus.qml", {
                            "gpioCtl": gpioCtrl
                        })
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("Printer")
                width: parent.width
                onClicked: {
                    gpioCtrl.selectPrinter()
                    stackView.push("PrintStatus.qml")
                    drawer.close()
                }
            }

            ItemDelegate {
                font.pixelSize: Qt.application.font.pixelSize * 2
                text: qsTr("Firmware")
                width: parent.width
                onClicked: {
                    stackView.push("FirmwareIo.qml", {
                            "watchFolder": usbWatch.usbPath
                        })
                    drawer.close()
                }

                enabled: usbWatch.available
            }
        }
    }

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: DeviceIo {}
    }

    Component.onCompleted: {
        gpioCtrl.selectControl()
        tunnel.startListening()
    }
}
