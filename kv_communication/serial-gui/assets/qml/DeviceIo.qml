import QtQuick 2.9
import QtQuick.Controls 2.2
import kvteam.control 1.0

Item {
    id: root

    ScrollView {
        id: sv
        anchors.fill: parent

        Flickable {
            id: flick
            anchors.fill: parent
            flickableDirection: Flickable.VerticalFlick
            contentY: 50
            contentHeight: home.contentHeight

            TextArea {
                id: home
                height: contentHeight
                wrapMode: TextArea.Wrap
                font.pixelSize: Qt.application.font.pixelSize * 1.5

                onTextChanged: {
                    flick.contentY = contentHeight - flick.height
                }
            }
        }
    }

    function logMessage(message) {
        _appendTo(String("[log\t%2]: %1\n")
            .arg(message)
            .arg(currentDate()))
    }

    function logInput(message) {
        _appendTo(String("[in\t%2]: %1\n")
            .arg(message)
            .arg(currentDate()))
    }

    function logOutput(message) {
        _appendTo(String("[out\t%2]: %1\n")
            .arg(message)
            .arg(currentDate()))
    }

    function _appendTo(message) {
        home.text += message
    }

    function currentDate() {
        var date = new Date()
        return date.toLocaleString(Qt.locale("en-US"), "dd-MM-yyyy HH:mm:ss")
    }
}
