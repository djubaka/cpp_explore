#include "message_reaction.hpp"
#include <kv-communication/communication_engine.hpp>
#include <kv-communication/external_connection.hpp>
#include "tunnel.hpp"

namespace reaction {
message_reaction as_tunnel(KvCommunication::external_connection* ext_conn,
                           const std::string& port, unsigned int baud) {
  return tunnel(ext_conn, communication_engine::device_dispatch(
                              communication_engine::uart(port, baud)));
}
}  // namespace reaction