#include "tunnel.hpp"
#include <fmt/printf.h>
#include <kv-communication/dispatcher.hpp>
#include <kv-communication/external_connection.hpp>

tunnel::tunnel(KvCommunication::external_connection* ext_conn,
               dispatch_t dispatch)
    : ext_conn_{ext_conn}, dispatch_{std::move(dispatch)} {}

void tunnel::process(message_view_t message) const {
  using namespace KvCommunication;

  using data_span = KvCommunication::dispatch_ingredients::data_span_t;
  auto callback = [this](data_span data, const std::error_code& ec) {
    if (ec) {
      fmt::print("error: {}\n", ec.message());
    } else {
      ext_conn_->send_message({data.begin(), data.end()});
    }
  };

  dispatch_->dispatch(dispatch_ingredients{
      {message.begin(), message.end()}, match<new_line>(), callback});
}

tunnel::tunnel(tunnel&& x) noexcept
    : ext_conn_{x.ext_conn_}, dispatch_{std::move(x.dispatch_)} {
  x.ext_conn_ = nullptr;
}

tunnel& tunnel::operator=(tunnel&& x) noexcept {
  ext_conn_ = x.ext_conn_;
  dispatch_ = std::move(x.dispatch_);
  x.ext_conn_ = nullptr;
  return *this;
}