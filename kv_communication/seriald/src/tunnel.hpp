#pragma once

#include <cstdint>
#include <gsl/span>
#include <memory>
#include <system_error>

namespace KvCommunication {
class external_connection;
class dispatcher;
}  // namespace KvCommunication

struct tunnel {
  using message_view_t = gsl::span<const std::uint8_t>;
  using dispatch_t = std::unique_ptr<KvCommunication::dispatcher>;

  tunnel(KvCommunication::external_connection* ext_conn, dispatch_t dispatch);
  void process(message_view_t message) const;

  tunnel(const tunnel&) = delete;
  tunnel& operator=(const tunnel&) = delete;

  tunnel(tunnel&& x) noexcept;
  tunnel& operator=(tunnel&& x) noexcept;

  KvCommunication::external_connection* ext_conn_;
  dispatch_t dispatch_;
};