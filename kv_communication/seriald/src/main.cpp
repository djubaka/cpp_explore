#include <CLI/CLI.hpp>
#include <kv-communication/communication_engine.hpp>
#include <optional>
#include "message_reaction.hpp"

int main(int argc, char** argv) {
  std::optional<KvCommunication::external_connection> server;
  std::optional<message_reaction> react;

  std::string port{};
  unsigned baud{};
  CLI::App app{"A serial port listening server"};
  app.add_option("--port", port, "Serial port name")->required();
  app.add_option("--baud", baud, "Serial port baudrate")->required();

  auto tunn = app.add_subcommand("tunnel", "Tunnel through port");

  std::string tunnel_port;
  tunn->add_option("--port", tunnel_port, "Tunnel port")->required();

  unsigned tunnel_baud;
  tunn->add_option("--baud", tunnel_baud, "Tunnel baudrate")->required();
  tunn->callback([&]() {
    server = communication_engine::server_with_usb_handle(port, baud);
    react = reaction::as_tunnel(&(*server), tunnel_port, tunnel_baud);
  });

  CLI11_PARSE(app, argc, argv);

  server->message_arrived.connect(&message_reaction::process, &(*react));
  server->start_listening();
  std::cin.get();
}