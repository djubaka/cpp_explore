#pragma once

#include <cstdint>
#include <gsl/span>
#include <memory>
#include <string>

class message_reaction;
namespace KvCommunication {
class external_connection;
}

namespace reaction {
message_reaction as_tunnel(KvCommunication::external_connection* ext_conn,
                           const std::string& port, unsigned baud);
}

class message_reaction {
  using message_t = gsl::span<const std::uint8_t>;

 public:
  template <typename T>
  message_reaction(T x)
      : self_{std::make_shared<message_reaction_model<T>>(std::move(x))} {}

  void process(message_t msg) const { self_->process_(msg); }

 private:
  class message_reaction_concept {
   public:
    virtual ~message_reaction_concept() = default;
    virtual void process_(message_t) const = 0;
  };

  template <typename T>
  class message_reaction_model final : public message_reaction_concept {
   public:
    message_reaction_model(T x) : x_{std::move(x)} {}
    void process_(message_t msg) const override { x_.process(msg); }
    T x_;
  };

  std::shared_ptr<const message_reaction_concept> self_;
};