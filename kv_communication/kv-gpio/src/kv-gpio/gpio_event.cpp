#include "kv-gpio/gpio_event.hpp"
#include "kv-gpio/gpio_export.hpp"
#include "kv-gpio/gpio_value.hpp"

void await_event_on_falling_edge(int sodimm, std::error_code& ec) {
  gpio_event ev{sodimm_gpio_pin{sodimm}};
  ev.wait_on_falling_edge(ec);
}

void await_event_on_rising_edge(int sodimm, std::error_code& ec) {
  gpio_event ev{sodimm_gpio_pin{sodimm}};
  ev.wait_on_rising_edge(ec);
}

void await_event_on_change(int sodimm, std::error_code& ec) {
  gpio_event ev{sodimm_gpio_pin{sodimm}};
  ev.wait_on_change(ec);
}

void gpio_event::wait_on_falling_edge(std::error_code& ec) const {
  gpio_export gpio_exp{pin_};
  gpio_exp.as_input(ec);
  gpio_exp.trigger_on_falling_edge(ec);

  gpio_value gpio_val{pin_};
  gpio_val.wait_for_trigger(ec);
}

void gpio_event::wait_on_rising_edge(std::error_code& ec) const {
  gpio_export gpio_exp{pin_};
  gpio_exp.as_input(ec);
  gpio_exp.trigger_on_rising_edge(ec);

  gpio_value gpio_val{pin_};
  gpio_val.wait_for_trigger(ec);
}

void gpio_event::wait_on_change(std::error_code& ec) const {
  gpio_export gpio_exp{pin_};
  gpio_exp.as_input(ec);
  gpio_exp.trigger_on_change(ec);

  gpio_value gpio_val{pin_};
  gpio_val.wait_for_trigger(ec);
}