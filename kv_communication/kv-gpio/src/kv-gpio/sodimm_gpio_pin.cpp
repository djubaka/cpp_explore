#include "kv-gpio/sodimm_gpio_pin.hpp"
#include <sstream>
#include <unordered_map>
#include <utility>

namespace sodimm_gpio_register {

struct gpio_bank {
  int controller;
  int number;
};

gpio_bank by_sodimm_pin(int sodimm_pin) {
  static std::unordered_map<int, gpio_bank> mapped_gpios = {
      {133, gpio_bank{.controller = 2, .number = 3}},
      {103, gpio_bank{.controller = 2, .number = 20}},
      {101, gpio_bank{.controller = 2, .number = 21}},
      {98, gpio_bank{.controller = 1, .number = 15}},
      {97, gpio_bank{.controller = 2, .number = 18}},
      {85, gpio_bank{.controller = 6, .number = 6}},
      {79, gpio_bank{.controller = 2, .number = 19}}};

  return mapped_gpios.at(sodimm_pin);
}

}  // namespace sodimm_gpio_register

sodimm_gpio_pin::sodimm_gpio_pin(int pin) : pin_{pin} {}

int sodimm_gpio_pin::as_kernel_pin() const {
  auto gpio_bank = sodimm_gpio_register::by_sodimm_pin(pin_);
  return (32 * (gpio_bank.controller - 1) + gpio_bank.number);
}

std::string sodimm_gpio_pin::as_kernel_pin_string() const {
  return std::to_string(as_kernel_pin());
}

std::string sodimm_gpio_pin::direction_path() const {
  std::stringstream sstr;
  sstr << "/sys/class/gpio/gpio" << as_kernel_pin() << "/direction";
  return sstr.str();
}

std::string sodimm_gpio_pin::edge_path() const {
  std::stringstream sstr;
  sstr << "/sys/class/gpio/gpio" << as_kernel_pin() << "/edge";
  return sstr.str();
}

std::string sodimm_gpio_pin::value_path() const {
  std::stringstream sstr;
  sstr << "/sys/class/gpio/gpio" << as_kernel_pin() << "/value";
  return sstr.str();
}
