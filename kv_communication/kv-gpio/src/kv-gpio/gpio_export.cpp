#include "kv-gpio/gpio_export.hpp"
#include <fcntl.h>
#include <unistd.h>
#include <cerrno>

namespace {

void write_to(const char* path, std::string_view in, std::error_code& ec) {
  if (ec) return;

  if (auto fd = ::open(path, O_WRONLY); fd > 0) {
    if (auto res = ::write(fd, in.data(), in.size()); res < 0) {
      if (errno != EBUSY) {
        // if it's busy it means that we've probably exported sometime previous
        // any other error should not have occured, so report it
        ec = std::error_code{errno, std::generic_category()};
      }
    }
    ::close(fd);
  } else {
    ec = std::error_code(errno, std::generic_category());
  }
}

}  // namespace

gpio_export::gpio_export(sodimm_gpio_pin pin) : pin_{pin} {}

void gpio_export::as_input(std::error_code& ec) const {
  do_pin_export(ec);
  configure_as("in", ec);
}

void gpio_export::as_output(std::error_code& ec) const {
  do_pin_export(ec);
  configure_as("out", ec);
}

void gpio_export::do_pin_export(std::error_code& ec) const {
  const char* gpio_exp = "/sys/class/gpio/export";
  const auto pin_str = pin_.as_kernel_pin_string();
  write_to(gpio_exp, pin_str, ec);
}

void gpio_export::configure_as(std::string_view descr,
                               std::error_code& ec) const {
  auto direction = pin_.direction_path();
  write_to(direction.c_str(), descr, ec);
}

void gpio_export::trigger_on_rising_edge(std::error_code& ec) const {
  trigger_on("rising", ec);
}

void gpio_export::trigger_on_falling_edge(std::error_code& ec) const {
  trigger_on("falling", ec);
}

void gpio_export::trigger_on_change(std::error_code& ec) const {
  trigger_on("both", ec);
}

void gpio_export::trigger_on(std::string_view edge, std::error_code& ec) const {
  auto e = pin_.edge_path();
  write_to(e.c_str(), edge, ec);
}
