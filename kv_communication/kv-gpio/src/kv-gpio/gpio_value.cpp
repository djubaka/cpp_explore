#include "kv-gpio/gpio_value.hpp"
#include <fcntl.h>
#include <poll.h>
#include <unistd.h>
#include <cerrno>

namespace {

void set_error(std::error_code& ec) {
  ec = std::error_code(errno, std::generic_category());
}

void wait_for(pollfd* poll_gpio, std::error_code& ec) {
  if (ec) return;

  for (;;) {
    if (auto res = ::poll(poll_gpio, 1, -1); res == -1) {
      return set_error(ec);
    }

    if ((poll_gpio->revents & POLLPRI) == POLLPRI) {
      return;
    }
  }
}

}  // namespace

gpio_value::gpio_value(sodimm_gpio_pin pin) : pin_{pin} {
  auto value = pin_.value_path();
  fd_ = ::open(value.c_str(), O_RDWR);
}

gpio_value::~gpio_value() {
  if (fd_ != -1) {
    ::close(fd_);
  }
}

void gpio_value::turn_on(std::error_code& ec) const { set_as("1", ec); }

void gpio_value::turn_off(std::error_code& ec) const { set_as("0", ec); }

void gpio_value::set_as(std::string_view level, std::error_code& ec) const {
  if (ec) return;

  if (auto res = ::write(fd_, level.data(), level.size()); res < 0) {
    ec = std::error_code{errno, std::generic_category()};
  }
}

char gpio_value::read(std::error_code& ec) const {
  if (ec) {
    return -1;
  }

  char level;
  if (auto res = ::read(fd_, &level, 1); res < 0) {
    ec = std::error_code{errno, std::generic_category()};
  }

  return level;
}

auto gpio_value::level(std::error_code& ec) const
    -> std::optional<std::uint8_t> {
  if (ec) {
    return std::nullopt;
  }

  const auto c = read(ec);
  return ec ? std::nullopt : std::make_optional(static_cast<std::uint8_t>(c));
}

void gpio_value::wait_for_trigger(std::error_code& ec) const {
  pollfd poll_gpio;
  poll_gpio.fd = fd_;
  poll_gpio.events = POLLPRI;
  poll(&poll_gpio, 1, -1);
  read(ec);  // discard first IRQ
  wait_for(&poll_gpio, ec);
}
