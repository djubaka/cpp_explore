#include "kv-gpio/gpio_direction.hpp"
#include <fcntl.h>
#include <unistd.h>
#include <array>
#include <cerrno>

gpio_direction::gpio_direction(sodimm_gpio_pin sodimm) : pin_(sodimm) {
  auto dir = pin_.direction_path();
  fd_ = ::open(dir.c_str(), O_RDONLY);
}

gpio_direction::~gpio_direction() noexcept {
  if (fd_ != -1) {
    ::close(fd_);
  }
}

auto gpio_direction::direction(std::error_code &ec) const -> std::string {
  if (ec) {
    return std::string{};
  }

  std::array<char, 5> dir{};
  auto res = ::read(fd_, dir.data(), dir.size());
  if (res < 0) {
    ec = std::error_code{errno, std::generic_category()};
  }

  // the read will contain an additional NL character, we discard it
  const auto beg = dir.begin();
  const auto end = dir.begin() + res - 1;
  return std::string{beg, end};
}