#include "kv-gpio/device_selection.hpp"
#include <optional>
#include "kv-gpio/gpio_export.hpp"
#include "kv-gpio/gpio_value.hpp"

namespace {

template <bool turn_on>
void switch_pin(int sodimm_no, std::error_code& ec) {
  if (ec) return;

  sodimm_gpio_pin pin{sodimm_no};
  gpio_export exp{pin};
  if (exp.as_output(ec); !ec) {
    gpio_value val{pin};
    if constexpr (turn_on)
      val.turn_on(ec);
    else
      val.turn_off(ec);
  }
}

void do_select_control(std::error_code& ec) {
  switch_pin<true>(103, ec);
  switch_pin<false>(101, ec);
}

void do_select_measurement(std::error_code& ec) {
  switch_pin<false>(103, ec);
  switch_pin<true>(101, ec);
}

void do_select_mtv_chopper(std::error_code& ec) {
  switch_pin<true>(103, ec);
  switch_pin<true>(101, ec);
}

void do_select_printer(std::error_code& ec) {
  switch_pin<true>(103, ec);
  switch_pin<true>(101, ec);
}

void do_select_module_1(std::error_code& ec) {
  switch_pin<false>(103, ec);
  switch_pin<false>(101, ec);

  switch_pin<true>(97, ec);
  switch_pin<false>(85, ec);
  switch_pin<false>(79, ec);
}

void do_select_module_2(std::error_code& ec) {
  switch_pin<false>(103, ec);
  switch_pin<false>(101, ec);

  switch_pin<false>(97, ec);
  switch_pin<true>(85, ec);
  switch_pin<false>(79, ec);
}

void do_select_module_3(std::error_code& ec) {
  switch_pin<false>(103, ec);
  switch_pin<false>(101, ec);

  switch_pin<true>(97, ec);
  switch_pin<true>(85, ec);
  switch_pin<false>(79, ec);
}

void do_select_module_4(std::error_code& ec) {
  switch_pin<false>(103, ec);
  switch_pin<false>(101, ec);

  switch_pin<false>(97, ec);
  switch_pin<false>(85, ec);
  switch_pin<true>(79, ec);
}

void do_select_module_5(std::error_code& ec) {
  switch_pin<false>(103, ec);
  switch_pin<false>(101, ec);

  switch_pin<true>(97, ec);
  switch_pin<false>(85, ec);
  switch_pin<true>(79, ec);
}

void do_select_module_6(std::error_code& ec) {
  switch_pin<false>(103, ec);
  switch_pin<false>(101, ec);

  switch_pin<false>(97, ec);
  switch_pin<true>(85, ec);
  switch_pin<true>(79, ec);
}

void do_select_stop(std::error_code& ec) {
  switch_pin<true>(97, ec);
  switch_pin<true>(85, ec);
  switch_pin<true>(79, ec);
}

void do_reset_stop(std::error_code& ec) {
  switch_pin<false>(97, ec);
  switch_pin<false>(85, ec);
  switch_pin<false>(79, ec);
}

enum class gpio_mode_t {
  control_board,
  measurement_board,
  mtv_chopper,
  printer,
  module_1,
  module_2,
  module_3,
  module_4,
  module_5,
  module_6,
  stop,
  reset_stop
};

std::optional<gpio_mode_t>& gpio_mode() {
  static std::optional<gpio_mode_t> gpio;
  return gpio;
}

bool gpio_mode_selected(gpio_mode_t gpio) {
  const auto& mode = gpio_mode();
  return mode.has_value() and (*mode == gpio);
}

void set_gpio_mode(gpio_mode_t gpio) {
  auto& mode = gpio_mode();
  mode = gpio;
}

using mode_selection_func = void (*)(std::error_code& ec);

mode_selection_func selection_delegate(gpio_mode_t mode) {
  switch (mode) {
    case gpio_mode_t::control_board:
      return do_select_control;
    case gpio_mode_t::measurement_board:
      return do_select_measurement;
    case gpio_mode_t::mtv_chopper:
      return do_select_mtv_chopper;
    case gpio_mode_t::printer:
      return do_select_printer;
    case gpio_mode_t::module_1:
      return do_select_module_1;
    case gpio_mode_t::module_2:
      return do_select_module_2;
    case gpio_mode_t::module_3:
      return do_select_module_3;
    case gpio_mode_t::module_4:
      return do_select_module_4;
    case gpio_mode_t::module_5:
      return do_select_module_5;
    case gpio_mode_t::module_6:
      return do_select_module_6;
    case gpio_mode_t::stop:
      return do_select_stop;
    case gpio_mode_t::reset_stop:
      return do_reset_stop;
    default:
      return do_select_control;
  }

  return do_select_control;
}

void do_select_mode(std::error_code& ec, gpio_mode_t mode) {
  if (gpio_mode_selected(mode)) {
    return;
  }

  auto dlgt = selection_delegate(mode);
  dlgt(ec);

  if (not ec) {
    set_gpio_mode(mode);
  }
}

}  // namespace

namespace device_selection {

void select_control_board(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::control_board);
}

void select_measurement(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::measurement_board);
}

void select_mtv_chopper(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::mtv_chopper);
}

void select_printer(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::printer);
}

void select_module_1(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::module_1);
}

void select_module_2(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::module_2);
}

void select_module_3(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::module_3);
}

void select_module_4(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::module_4);
}

void select_module_5(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::module_5);
}

void select_module_6(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::module_6);
}

void select_stop(std::error_code& ec) { do_select_mode(ec, gpio_mode_t::stop); }

void reset_stop(std::error_code& ec) {
  do_select_mode(ec, gpio_mode_t::reset_stop);
}

}  // namespace device_selection