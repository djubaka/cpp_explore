#pragma once

#include <system_error>
#include "sodimm_gpio_pin.hpp"

void await_event_on_falling_edge(int sodimm, std::error_code& ec);
void await_event_on_rising_edge(int sodimm, std::error_code& ec);
void await_event_on_change(int sodimm, std::error_code& ec);

struct gpio_event {
  explicit gpio_event(sodimm_gpio_pin pin) : pin_{pin} {}
  void wait_on_rising_edge(std::error_code& ec) const;
  void wait_on_falling_edge(std::error_code& ec) const;
  void wait_on_change(std::error_code& ec) const;
  sodimm_gpio_pin pin_;
};