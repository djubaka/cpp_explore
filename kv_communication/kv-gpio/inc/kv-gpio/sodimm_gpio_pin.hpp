#pragma once

#include <string>

class sodimm_gpio_pin {
 public:
  sodimm_gpio_pin(int pin);
  int as_kernel_pin() const;
  std::string as_kernel_pin_string() const;
  std::string direction_path() const;
  std::string edge_path() const;
  std::string value_path() const;
  auto sodimm_number() const { return pin_; }

 private:
  int pin_;
};
