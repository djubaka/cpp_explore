#pragma once

#include <optional>
#include <string_view>
#include <system_error>
#include "sodimm_gpio_pin.hpp"

class gpio_value {
 public:
  explicit gpio_value(sodimm_gpio_pin pin);
  ~gpio_value();

  gpio_value(const gpio_value&) = delete;
  gpio_value& operator=(const gpio_value&) = delete;

  gpio_value(gpio_value&& x) : fd_{x.fd_}, pin_{x.pin_} { x.fd_ = -1; }
  gpio_value& operator=(gpio_value&& x) {
    fd_ = x.fd_;
    pin_ = x.pin_;
    x.fd_ = -1;

    return *this;
  }

  auto fd() const { return fd_; }

  void turn_on(std::error_code& ec) const;
  void turn_off(std::error_code& ec) const;
  char read(std::error_code& ec) const;
  auto level(std::error_code& ec) const -> std::optional<std::uint8_t>;
  void wait_for_trigger(std::error_code& ec) const;

 private:
  void set_as(std::string_view level, std::error_code& ec) const;

 private:
  int fd_{-1};
  sodimm_gpio_pin pin_;
};
