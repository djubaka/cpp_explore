#pragma once

#include <system_error>

namespace device_selection {
void select_control_board(std::error_code& ec);
void select_measurement(std::error_code& ec);
void select_printer(std::error_code& ec);
void select_module_1(std::error_code& ec);
void select_module_2(std::error_code& ec);
void select_module_3(std::error_code& ec);
void select_module_4(std::error_code& ec);
void select_module_5(std::error_code& ec);
void select_module_6(std::error_code& ec);
void select_stop(std::error_code& ec);
void reset_stop(std::error_code& ec);

}  // namespace device_selection
