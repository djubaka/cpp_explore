#pragma once

#include <string_view>
#include <system_error>
#include "sodimm_gpio_pin.hpp"

class gpio_export {
 public:
  explicit gpio_export(sodimm_gpio_pin pin);
  void as_input(std::error_code& ec) const;
  void as_output(std::error_code& ec) const;
  void trigger_on_rising_edge(std::error_code& ec) const;
  void trigger_on_falling_edge(std::error_code& ec) const;
  void trigger_on_change(std::error_code& ec) const;

 private:
  void do_pin_export(std::error_code& ec) const;
  void configure_as(std::string_view descr, std::error_code& ec) const;
  void trigger_on(std::string_view edge, std::error_code& ec) const;

 private:
  sodimm_gpio_pin pin_;
};