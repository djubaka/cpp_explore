#pragma once

#include <string>
#include <system_error>
#include "sodimm_gpio_pin.hpp"

class gpio_direction {
 public:
  explicit gpio_direction(sodimm_gpio_pin sodimm);
  ~gpio_direction() noexcept;

  gpio_direction(const gpio_direction&) = delete;
  gpio_direction& operator=(const gpio_direction&) = delete;

  gpio_direction(gpio_direction&& x) noexcept : fd_(x.fd_), pin_(x.pin_) {
    x.fd_ = -1;
  }

  gpio_direction& operator=(gpio_direction&& x) noexcept {
    pin_ = x.pin_;
    fd_ = x.fd_;
    x.fd_ = -1;
    return *this;
  }

  auto direction(std::error_code& ec) const -> std::string;

 private:
  int fd_{-1};
  sodimm_gpio_pin pin_;
};