#include "kv-communication/external_connection.hpp"
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <asio/error.hpp>
#include "ext_conn/external_connection_handle.hpp"
#include "kv-communication/log_bytes.hpp"

namespace KvCommunication {
external_connection::external_connection(ext_conn_t ext_conn_handle)
    : ext_conn_{std::move(ext_conn_handle)} {
  ext_conn_->external_message_arrived.connect(&external_connection::on_message,
                                              this);
}

external_connection::~external_connection() = default;

external_connection::external_connection(external_connection &&) = default;

external_connection &external_connection::operator=(external_connection &&) =
    default;

void external_connection::start_listening() { ext_conn_->start_listening(); }

void external_connection::on_message(message_view_t message,
                                     const std::error_code &ec) {
  if (ec) {
    if (ec != asio::error::operation_aborted && ec != asio::error::eof) {
      fmt::print("reading from extern connection error: {}\n", ec.message());
    }

  } else {
    fmt::print("input arrived: {}\n", KvCommunication::log_bytes{message});
    message_arrived(message);
  }
}

void external_connection::stop_listening() { ext_conn_->stop_listening(); }

void external_connection::send_message(std::vector<std::uint8_t> message) {
  fmt::print("replying: {}\n", KvCommunication::log_bytes{message});
  ext_conn_->send_message(std::move(message));
}

void external_connection::send_error_code(const std::error_code &ec) {
  // TO DO
}
}  // namespace KvCommunication