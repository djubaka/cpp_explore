#pragma once

#include <condition_variable>
#include <deque>
#include <mutex>
#include "kv-communication/dispatcher.hpp"

template <typename T>
class dispatch_queue {
  using lock_t = std::unique_lock<std::mutex>;

 public:
  bool pop(T &ingredients) {
    lock_t lock{mutex_};
    while (q_.empty() && !done_) {
      ready_.wait(lock);
    }

    if (q_.empty()) {
      return false;
    }

    ingredients = std::move(q_.front());
    q_.pop_front();

    return true;
  }

  void push(T ingredients) {
    {
      lock_t lock{mutex_};
      q_.emplace_back(std::move(ingredients));
    }

    ready_.notify_one();
  }

  void done() {
    {
      lock_t lock{mutex_};
      done_ = true;
    }

    ready_.notify_all();
  }

 private:
  bool done_{false};
  std::deque<T> q_;
  std::mutex mutex_;
  std::condition_variable ready_;
};
