#include "sync_device_dispatcher.hpp"
#include "kv-communication/icommunication.hpp"

namespace KvCommunication {
sync_device_dispatcher::sync_device_dispatcher(
    sync_device_dispatcher::device_comm_t comm)
    : comm_{comm} {
  dispatch_thread_ = std::thread([this]() { run(); });
}

sync_device_dispatcher::~sync_device_dispatcher() {
  dispatch_queue_.done();
  dispatch_thread_.join();
}

void sync_device_dispatcher::dispatch(dispatch_ingredients ingredients) {
  dispatch_queue_.push(std::move(ingredients));
}

void sync_device_dispatcher::run() {
  while (true) {
    dispatch_ingredients pkg;
    if (!dispatch_queue_.pop(pkg)) {
      break;
    }

    std::error_code ec;
    auto res = comm_->upload(pkg.data, pkg.match, ec);
    pkg.callback(res, ec);
  }
}
}  // namespace KvCommunication