#include "device_dispatcher.hpp"
#include "kv-communication/icommunication.hpp"

namespace KvCommunication {
device_dispatcher::device_dispatcher(device_dispatcher::device_comm_t comm)
    : comm_{comm} {
  dispatch_thread_ = std::thread([this]() { run(); });
}

device_dispatcher::~device_dispatcher() {
  dispatch_queue_.done();
  dispatch_thread_.join();
}

void device_dispatcher::dispatch(dispatch_ingredients ingredients) {
  dispatch_queue_.push(std::move(ingredients));
}

void device_dispatcher::run() {
  while (true) {
    {
      std::unique_lock<std::mutex> lk{m_};
      cv_.wait(lk, [this] { return !dispatching_; });
      dispatching_ = true;
    }

    if (!dispatch_queue_.pop(current_package_)) break;

    do_dispatch();
  }
}

void device_dispatcher::do_dispatch() {
  auto dt = data_span{current_package_.data};
  auto cb = [&](data_span data, const std::error_code& ec) {
    current_package_.callback(data, ec);
    on_dispatch_completed();
  };

  comm_->async_upload(dt, cb, current_package_.match);
}

void device_dispatcher::on_dispatch_completed() {
  {
    std::lock_guard<std::mutex> lk{m_};
    dispatching_ = false;
  }

  cv_.notify_one();
}
}  // namespace KvCommunication
