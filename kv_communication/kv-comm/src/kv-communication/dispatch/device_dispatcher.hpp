#pragma once

#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>
#include "dispatch_queue.hpp"

namespace KvCommunication {
class ICommunication;

class device_dispatcher : public dispatcher {
  using device_comm_t = KvCommunication::ICommunication *;

 public:
  explicit device_dispatcher(device_comm_t);
  ~device_dispatcher() override;

  void dispatch(dispatch_ingredients) override;

 private:
  void run();
  void do_dispatch();
  void on_dispatch_completed();

 private:
  device_comm_t comm_;
  dispatch_queue<dispatch_ingredients> dispatch_queue_;
  dispatch_ingredients current_package_;

  bool dispatching_{false};
  std::mutex m_;
  std::condition_variable cv_;
  std::thread dispatch_thread_;
};

}  // namespace KvCommunication
