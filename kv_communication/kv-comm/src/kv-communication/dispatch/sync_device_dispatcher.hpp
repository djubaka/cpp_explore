#pragma once

#include <thread>
#include "dispatch_queue.hpp"

namespace KvCommunication {
class ICommunication;

class sync_device_dispatcher : public dispatcher {
  using device_comm_t = KvCommunication::ICommunication *;

 public:
  explicit sync_device_dispatcher(device_comm_t);
  ~sync_device_dispatcher() override;

  void dispatch(dispatch_ingredients) override;

 private:
  void run();

 private:
  device_comm_t comm_;
  dispatch_queue<dispatch_ingredients> dispatch_queue_;
  std::thread dispatch_thread_;
};

}  // namespace KvCommunication