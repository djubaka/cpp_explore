#pragma once

#include <asio/io_context.hpp>
#include <asio/serial_port.hpp>
#include <memory>
#include "kv-communication/icommunication.hpp"

namespace KvCommunication {
std::unique_ptr<ICommunication> uart(asio::io_context& io,
                                     const std::string& port_name,
                                     unsigned int baud = 4800);

void change_baudrate(asio::serial_port::baud_rate new_baud);
void change_stop_bits(asio::serial_port::stop_bits stop_bits);
void change_parity(asio::serial_port::parity new_parity);
}  // namespace KvCommunication