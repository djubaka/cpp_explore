#include "communication_builder.hpp"
#include <asio/serial_port.hpp>
#include <sigslot/signal.hpp>
#include <system_error>
#include "communication_model.hpp"
#include "null_communication.hpp"

namespace {
using baudrate = asio::serial_port::baud_rate;
using flow_control = asio::serial_port_base::flow_control;
using parity = asio::serial_port_base::parity;
using stop_bits = asio::serial_port_base::stop_bits;
using char_size = asio::serial_port_base::character_size;
using serial_model = KvCommunication::communication_model<asio::serial_port>;

sigslot::signal<baudrate>& baud_signal() {
  static sigslot::signal<baudrate> baudrate_changed;
  return baudrate_changed;
}

sigslot::signal<parity>& parity_signal() {
  static sigslot::signal<parity> parity_changed;
  return parity_changed;
}

sigslot::signal<stop_bits>& stop_bits_signal() {
  static sigslot::signal<stop_bits> stop_bits_changed;
  return stop_bits_changed;
}

asio::serial_port initiliazed_port(asio::io_context& io,
                                   const std::string& port_name, unsigned baud,
                                   std::error_code& ec) {
  asio::serial_port port{io};
  port.open(port_name, ec);
  port.set_option(baudrate{baud}, ec);
  port.set_option(flow_control{flow_control::none}, ec);
  port.set_option(parity{parity::even}, ec);
  port.set_option(char_size{8}, ec);
  port.set_option(stop_bits{stop_bits::one}, ec);
  return port;
}

std::unique_ptr<serial_model> port_as_options_observer(asio::serial_port port,
                                                       asio::io_context& io) {
  auto comm = std::make_unique<serial_model>(std::move(port), io);

  auto baud_conn = baud_signal().connect(
      [ptr = comm.get()](baudrate baud) { ptr->protocol().set_option(baud); });

  auto par_conn = parity_signal().connect(
      [ptr = comm.get()](parity par) { ptr->protocol().set_option(par); });

  auto stb_conn = stop_bits_signal().connect(
      [ptr = comm.get()](stop_bits bits) { ptr->protocol().set_option(bits); });

  comm->on_destruction.connect([baud_conn, par_conn, stb_conn]() mutable {
    if (baud_conn.valid()) {
      baud_conn.disconnect();
    }

    if (par_conn.valid()) {
      par_conn.disconnect();
    }

    if (stb_conn.valid()) {
      stb_conn.disconnect();
    }
  });

  return comm;
}

}  // namespace

namespace KvCommunication {
std::unique_ptr<ICommunication> uart(asio::io_context& io,
                                     const std::string& port_name,
                                     unsigned baud) {
  std::error_code ec;
  auto port = initiliazed_port(io, port_name, baud, ec);
  if (ec) {
    return std::make_unique<null_communication>(ec);
  }

  return port_as_options_observer(std::move(port), io);
}

void change_baudrate(asio::serial_port::baud_rate new_baud) {
  baud_signal()(new_baud);
}

void change_parity(asio::serial_port::parity new_parity) {
  parity_signal()(new_parity);
}

void change_stop_bits(asio::serial_port::stop_bits stop_bits) {
  stop_bits_signal()(stop_bits);
}

}  // namespace KvCommunication
