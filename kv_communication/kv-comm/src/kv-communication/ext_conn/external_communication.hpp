#pragma once

#include <asio/io_service.hpp>
#include <asio/strand.hpp>
#include <deque>
#include "external_connection_handle.hpp"

namespace KvCommunication {
class ICommunication;
class external_communication : public external_connection_handle {
  using response_queue_t = std::deque<message_t>;
  using comm_t = KvCommunication::ICommunication*;

 public:
  external_communication(asio::io_service& io, comm_t comm);

  void start_listening();
  void stop_listening();
  void send_message(message_t message);

 private:
  void read_packet();
  void emit_message(message_view_t message);
  void queue_message(message_t data);
  void start_packet_send();
  void packet_send_done(const std::error_code& ec);
  static bool message_not_valid(message_view_t message);
  static std::vector<message_view_t> split_message(message_view_t message);

 private:
  asio::io_service::strand strand_;
  comm_t comm_;
  response_queue_t response_queue_;
};

}  // namespace KvCommunication
