#include "external_communication.hpp"
#include <fmt/ostream.h>
#include <fmt/printf.h>
#include <asio/post.hpp>
#include <gsl/span_ext>
#include <string>
#include "kv-communication/icommunication.hpp"
#include "kv-communication/log_bytes.hpp"

namespace KvCommunication {
external_communication::external_communication(asio::io_service &io,
                                               comm_t comm)
    : strand_{io}, comm_{comm} {
  comm_->configure_timeout(std::chrono::milliseconds::zero());
}

void external_communication::start_listening() { read_packet(); }

void external_communication::read_packet() {
  auto cb = [this](data_span data, const std::error_code &ec) {
    if (ec) {
      external_message_arrived(data, ec);
    } else {
      emit_message(data);
      read_packet();
    }
  };

  comm_->async_read(std::move(cb), match<zero_terminated>());
}

void external_communication::emit_message(message_view_t message) {
  // there is a possibility that several messages where joined
  // split them first

  std::error_code ec{};
  auto split = split_message(message);
  for (auto s : split) {
    if (message_not_valid(s)) {
      fmt::print("extern input message format wrong: {}\n", log_bytes{s});
    } else {
      external_message_arrived(s, ec);
    }
  }
}

std::vector<external_communication::message_view_t>
external_communication::split_message(message_view_t message) {
  std::vector<message_view_t> split;

  auto first = message.begin();
  while (first != message.end()) {
    const auto second = std::find(first, std::end(message), 0x00);
    if (first != second) {
      auto from_start = std::distance(message.begin(), first);
      auto range = std::distance(first, second);
      auto len = (second != message.end()) ? (range + 1) : range;
      split.emplace_back(gsl::make_span(message.data() + from_start, len));
    }
    if (second == message.end()) break;
    first = std::next(second);
  }

  return split;
}

bool external_communication::message_not_valid(message_view_t message) {
  if (message.size() < 2) return true;

  // the message should have a head of 27(0x1b), and a tail of 0
  auto first = *(message.begin());
  auto last = *(message.end() - 1);
  return first != 0x1b || last != 0x00;
}

void external_communication::stop_listening() { comm_->cancel(); }

void external_communication::send_message(message_t message) {
  asio::post(strand_, [this, msg = std::move(message)] {
    queue_message(std::move(msg));
  });
}

void external_communication::queue_message(message_t data) {
  bool write_in_progress = !response_queue_.empty();
  response_queue_.push_back(std::move(data));
  if (!write_in_progress) {
    start_packet_send();
  }
}

void external_communication::start_packet_send() {
  asio::post(strand_, [this] {
    auto &data = response_queue_.front();
    comm_->async_write(
        data, [this](const std::error_code &ec) { packet_send_done(ec); });
  });
}

void external_communication::packet_send_done(const std::error_code &ec) {
  if (ec) {
    fmt::print("writing to extern conn error:{}\n", ec.message());
  } else {
    response_queue_.pop_front();
    if (!response_queue_.empty()) {
      start_packet_send();
    }
  }
}
}  // namespace KvCommunication
