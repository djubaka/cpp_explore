#pragma once

#include <asio/io_context.hpp>
#include <memory>
#include "external_connection_handle.hpp"
#include "kv-communication/icommunication.hpp"

namespace KvCommunication {
namespace detail {
class tcp_server;
}

class tcp_connection_handle : public external_connection_handle {
 public:
  explicit tcp_connection_handle(asio::io_context& io);
  ~tcp_connection_handle() override;

  void start_listening() override;
  void stop_listening() override;
  void send_message(message_t) override;

 private:
  void on_connection_established(ICommunication* comm);
  void on_ext_message(message_view_t, const std::error_code& ec);

 private:
  std::unique_ptr<detail::tcp_server> server_;
  std::unique_ptr<ICommunication> comm_;
  std::unique_ptr<external_connection_handle> handle_;
};
}  // namespace KvCommunication