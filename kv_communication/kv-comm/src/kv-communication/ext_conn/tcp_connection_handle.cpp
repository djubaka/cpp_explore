#include "tcp_connection_handle.hpp"
#include "external_communication.hpp"
#include "kv-communication/communication_model.hpp"
#include <asio/ip/tcp.hpp>
#include <fmt/printf.h>
#include <optional>

namespace KvCommunication {
namespace detail {
class tcp_server {
  using tcp = asio::ip::tcp;
  using comm_t = KvCommunication::ICommunication *;

public:
  explicit tcp_server(asio::io_context &io) : acceptor_{io} {}

  void start(std::uint16_t port) {
    tcp::endpoint endpoint{tcp::v4(), port};
    acceptor_.open(endpoint.protocol());
    acceptor_.set_option(tcp::acceptor::reuse_address(true));
    acceptor_.bind(endpoint);
    acceptor_.listen();
    do_accept();
  }

  void stop() { acceptor_.cancel(); }

  asio::io_context &io_context() { return acceptor_.get_io_context(); }
  sigslot::signal<comm_t> on_connection_accepted;

private:
  void do_accept() {
    socket_ = tcp::socket{io_context()};
    acceptor_.async_accept(
        *socket_, [this](const std::error_code &ec) { accept_handler(ec); });
  }

  void accept_handler(const std::error_code &ec) {
    if (ec) {
      fmt::print("server accept error: {}\n", ec.message());
    } else {
      on_connection_accepted(new communication_model<tcp::socket>{
          std::move(*socket_), io_context()});
      do_accept();
    }
  }

private:
  tcp::acceptor acceptor_;
  std::optional<tcp::socket> socket_;
};
} // namespace detail

tcp_connection_handle::tcp_connection_handle(asio::io_context &io)
    : server_{std::make_unique<detail::tcp_server>(io)} {
  server_->on_connection_accepted.connect(
      &tcp_connection_handle::on_connection_established, this);
}

tcp_connection_handle::~tcp_connection_handle() { server_->stop(); }

void tcp_connection_handle::start_listening() { server_->start(54321); }

void tcp_connection_handle::stop_listening() {
  if (comm_) {
    handle_->stop_listening();
  }
}

void tcp_connection_handle::send_message(message_t msg) {
  if (comm_) {
    handle_->send_message(std::move(msg));
  }
}

void tcp_connection_handle::on_connection_established(ICommunication *comm) {
  comm_ = std::unique_ptr<ICommunication>{comm};
  handle_ = std::make_unique<external_communication>(server_->io_context(),
                                                     comm_.get());

  handle_->external_message_arrived.connect(
      &tcp_connection_handle::on_ext_message, this);

  handle_->start_listening();
}

void tcp_connection_handle::on_ext_message(message_view_t msg,
                                           const std::error_code &ec) {
  external_message_arrived(msg, ec);
}

} // namespace KvCommunication