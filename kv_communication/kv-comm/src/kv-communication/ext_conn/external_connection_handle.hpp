#pragma once

#include <cstdint>
#include <gsl/span>
#include <sigslot/signal.hpp>
#include <string_view>
#include <system_error>
#include <vector>

namespace KvCommunication {
class external_connection_handle {
 public:
  using message_view_t = gsl::span<const std::uint8_t>;
  using message_t = std::vector<std::uint8_t>;

 public:
  virtual ~external_connection_handle() = default;
  virtual void start_listening() = 0;
  virtual void stop_listening() = 0;
  virtual void send_message(message_t message) = 0;
  sigslot::signal<message_view_t, const std::error_code&>
      external_message_arrived;
};
}  // namespace KvCommunication