#include "null_communication.hpp"

namespace KvCommunication {
void null_communication::close() {}

void null_communication::cancel() {}

std::size_t null_communication::bytes_available(std::error_code &ec) {
  ec = ec_;
  return 0;
}

void null_communication::clear_buffer(std::error_code &ec) { ec = ec_; }

io_result null_communication::upload(data_span, match_delegate,
                                     std::error_code &ec) {
  ec = ec_;
  return io_result{};
}

void null_communication::write(data_span, std::error_code &ec) { ec = ec_; }

io_result null_communication::read(match_delegate, std::error_code &ec) {
  ec = ec_;
  return io_result{};
}

void null_communication::async_upload(data_span, read_callback_t callback,
                                      match_delegate) {
  io_result empty{};
  callback(empty, ec_);
}

void null_communication::async_write(data_span, write_callback_t callback) {
  callback(ec_);
}

void null_communication::async_read(read_callback_t callback, match_delegate) {
  io_result empty{};
  callback(empty, ec_);
}
}  // namespace KvCommunication
