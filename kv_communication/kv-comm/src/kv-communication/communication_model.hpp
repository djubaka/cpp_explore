#pragma once

#include <asio/bind_executor.hpp>
#include <asio/io_context.hpp>
#include <asio/post.hpp>
#include <asio/read.hpp>
#include <asio/read_until.hpp>
#include <asio/steady_timer.hpp>
#include <asio/strand.hpp>
#include <asio/write.hpp>
#include <sigslot/signal.hpp>
#include "kv-communication/icommunication.hpp"

namespace asio {
template <>
struct is_match_condition<KvCommunication::match_delegate>
    : public std::true_type {};
}  // namespace asio

namespace KvCommunication {
template <typename AsioProtocol>
class communication_model : public ICommunication {
 public:
  communication_model(AsioProtocol x, asio::io_context &io)
      : protocol_{std::move(x)}, watchdog_{io}, strand_{io} {};

  ~communication_model() { on_destruction(); }

  AsioProtocol &protocol() { return protocol_; }
  const AsioProtocol &protocol() const { return protocol_; }

  sigslot::signal<> on_destruction;

  void close() override {
    asio::post(strand_, [this]() {
      if (protocol_.is_open()) {
        std::error_code ec;
        protocol_.close(ec);
      }
    });
  }

  void cancel() override {
    asio::post(strand_, [this]() {
      std::error_code ec;
      protocol_.cancel(ec);
    });
  }

  std::size_t bytes_available(std::error_code &ec) override {
    if (ec) return 0;

    int available = 0;
    if (ioctl(protocol_.native_handle(), FIONREAD, &available) == -1) {
      ec = std::error_code{errno, std::generic_category()};
    }

    return ec ? static_cast<std::size_t>(0)
              : static_cast<std::size_t>(available);
  }

  void clear_buffer(std::error_code &ec) override {
    if (auto available = bytes_available(ec)) {
      io_result buff;
      asio::read(protocol_, asio::dynamic_buffer(buff),
                 asio::transfer_exactly(available), ec);
    }
  }

  io_result upload(data_span bytes_to_send, match_delegate match,
                   std::error_code &ec) override {
    write(bytes_to_send, ec);
    return read(match, ec);
  }

  void write(data_span bytes_to_send, std::error_code &ec) override {
    if (ec) return;
    auto len = static_cast<std::size_t>(bytes_to_send.size());
    auto data_buff = asio::buffer(bytes_to_send.data(), len);
    asio::write(protocol_, data_buff, ec);
  }

  io_result read(match_delegate match, std::error_code &ec) override {
    io_result buff;
    if (wait_for_incoming(ec); !ec) {
      asio::read_until(protocol_, asio::dynamic_buffer(buff), match, ec);
    }

    return buff;
  }

  void async_upload(data_span bytes_to_send, read_callback_t callback,
                    match_delegate match) override {
    std::error_code ec;
    if (write(bytes_to_send, ec); ec) {
      return callback(io_result{}, ec);
    }

    async_read(std::move(callback), match);
  }

  void async_write(data_span bytes_to_send, write_callback_t wcb) override {
    auto len = static_cast<std::size_t>(bytes_to_send.size());
    auto buff = asio::buffer(bytes_to_send.data(), len);

    asio::async_write(protocol_, buff,
                      [wcb_ = std::move(wcb)](const std::error_code &ec,
                                              std::size_t) { wcb_(ec); });
  }

  void async_read(read_callback_t rcb, match_delegate match) override {
    auto on_read = [this, cb = std::move(rcb)](const std::error_code &ec,
                                               std::size_t bytes_read) {
      auto watch_cancelled = (watchdog_.cancel() > 0);
      auto io_to_report = on_data(ec, bytes_read);
      auto ec_to_report = watch_cancelled ? ec : asio::error::timed_out;
      cb(io_to_report, ec);
    };

    asio::async_read_until(protocol_, asio::dynamic_buffer(buff_), match,
                           asio::bind_executor(strand_, std::move(on_read)));

    if (cancellable()) {
      kickoff_watchdog();
    }
  }

 private:
  void wait_for_incoming(std::error_code &ec) {
    if (ec) return;

    auto fd = protocol_.native_handle();
    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);

    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = std::chrono::microseconds{timeout()}.count();

    int ret = select(fd + 1, &rfds, nullptr, nullptr, &tv);
    if (ret == -1) {
      ec = std::error_code{errno, std::generic_category()};
    } else if (ret == 0) {
      ec = asio::error::timed_out;
    }
  }

  void kickoff_watchdog() {
    auto on_timed_out = [this](const std::error_code &ec) {
      if (ec) return;

      std::error_code e;
      protocol_.cancel(e);
    };

    watchdog_.expires_from_now(timeout());
    watchdog_.async_wait(asio::bind_executor(strand_, std::move(on_timed_out)));
  }

  io_result on_data(const std::error_code &ec, std::size_t bytes_read) {
    // we need to make a copy to make reads reentrant
    // inside the callback could be another call to async_read

    io_result io_res;
    if (!ec) {
      io_res.reserve(bytes_read);
      io_res.insert(io_res.begin(), buff_.begin(), buff_.begin() + bytes_read);
    }

    buff_.clear();
    return io_res;
  }

 private:
  AsioProtocol protocol_;
  asio::steady_timer watchdog_;
  asio::io_context::strand strand_;
  std::vector<std::uint8_t> buff_;
};
}  // namespace KvCommunication