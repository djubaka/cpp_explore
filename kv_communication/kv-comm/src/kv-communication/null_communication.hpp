#ifndef NULL_COMMUNICATION_HPP
#define NULL_COMMUNICATION_HPP

#include <system_error>
#include "kv-communication/icommunication.hpp"

namespace KvCommunication {
class null_communication : public ICommunication {
 public:
  explicit null_communication(std::error_code ec) : ec_{ec} {}

  void close() override;
  void cancel() override;
  std::size_t bytes_available(std::error_code &ec) override;
  void clear_buffer(std::error_code &ec) override;
  io_result upload(data_span, match_delegate, std::error_code &) override;
  void write(data_span, std::error_code &ec) override;
  io_result read(match_delegate, std::error_code &) override;
  void async_upload(data_span, read_callback_t, match_delegate) override;
  void async_write(data_span, write_callback_t) override;
  void async_read(read_callback_t, match_delegate) override;

 private:
  std::error_code ec_;
};
}  // namespace KvCommunication

#endif  // NULL_COMMUNICATION_HPP
