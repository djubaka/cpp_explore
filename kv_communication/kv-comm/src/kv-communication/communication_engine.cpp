#include "kv-communication/communication_engine.hpp"
#include <asio/error.hpp>
#include <asio/io_service.hpp>
#include <asio/serial_port.hpp>
#include <map>
#include <thread>
#include <vector>
#include "communication_builder.hpp"
#include "dispatch/sync_device_dispatcher.hpp"
#include "ext_conn/external_communication.hpp"
#include "ext_conn/tcp_connection_handle.hpp"

namespace {
using thread_pool_t = std::vector<std::thread>;
using asio_work_t = std::unique_ptr<asio::io_service::work>;
using comm_map_t =
    std::map<std::string, std::unique_ptr<KvCommunication::ICommunication>>;

struct engine_impl {
  engine_impl() {
    work = std::make_unique<asio::io_service::work>(io);
    auto cores = std::thread::hardware_concurrency();
    for (std::size_t i = 0; i < cores; ++i)
      thread_pool.emplace_back([this]() { io.run(); });
  }

  ~engine_impl() {
    work = nullptr;
    io.stop();
    for (auto &t : thread_pool) {
      t.join();
    }
  }

  communication_engine::dev_comm_t uart(const std::string &port,
                                        unsigned baudrate) {
    auto it = comm_cache.find(port);
    if (it == comm_cache.end()) {
      it = comm_cache.emplace(port, KvCommunication::uart(io, port, baudrate))
               .first;
    }

    return it->second.get();
  }

  asio::io_service io;
  asio_work_t work;
  thread_pool_t thread_pool;
  comm_map_t comm_cache;
};

engine_impl &impl() {
  static engine_impl impl;
  return impl;
}

}  // namespace

namespace communication_engine {
dev_comm_t uart(const std::string &port_name, std::uint32_t baudrate) {
  return impl().uart(port_name, baudrate);
}

dispatch_t device_dispatch(dev_comm_t comm) {
  return std::make_unique<KvCommunication::sync_device_dispatcher>(comm);
}

ext_conn_t server_with_usb_handle(const std::string &port,
                                  std::uint32_t baudrate) {
  return ext_conn_t{std::make_unique<KvCommunication::external_communication>(
      impl().io, uart(port, baudrate))};
}

ext_conn_t server_with_tcp_handle() {
  return ext_conn_t{
      std::make_unique<KvCommunication::tcp_connection_handle>(impl().io)};
}

bool is_timeout_error(const std::error_code &ec) {
  return (ec == asio::error::timed_out);
}

void change_baudrate(std::uint32_t baud) {
  KvCommunication::change_baudrate(asio::serial_port::baud_rate(baud));
}

void change_parity_to_none() {
  KvCommunication::change_parity(
      asio::serial_port::parity{asio::serial_port::parity::none});
}

void change_parity_to_even() {
  KvCommunication::change_parity(
      asio::serial_port::parity{asio::serial_port::parity::even});
}

}  // namespace communication_engine