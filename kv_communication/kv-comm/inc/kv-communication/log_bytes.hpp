#pragma once

#include <fmt/format.h>
#include <cctype>
#include <gsl/span>

namespace KvCommunication {

struct log_bytes {
  explicit log_bytes(gsl::span<const unsigned char> in) : data{in} {}

  template <typename OStream>
  friend OStream& operator<<(OStream& os, const log_bytes& log_bytes) {
    for (auto b : log_bytes.data) {
      if (std::isprint(b)) {
        os << b;
      } else {
        os << fmt::format("<0x{:02X}>", b);
      }

      os << " ";
    }

    return os;
  }

  gsl::span<const unsigned char> data;
};

}  // namespace KvCommunication