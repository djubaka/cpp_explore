#pragma once

#include <cstdint>
#include <gsl/span>
#include <memory>
#include <sigslot/signal.hpp>
#include <system_error>
#include <vector>

namespace KvCommunication {
class external_connection_handle;
class external_connection {
 public:
  using ext_conn_t = std::unique_ptr<external_connection_handle>;
  using message_view_t = gsl::span<const std::uint8_t>;

 public:
  explicit external_connection(ext_conn_t ext_conn_handle);
  ~external_connection();

  external_connection(external_connection&&);
  external_connection& operator=(external_connection&&);

  void start_listening();
  void stop_listening();
  void send_message(std::vector<std::uint8_t> message);
  void send_error_code(const std::error_code& ec);

  sigslot::signal<message_view_t> message_arrived;

 private:
  void on_message(message_view_t message, const std::error_code& ec);

 private:
  ext_conn_t ext_conn_;
};
}  // namespace KvCommunication