#pragma once

#include <functional>
#include <gsl/span>
#include <system_error>
#include <vector>
#include "response_match.hpp"

namespace KvCommunication {

struct dispatch_ingredients {
  using command_bytes_t = std::vector<std::uint8_t>;
  using data_span_t = gsl::span<const std::uint8_t>;
  using callback_t = std::function<void(data_span_t, const std::error_code &)>;

  dispatch_ingredients() {}

  dispatch_ingredients(command_bytes_t data_i, match_delegate mtch,
                       callback_t callback_i)
      : data{std::move(data_i)},
        match{std::move(mtch)},
        callback{std::move(callback_i)} {}

  command_bytes_t data;
  match_delegate match{new_line{}};
  callback_t callback;
};

class dispatcher {
 public:
  virtual ~dispatcher() = default;
  virtual void dispatch(dispatch_ingredients) = 0;
};

}  // namespace KvCommunication
