#pragma once

#include <memory>
#include <string>
#include "dispatcher.hpp"
#include "external_connection.hpp"
#include "icommunication.hpp"

namespace communication_engine {
using dev_comm_t = KvCommunication::ICommunication *;
using dispatch_t = std::unique_ptr<KvCommunication::dispatcher>;
using ext_conn_t = KvCommunication::external_connection;

dev_comm_t uart(const std::string &port, std::uint32_t baudrate);
dispatch_t device_dispatch(dev_comm_t comm);
ext_conn_t server_with_usb_handle(const std::string &port,
                                  std::uint32_t baudrate);

ext_conn_t server_with_tcp_handle();

bool is_timeout_error(const std::error_code &ec);
void change_baudrate(std::uint32_t baudrate);
void change_parity_to_none();
void change_parity_to_even();
};  // namespace communication_engine
