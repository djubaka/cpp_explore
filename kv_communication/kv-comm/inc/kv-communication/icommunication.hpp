#pragma once

#include <chrono>
#include <functional>
#include <gsl/span>
#include <system_error>
#include <vector>
#include "response_match.hpp"

namespace KvCommunication {

using io_result = std::vector<std::uint8_t>;
using data_span = gsl::span<const std::uint8_t>;
using write_callback_t = std::function<void(const std::error_code &)>;
using read_callback_t = std::function<void(data_span, const std::error_code &)>;

class ICommunication {
  using milliseconds = std::chrono::milliseconds;

 public:
  virtual ~ICommunication() = default;
  virtual void close() = 0;
  virtual void cancel() = 0;
  virtual void clear_buffer(std::error_code &ec) = 0;
  virtual void write(data_span, std::error_code &ec) = 0;
  virtual void async_upload(data_span, read_callback_t, match_delegate) = 0;
  virtual void async_write(data_span, write_callback_t) = 0;
  virtual void async_read(read_callback_t, match_delegate) = 0;
  virtual io_result upload(data_span, match_delegate, std::error_code &ec) = 0;
  virtual io_result read(match_delegate, std::error_code &ec) = 0;
  virtual std::size_t bytes_available(std::error_code &ec) = 0;

  milliseconds timeout() const { return timeout_; }
  void configure_timeout(milliseconds timeout) { timeout_ = timeout; }
  bool cancellable() const { return timeout_ > milliseconds::zero(); }

 private:
  std::chrono::milliseconds timeout_{5000};
};
}  // namespace KvCommunication
