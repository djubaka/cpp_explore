#pragma once

#include <functional>
#include <gsl/span>
#include <gsl/span_ext>
#include <iterator>
#include <utility>
#include <variant>

namespace KvCommunication {

struct any_received {
  template <typename Iterator>
  std::pair<Iterator, bool> operator()(Iterator begin, Iterator end) const {
    auto size = std::distance(begin, end);
    return size ? std::make_pair(end, true) : std::make_pair(begin, false);
  }
};

struct new_line {
  template <typename Iterator>
  std::pair<Iterator, bool> operator()(Iterator begin, Iterator end) const {
    auto size = std::distance(begin, end);
    if (size < 2) return std::make_pair(begin, false);

    char r = *(end - 2);
    char n = *(end - 1);

    bool is_match = (r == 13 && n == 10);
    return is_match ? std::make_pair(end, true) : std::make_pair(begin, false);
  }
};

struct zero_terminated {
  template <typename Iterator>
  std::pair<Iterator, bool> operator()(Iterator begin, Iterator end) const {
    auto size = std::distance(begin, end);
    if (size) {
      char z = *(end - 1);
      bool is_match = (z == 0);
      return is_match ? std::make_pair(end, true)
                      : std::make_pair(begin, false);
    } else {
      return std::make_pair(begin, false);
    }
  }
};

struct exact_count {
  explicit exact_count(std::uint32_t bytes_count)
      : bytes_to_read{bytes_count} {}

  template <typename Iterator>
  std::pair<Iterator, bool> operator()(Iterator begin, Iterator end) const {
    auto size = std::distance(begin, end);
    if (size < bytes_to_read)
      return std::make_pair(begin, false);
    else
      return std::make_pair(begin + bytes_to_read, true);
  }

  std::uint32_t bytes_to_read;
};

struct custom_match {
  using delegate = std::function<bool(gsl::span<const std::uint8_t>)>;
  explicit custom_match(delegate deleg) : delegate_{std::move(deleg)} {}

  template <typename Iterator>
  std::pair<Iterator, bool> operator()(Iterator begin, Iterator end) const {
    auto frst = reinterpret_cast<const std::uint8_t*>(&*begin);
    auto size = std::distance(begin, end);
    auto is_match = delegate_(gsl::make_span(frst, size));
    return is_match ? std::make_pair(end, true) : std::make_pair(begin, false);
  }

  delegate delegate_;
};

struct match_delegate {
  using match_variant = std::variant<any_received, new_line, zero_terminated,
                                     exact_count, custom_match>;

  match_delegate(match_variant mtch) : match{std::move(mtch)} {}

  template <typename Iterator>
  std::pair<Iterator, bool> operator()(Iterator begin, Iterator end) const {
    return std::visit(
        [begin, end](auto&& arg) -> std::pair<Iterator, bool> {
          return arg(begin, end);
        },
        match);
  }

  match_variant match;
};

template <typename MatchCondition>
match_delegate match() {
  return match_delegate{MatchCondition{}};
}

inline match_delegate match(custom_match::delegate deleg) {
  return match_delegate{custom_match{std::move(deleg)}};
}

}  // namespace KvCommunication