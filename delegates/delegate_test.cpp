#include "delegate.hpp"
#include <iostream>
#include <memory>
#include <string>

/*
template <class F, class... Args>
void call(F&& f, Args&&... args) {
  f(std::forward<Args>(args)...);
}
*/

using Callback = delegate<void(const std::string&, const std::error_code&)>;

void async_read(Callback callback, const std::string& delimiter = "\r\n") {
  callback("samir", std::error_code{});
}

struct string_functor {
  string_functor() { std::cout << "functor ctor\r\n"; }
  ~string_functor() { std::cout << "functor dtor\r\n"; }

  string_functor(const string_functor&) { std::cout << "functor copy\r\n"; }
  string_functor(string_functor&&) { std::cout << "functor move\r\n"; }

  string_functor& operator=(const string_functor&) {
    std::cout << "functor assignment\r\n";
    return *this;
  }

  string_functor& operator=(string_functor&&) {
    std::cout << "functor move assignment\r\n";
    return *this;
  }

  void operator()(const std::string& name) const {
    std::cout << "hello, " << name << "\r\n";
  }
};

int main() {
  auto functor = std::make_unique<string_functor>();
  async_read([f_ = std::move(functor)](const std::string& name,
                                       const std::error_code& ec) {
    std::cout << "hello: " << name << "\r\n";
    std::cout << "error: " << ec.message() << "\r\n";
    (*f_)(name);
  });
}